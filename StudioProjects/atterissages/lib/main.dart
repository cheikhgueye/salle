import 'dart:convert';
import 'dart:io';
import 'package:atterissages/cer.dart';
import 'package:atterissages/src/pages/splash/splash.dart';
import 'package:flutter/material.dart';

import 'package:flutter/services.dart';
//import 'package:flutter_stetho/flutter_stetho.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  /*
  WidgetsFlutterBinding.ensureInitialized();
  assert(
  await addSelfSignedCertificate(),
  );
  */
 // Stetho.initialize();

//  HttpOverrides.global = new MyHttpOverrides();

  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  static Map<int, Color> color = {
    50: Color.fromRGBO(41, 91, 153, .1),
    100: Color.fromRGBO(41, 91, 153, .2),
    200: Color.fromRGBO(41, 91, 153, .3),

    300: Color.fromRGBO(41, 91, 153, .4),

    400: Color.fromRGBO(41, 91, 153, .5),

    500: Color.fromRGBO(41, 91, 153, .6),

    600: Color.fromRGBO(41, 91, 153, .7),

    700: Color.fromRGBO(41, 91, 153, .8),

    800: Color.fromRGBO(41, 91, 153, .9),
    900: Color.fromRGBO(41, 91, 153, 1),
  };
  MaterialColor primeColor = MaterialColor(0xFF295b99, color);
  MaterialColor accentColor = MaterialColor(0xFF295b99, color);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Atterisage",
      theme: ThemeData(
        primarySwatch: primeColor,
        primaryColor: Colors.deepOrange,
        accentColor: primeColor,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen(),
    );
  }
}
