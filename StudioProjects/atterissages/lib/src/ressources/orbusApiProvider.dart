import 'dart:convert';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:atterissages/src/pages/splash/splash.dart';
//import 'package:eyro_toast/eyro_toast.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:http/io_client.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:io';
import 'package:http/io_client.dart';
//import 'package:http_certificate_pinning/secure_http_client.dart';
// Model Side

import 'dart:convert';
class OrbusApiProvider {

  // http://41.214.74.213:8022
  //final String baseUrl="http://10.3.80.166:7070/survolatt/";
  // final String baseUrl="https://192.168.2.32:8443/survol/​";
 //final String baseUrl='https://survolmob.gainde2000.sn';
 // final String baseUrl= 'https://192.168.2.32:8443/survolatt';
 final String baseUrl='https://mobileclearance.diplomatie.gouv.sn';

  Client client = Client();
 HttpClient client1 = new HttpClient();


 signin(username,password) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //context.usePrivateKey("/home/dbocharo/.minikube/certificat_prive.key");
    client1.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);


    var body
    = {
      "password":password.toString(),
      "username":username.toString() ,
   ///   "desktop": true,

    };
    print(Uri.parse(baseUrl+'/auth'+'/login'));
    HttpClientRequest request = await client1.postUrl(Uri.parse(baseUrl+'/auth'+'/login'));
    request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);
    final Map<String, String> headers = {
      "content-type":"application/json",
    //  "authorization":"Bearer "+prefs.getString("token"),
    };

    print(json.decode(reply));

    if(json.decode(reply)["status"]=="true"){
      print(json.decode(reply));
        prefs.setString("otp",json.decode(reply)["opt"]);
        prefs.setString("username", username);
        return json.decode(reply);

    }

 //   prefs.setString("otp",json.decode(response.body) ["opt"]);
    prefs.setString("username", username);
   return json.decode(reply);


    /*
print( Uri.parse('https://survolmob.gainde2000.sn:8080'+'/auth'+'/login'));
    final response = await client.post(
        Uri.parse('https://survolmob.gainde2000.sn:8080'+'/auth'+'/login'),
      headers:headers,
      body: json.encode(body),
      encoding: Encoding.getByName("utf-8")

    );

    if (response.statusCode == 200) {
      print(json.decode(response.body));

      prefs.setString("otp",json.decode(response.body) ["opt"]);
      prefs.setString("username", username);
      return json.decode(response.body);
      var data=json.decode(response.body);
     /* if(data ["status"]=="true"){

        prefs.setString("identifiant",data ["identifiant"].toString()  );
        prefs.setString("profil",data ["profil"]  );
        prefs.setString("libelleEntite",data ["codeEntite"]  );
        prefs.setString("survoltoken",data ["token"]  );
        prefs.setString("nom",data ["prenom"] +" "+ data ["nom"] );
        prefs.setString("codeProfil",data ["codeProfil"] );
        print(data ["username"]);

      }

     //// print(json.decode(response.body));
      return json.decode(response.body);*/
    } else {

     // return json.decode(response.body);
      throw Exception('Failed to load post');
    }
*/
  }

  verifOtp(otp,username) async{
    var gotp;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    gotp=prefs.getString("otp");
    client1.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    var body
    = {
      "givenOtp": int.parse(otp) ,
      "correctOtp":gotp ,
      "username": username.toString()

    };


    final Map<String, String> headers = {
      "content-type":"application/json",
      //  "authorization":"Bearer "+prefs.getString("token"),

    };
    HttpClientRequest request = await client1.postUrl(Uri.parse( baseUrl+ '/auth'+'/verifyOtp'));
    request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);


    print(json.decode(reply));

    if(json.decode(reply)["status"]=="true"){
      prefs.setString("identifiant",json.decode(reply) ["identifiant"].toString()  );
      prefs.setString("profil",json.decode(reply) ["profil"]  );
      prefs.setString("libelleEntite",json.decode(reply) ["codeEntite"]  );
      prefs.setString("survoltoken",json.decode(reply) ["token"]  );
      prefs.setString("nom",json.decode(reply) ["prenom"] +" "+ json.decode(reply) ["nom"] );
      prefs.setString("codeProfil",json.decode(reply) ["codeProfil"] );
      print(json.decode(reply) ["username"]);

      return json.decode(reply);

    } else {
      return 0;
    }


    /*

    final response = await client.post(
      baseUrl+ '/auth'+'/verifyOtp',
      headers:headers,
      body: json.encode(body),
      // encoding: Encoding.getByName("utf-8")

    );
    print(json.decode(response.body));
    if (response.statusCode == 200 && json.decode(response.body)["status"]!="false" ) {
      var data=json.decode(response.body);


      prefs.setString("identifiant",data ["identifiant"].toString()  );
      prefs.setString("profil",data ["profil"]  );
      prefs.setString("libelleEntite",data ["codeEntite"]  );
      prefs.setString("survoltoken",data ["token"]  );
      prefs.setString("nom",data ["prenom"] +" "+ data ["nom"] );
      prefs.setString("codeProfil",data ["codeProfil"] );
      print(data ["username"]);
      print(json.decode(response.body));
      return json.decode(response.body);
    } else {


      print(response.statusCode);

      return 0;
    ///  throw Exception('Failed to load post');
    }*/

  }
  dPermanantList() async{
    client1.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

    var token;
 var username;
 var id ;
 SharedPreferences prefs = await SharedPreferences.getInstance();
 token=prefs.get("survoltoken");
 username=prefs.getString("username");
 id=prefs.getString("identifiant");
 print(token);
   var body = {
      "id":id ,
      "token": token,
      "username": username
    };
    HttpClientRequest request = await client1.postUrl(Uri.parse(   baseUrl+ "/demande"+"/permanante"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer '+ token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(json.decode(reply));






      return json.decode(reply);




    /*
    final Map<String, String> headers = {
          'Content-Type' : 'application/json',
          'Accept-Language' : 'fr',
          'Authorization' : 'Bearer '+token,
    };
    final response = await client.post(
      baseUrl+ "/demande"+"/permanante",
          headers:headers,
          body: json.encode(body),
          encoding: Encoding.getByName("utf-8")

    );
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {


      return 0;
    }*/

  }


  dPonctuelleList() async{
    client1.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

    var token;
    var username;
    var id ;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.get("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");


    var body
    = {
      "id": 0,
      "token": token,
      "username": username
    };

    HttpClientRequest request = await client1.postUrl(Uri.parse(    baseUrl+ "/demande"+"/ponctuelle"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer '+ token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(json.decode(reply));






    return json.decode(reply);

  }
  dPreavisList() async{



    client1.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

    var token;
    var username;
    var id ;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.get("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");


    var body
    = {
      "id": 0,
      "token": token,
      "username": username
    };

    HttpClientRequest request = await client1.postUrl(Uri.parse(    baseUrl+ "/demande"+"/preavis",));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer '+ token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(json.decode(reply));






    return json.decode(reply);

  }
  dEscaleList() async{



    client1.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

    var token;
    var username;
    var id ;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.get("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");


    var body
    = {
      "id": 0,
      "token": token,
      "username": username
    };

    HttpClientRequest request = await client1.postUrl(Uri.parse(     baseUrl+ "/demande"+"/escale"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer '+ token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(json.decode(reply));






    return json.decode(reply);

  }

  dashBord() async{
    client1.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);

    var token;
    var username;
    var id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.getString("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");


    var body
    = {
      "id": id,
      "token": token,
      "username": username

    };

    HttpClientRequest request = await client1.postUrl(Uri.parse(  baseUrl+ "/tableaubord/accueil"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer '+ token);

  //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);


    print(json.decode(reply));




      return json.decode(reply);



  }
  traitementFavorable(idD,com) async {
    var token;
    var username;
    var id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token = prefs.getString("survoltoken");
    username = prefs.getString("username");
    id = prefs.getString("identifiant");


    var body
    = {
      "commentaire": com,
      "identifiantDemande": idD,
      "loginDto": {
        "id": id,
        "token": token,
        "username": username
      },
      "typeAction": {
        "tacId": 0,
        "tacLibelle": "string"
      }
    };
    print(body);
    final Map<String, String> headers = {

      'Content-Type': 'application/json',
      //'Accept-Language' :"fr",
      // 'Authorization' : 'Bearer '+ token,

      //   "authorization":"Bearer "+prefs.getString("token"),

    };
    print(headers);


    client1.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client1.postUrl(
        Uri.parse(baseUrl + "/demande" + "/traitementfavorable"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer ' + token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);


    print(json.decode(reply));


    return json.decode(reply);
   /* if (json.decode(reply))


    final response = await client.post(
      baseUrl+ "/demande"+"/traitementfavorable",
      headers:headers,
      body: json.encode(body),
      //    encoding: Encoding.getByName("utf-8")

    );
    if (response.statusCode == 200) {
      print(json.decode(response.body));
      return json.decode(response.body);
    } else {
      print(response.body);
      return 0;
    }*/

  }

  valider(idD,com) async{
    var token;
    var username;
    var id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.getString("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");


    var body
    = {
      "commentaire": com,
      "identifiantDemande": idD,
      "loginDto": {
        "id": id,
        "token": token,
        "username": username
      },
      "typeAction": {
        "tacId": 0,
        "tacLibelle": "string"
      }
    };
    print( body );
    final Map<String, String> headers = {

      'Content-Type' : 'application/json',
      //'Accept-Language' :"fr",
      // 'Authorization' : 'Bearer '+ token,

      //   "authorization":"Bearer "+prefs.getString("token"),

    };
    print(headers );
   /* final response = await client.post(
      baseUrl+ "/demande"+"/valider",
      headers:headers,
      body: json.encode(body),
      //    encoding: Encoding.getByName("utf-8")

    );*/
    client1.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client1.postUrl(Uri.parse(    baseUrl+ "/demande"+"/valider"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer ' + token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);


    print(json.decode(reply));


    return json.decode(reply);
   /* if (response.statusCode == 200) {
      print(json.decode(response.body));
      return json.decode(response.body);
    } else {
      print(response.body);
      return 0;
    }*/

  }

  transmettre(idD,com) async{
    var token;
    var username;
    var id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.getString("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");


    var body
    = {
      "commentaire": com,
      "identifiantDemande": idD,
      "loginDto": {
        "id": id,
        "token": token,
        "username": username
      },
      "typeAction": {
        "tacId": 0,
        "tacLibelle": "string"
      }
    };
    print( body );
    final Map<String, String> headers = {

      'Content-Type' : 'application/json',
      //'Accept-Language' :"fr",
     // 'Authorization' : 'Bearer '+ token,

      //   "authorization":"Bearer "+prefs.getString("token"),

    };
    print(headers );
   /* final response = await client.post(
      baseUrl+ "/demande"+"/transmettre",
      headers:headers,
      body: json.encode(body),
      //    encoding: Encoding.getByName("utf-8")

    );*/

    client1.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client1.postUrl(Uri.parse(     baseUrl+ "/demande"+"/transmettre"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer ' + token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);


    print(json.decode(reply));


    return json.decode(reply);

   /*
    if (response.statusCode == 200) {
      print(json.decode(response.body));
      return json.decode(response.body);
    } else {
      Fluttertoast.showToast(
          msg:json.decode(response.body)["message"],
          toastLength: Toast.LENGTH_SHORT,
       //   gravity: ToastGravity.BOTTOM // also possible "TOP" and "CENTER"
        //  backgroundColor: "#e74c3c",
        //  textColor: '#ffffff'

      );
      print(response.body);
      return 0;
    }*/

  }

  traiterParMAES(idD,com) async{
    var token;
    var username;
    var id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.getString("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");


    var body
    = {
      "commentaire": com,
      "identifiantDemande": idD,
      "loginDto": {
        "id": id,
        "token": token,
        "username": username
      },
      "typeAction": {
        "tacId": 0,
        "tacLibelle": "string"
      }
    };
    print( body );
    final Map<String, String> headers = {

      'Content-Type' : 'application/json',
      //'Accept-Language' :"fr",
      // 'Authorization' : 'Bearer '+ token,

      //   "authorization":"Bearer "+prefs.getString("token"),

    };
    print(headers );
   /* final response = await client.post(
      baseUrl+ "/demande"+"/traiterParMaese",
      headers:headers,
      body: json.encode(body),
      //    encoding: Encoding.getByName("utf-8")

    );*/
    client1.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client1.postUrl(Uri.parse(    baseUrl+ "/demande"+"/traiterParMaese"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer ' + token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);


    print(json.decode(reply));


    return json.decode(reply);
   /* if (response.statusCode == 200) {
      print(json.decode(response.body));
      return json.decode(response.body);
    } else {
      print(response.body);
      return 0;
    }*/

  }
 refuse(idD,com) async{
    var token;
    var username;
    var id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.getString("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");


    var body
    = {
      "commentaire": com,
      "identifiantDemande": idD,
      "loginDto": {
        "id": id,
        "token": token,
        "username": username
      },
      "typeAction": {
        "tacId": 0,
        "tacLibelle": "string"
      }
    };
    print( body );
    final Map<String, String> headers = {

      'Content-Type' : 'application/json',
      'Accept-Language' :"fr",
      'Authorization' : 'Bearer '+ token,

      //   "authorization":"Bearer "+prefs.getString("token"),

    };
    print(headers );
    /*final response = await client.post(
      baseUrl+ "/demande"+"/revokeDemandePermanente",
      headers:headers,
      body: json.encode(body),
      //    encoding: Encoding.getByName("utf-8")

    );*/
    client1.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client1.postUrl(Uri.parse(   baseUrl+ "/demande"+"/revokeDemandePermanente"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer ' + token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);


    print(json.decode(reply));


    return json.decode(reply);
  /*  if (response.statusCode == 200) {
      print(json.decode(response.body));
      return json.decode(response.body);
    } else {
      print(response.statusCode);
      return 0;
    }*/

  }
 refusePrevalider(idD,com) async{
   var token;
   var username;
   var id;
   SharedPreferences prefs = await SharedPreferences.getInstance();
   token=prefs.getString("survoltoken");
   username=prefs.getString("username");
   id=prefs.getString("identifiant");


   var body
   = {
     "commentaire": com,
     "identifiantDemande": idD,
     "loginDto": {
       "id": id,
       "token": token,
       "username": username
     },
     "typeAction": {
       "tacId": 0,
       "tacLibelle": "string"
     }
   };
   print( body );
   final Map<String, String> headers = {

     'Content-Type' : 'application/json',
     'Accept-Language' :"fr",
     'Authorization' : 'Bearer '+ token,

     //   "authorization":"Bearer "+prefs.getString("token"),

   };
   print(headers );
  /* final response = await client.post(
     baseUrl+ "/demande"+"/refusprevalider",
     headers:headers,
     body: json.encode(body),
     //    encoding: Encoding.getByName("utf-8")

   );*/

   client1.badCertificateCallback =
   ((X509Certificate cert, String host, int port) => true);

   HttpClientRequest request = await client1.postUrl(Uri.parse( baseUrl+ "/demande"+"/refusprevalider"));
   request.headers.set('content-type', 'application/json');

   request.headers.set('Authorization', 'Bearer ' + token);

   //  request.headers.set('content-type', 'application/json');

   request.add(utf8.encode(json.encode(body)));

   HttpClientResponse response = await request.close();

   String reply = await response.transform(utf8.decoder).join();

   print(reply);


   print(json.decode(reply));


   return json.decode(reply);
  /* if (response.statusCode == 200) {
     print(json.decode(response.body));
     return json.decode(response.body);
   } else {
     print(response.statusCode);
     return 0;
   }*/

 }

  details(idC) async{
    var token;
    var username;
    var id ;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.get("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");


    var body
    ={
      "identifiantDemande": idC,
      "loginDto": {
        "id":id,
        "token": token,
        "username": username
      }
    }
;
    final Map<String, String> headers = {
      "content-type":"application/json",
      //  "authorization":"Bearer "+prefs.getString("token"),

    };
    print(headers );
   /* final response = await client.post(
      baseUrl+ "/demande"+"/detailsdemande",
      headers:headers,
      body: json.encode(body),
      // encoding: Encoding.getByName("utf-8")

    );*/


    client1.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client1.postUrl(
        Uri.parse(baseUrl+ "/demande"+"/detailsdemande"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer ' + token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);


    print(json.decode(reply));


    return json.decode(reply);
  /*  if (response.statusCode == 200) {
      print(json.decode(response.body));
      return json.decode(response.body);
    } else {
      print(response.statusCode);
      return 0;
    }
*/
  }
  doc(name,type) async{
    var token;
    var username;
    var id ;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.get("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");



    ;
    final Map<String, String> headers = {
      'Content-Type': 'application/pdf',
      'Accept-Language' : "fr",
      'Authorization' : 'Bearer '+token,

      //  "authorization":"Bearer "+prefs.getString("token"),

    };
    client1.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client1.getUrl(Uri.parse(    baseUrl+"/demande"+"/files"+"/download?file=" + name+'&fileType='+type));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer ' + token);

    //  request.headers.set('content-type', 'application/json');

  //  request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);


    print(json.decode(reply));


    return json.decode(reply);
   /* print(headers );
    final response = await client.get(
      baseUrl+"/demande"+"/files"+"/download?file=" + name+'&fileType='+type,
      headers:headers,
     // body: json.encode(body),
      // encoding: Encoding.getByName("utf-8")

    );
    if (response.statusCode == 200) {
      print(json.decode(response.body));
      print(response.statusCode);
      return json.decode(response.body);
    } else {
      print(response.statusCode);
      return 0;
    }*/

  }




  validationMaese(idD,com) async{
    var token;
    var username;
    var id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.getString("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");


    var body
    = {
      "commentaire": com,
      "identifiantDemande": idD,
      "loginDto": {
        "id": id,
        "token": token,
        "username": username
      },
      "typeAction": {
        "tacId": 0,
        "tacLibelle": "string"
      }
    };
    print( body );
    final Map<String, String> headers = {

      'Content-Type' : 'application/json',
      //'Accept-Language' :"fr",
      // 'Authorization' : 'Bearer '+ token,

      //   "authorization":"Bearer "+prefs.getString("token"),

    };

    client1.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client1.postUrl(Uri.parse(   baseUrl+ "/demande"+"/valider"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer ' + token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);


    print(json.decode(reply));


    return json.decode(reply);
   /* print(headers );
    final response = await client.post(
      baseUrl+ "/demande"+"/valider",
      headers:headers,
      body: json.encode(body),
      //    encoding: Encoding.getByName("utf-8")

    );
    if (response.statusCode == 200) {
      print(json.decode(response.body));
      return json.decode(response.body);
    } else {
      print(response.body);
      return 0;
    }*/

  }




  delivranceMaese(idD,com) async{
    var token;
    var username;
    var id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.getString("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");


    var body
    = {
      "commentaire": com,
      "identifiantDemande": idD,
      "loginDto": {
        "id": id,
        "token": token,
        "username": username
      },
      "typeAction": {
        "tacId": 0,
        "tacLibelle": "string"
      }
    };
    print( body );
    final Map<String, String> headers = {

      'Content-Type' : 'application/json',
      //'Accept-Language' :"fr",
      // 'Authorization' : 'Bearer '+ token,

      //   "authorization":"Bearer "+prefs.getString("token"),

    };

    client1.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client1.postUrl(Uri.parse(  baseUrl+ "/demande/delivrer"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer ' + token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);


    print(json.decode(reply));


    return json.decode(reply);
  /*  print(headers );
    final response = await client.post(
      baseUrl+ "/demande/delivrer",
      headers:headers,
      body: json.encode(body),
      //    encoding: Encoding.getByName("utf-8")

    );
    if (response.statusCode == 200) {
      print(json.decode(response.body));
      return json.decode(response.body);
    } else {
      print(response.body);
      return 0;
    }*/

  }

 invalider(idD,com) async{
   var token;
   var username;
   var id;
   SharedPreferences prefs = await SharedPreferences.getInstance();
   token=prefs.getString("survoltoken");
   username=prefs.getString("username");
   id=prefs.getString("identifiant");


   var body
   = {
     "commentaire": com,
     "identifiantDemande": idD,
     "loginDto": {
       "id": id,
       "token": token,
       "username": username
     },
     "typeAction": {
       "tacId": 0,
       "tacLibelle": "string"
     }
   };
   print( body );
   final Map<String, String> headers = {

     'Content-Type' : 'application/json',
     //'Accept-Language' :"fr",
     // 'Authorization' : 'Bearer '+ token,

     //   "authorization":"Bearer "+prefs.getString("token"),

   };
   client1.badCertificateCallback =
   ((X509Certificate cert, String host, int port) => true);

   HttpClientRequest request = await client1.postUrl(Uri.parse( baseUrl+ "/demande"+"/invalider"));
   request.headers.set('content-type', 'application/json');

   request.headers.set('Authorization', 'Bearer ' + token);

   //  request.headers.set('content-type', 'application/json');

   request.add(utf8.encode(json.encode(body)));

   HttpClientResponse response = await request.close();

   String reply = await response.transform(utf8.decoder).join();

   print(reply);


   print(json.decode(reply));


   return json.decode(reply);
  /* print(headers );
   final response = await client.post(
     baseUrl+ "/demande"+"/invalider",
     headers:headers,
     body: json.encode(body),
     //    encoding: Encoding.getByName("utf-8")

   );
   if (response.statusCode == 200) {
     print(json.decode(response.body));
     return json.decode(response.body);
   } else {
     print(response.body);
     return 0;
   }*/

 }

 miseEnAttente(idD,com) async{
   var token;
   var username;
   var id;
   SharedPreferences prefs = await SharedPreferences.getInstance();
   token=prefs.getString("survoltoken");
   username=prefs.getString("username");
   id=prefs.getString("identifiant");


   var body
   = {
     "commentaire": com,
     "identifiantDemande": idD,
     "loginDto": {
       "id": id,
       "token": token,
       "username": username
     },
     "typeAction": {
       "tacId": 0,
       "tacLibelle": "string"
     }
   };
   print( body );
   final Map<String, String> headers = {

     'Content-Type' : 'application/json',
     //'Accept-Language' :"fr",
     // 'Authorization' : 'Bearer '+ token,

     //   "authorization":"Bearer "+prefs.getString("token"),

   };
   client1.badCertificateCallback =
   ((X509Certificate cert, String host, int port) => true);

   HttpClientRequest request = await client1.postUrl(Uri.parse(   baseUrl+ "/demande"+"/invalider"));
   request.headers.set('content-type', 'application/json');

   request.headers.set('Authorization', 'Bearer ' + token);

   //  request.headers.set('content-type', 'application/json');

   request.add(utf8.encode(json.encode(body)));

   HttpClientResponse response = await request.close();

   String reply = await response.transform(utf8.decoder).join();

   print(reply);


   print(json.decode(reply));


   return json.decode(reply);
  /* print(headers );
   final response = await client.post(
     baseUrl+ "/demande"+"/invalider",
     headers:headers,
     body: json.encode(body),
     //    encoding: Encoding.getByName("utf-8")

   );
   if (response.statusCode == 200) {
     print(json.decode(response.body));
     return json.decode(response.body);
   } else {
     print(response.body);
     return 0;
   }*/

 }

  traitementDefavorable(idD,com) async{
    var token;
    var username;
    var id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.getString("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");


    var body
    = {
      "commentaire": com,
      "identifiantDemande": idD,
      "loginDto": {
        "id": id,
        "token": token,
        "username": username
      },
      "typeAction": {
        "tacId": 0,
        "tacLibelle": "string"
      }
    };
    print( body );
    final Map<String, String> headers = {

      'Content-Type' : 'application/json',
      //'Accept-Language' :"fr",
      // 'Authorization' : 'Bearer '+ token,

      //   "authorization":"Bearer "+prefs.getString("token"),

    };
    client1.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client1.postUrl(Uri.parse(  baseUrl+ "/demande"+"/traitementdefavorable"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer ' + token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);


    print(json.decode(reply));


    return json.decode(reply);
 /*   final response = await client.post(
      baseUrl+ "/demande"+"/traitementdefavorable",
      headers:headers,
      body: json.encode(body),
      //    encoding: Encoding.getByName("utf-8")

    );
    if (response.statusCode == 200) {
      print(json.decode(response.body));
      return json.decode(response.body);
    } else {
      print(response.body);
      return 0;
    }*/

  }

  traitementInvalide(idD,com) async{
    var token;
    var username;
    var id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.getString("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");
    var body = {
      "commentaire": com,
      "identifiantDemande": idD,
      "loginDto": {
        "id": id,
        "token": token,
        "username": username
      },
      "typeAction": {
        "tacId": 0,
        "tacLibelle": "string"
      }
    };
    print( body );
    final Map<String, String> headers = {

      'Content-Type' : 'application/json',
      //'Accept-Language' :"fr",
      // 'Authorization' : 'Bearer '+ token,

      //   "authorization":"Bearer "+prefs.getString("token"),

    };
    client1.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await client1.postUrl(Uri.parse(  baseUrl+ "/demande"+"/invalider"));
    request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', 'Bearer ' + token);
    //  request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(body)));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    print(reply);
    print(json.decode(reply));
    return json.decode(reply);
    /*final response = await client.post(
      baseUrl+ "/demande"+"/invalider",
      headers:headers,
      body: json.encode(body),
      //    encoding: Encoding.getByName("utf-8")

    );
    if (response.statusCode == 200) {
      print(json.decode(response.body));
      return json.decode(response.body);
    } else {
      print(response.body);
      return 0;
    }*/

  }

  refusPrevalider(idD,com) async{
    var token;
    var username;
    var id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.getString("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");

    var body
    = {
      "commentaire": com,
      "identifiantDemande": idD,
      "loginDto": {
        "id": id,
        "token": token,
        "username": username
      },
      "typeAction": {
        "tacId": 0,
        "tacLibelle": "string"
      }
    };
    print( body );
    final Map<String, String> headers = {

      'Content-Type' : 'application/json',
      //'Accept-Language' :"fr",
      // 'Authorization' : 'Bearer '+ token,
      //   "authorization":"Bearer "+prefs.getString("token"),

    };
    print(headers );
    client1.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client1.postUrl(Uri.parse( baseUrl+ "/demande"+"/refusprevalider"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer ' + token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);


    print(json.decode(reply));


    return json.decode(reply);
  /*  final response = await client.post(
      baseUrl+ "/demande"+"/refusprevalider",
      headers:headers,
      body: json.encode(body),
      //    encoding: Encoding.getByName("utf-8")

    );
    if (response.statusCode == 200) {
      print(json.decode(response.body));
      return json.decode(response.body);
    } else {
      print(response.body);
      return 0;
    }*/

  }

  prevalider(idD,com) async{
    var token;
    var username;
    var id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.getString("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");

    var body
    = {
      "commentaire": com,
      "identifiantDemande": idD,
      "loginDto": {
        "id": id,
        "token": token,
        "username": username
      },
      "typeAction": {
        "tacId": 0,
        "tacLibelle": "string"
      }
    };
    print( body );
    final Map<String, String> headers = {

      'Content-Type' : 'application/json',
      //'Accept-Language' :"fr",
      // 'Authorization' : 'Bearer '+ token,

      //   "authorization":"Bearer "+prefs.getString("token"),

    };
    print(headers );
   /* final response = await client.post(
      baseUrl+ "/demande"+"/prevalider",
      headers:headers,
      body: json.encode(body),
      //    encoding: Encoding.getByName("utf-8")

    );*/
    client1.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client1.postUrl(Uri.parse( baseUrl+ "/demande"+"/prevalider"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer ' + token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);


    print(json.decode(reply));


    return json.decode(reply);


    /*
    if (response.statusCode == 200) {
      print(json.decode(response.body));
      return json.decode(response.body);
    } else {
      print(response.body);
      return 0;
    }*/

  }



  commenterSuivi(idD,com) async{
    var token;
    var username;
    var id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.getString("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");
    var body
    = {
      "commentaire": com,
      "identifiantDemande": idD,
      "loginDto": {
        "id": id,
        "token": token,
        "username": username
      },
      "typeAction": {
        "tacId": 0,
        "tacLibelle": "string"
      }
    };
    print( body );
    final Map<String, String> headers = {

      'Content-Type' : 'application/json',
      //'Accept-Language' :"fr",
      // 'Authorization' : 'Bearer '+ token,

      //   "authorization":"Bearer "+prefs.getString("token"),

    };
    print(headers );
   /* final response = await client.post(
      baseUrl+ "/demande"+"/commenter",
      headers:headers,
      body: json.encode(body),
      //    encoding: Encoding.getByName("utf-8")

    );*/

    client1.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client1.postUrl(Uri.parse( baseUrl+ "/demande"+"/commenter"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer ' + token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);


    print(json.decode(reply));


    return json.decode(reply);

   /* if (response.statusCode == 200) {
      print(json.decode(response.body));
      return json.decode(response.body);
    } else {
      print(response.body);
      return 0;
    }
*/
  }




  retournerDemande(idD,com) async{
    var token;
    var username;
    var id;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token=prefs.getString("survoltoken");
    username=prefs.getString("username");
    id=prefs.getString("identifiant");


    var body
    = {
      "commentaire": com,
      "identifiantDemande": idD,
      "loginDto": {
        "id": id,
        "token": token,
        "username": username
      },
      "typeAction": {
        "tacId": 0,
        "tacLibelle": "string"
      }
    };
    print( body );
    final Map<String, String> headers = {

      'Content-Type' : 'application/json',
      //'Accept-Language' :"fr",
      // 'Authorization' : 'Bearer '+ token,

      //   "authorization":"Bearer "+prefs.getString("token"),

    };
    print(headers );
   /* final response = await client.post(
      baseUrl+ "/demande"+"/retourner",
      headers:headers,
      body: json.encode(body),
      //    encoding: Encoding.getByName("utf-8")

    );*/

    client1.badCertificateCallback =
    ((X509Certificate cert, String host, int port) => true);

    HttpClientRequest request = await client1.postUrl(Uri.parse(  baseUrl+ "/demande"+"/retourner"));
    request.headers.set('content-type', 'application/json');

    request.headers.set('Authorization', 'Bearer ' + token);

    //  request.headers.set('content-type', 'application/json');

    request.add(utf8.encode(json.encode(body)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print(reply);


    print(json.decode(reply));


    return json.decode(reply);
  /*  if (response.statusCode == 200) {
      print(json.decode(response.body));
      return json.decode(response.body);
    } else {
      Fluttertoast.showToast(
        msg:json.decode(response.body)["message"],
        toastLength: Toast.LENGTH_SHORT,
        //   gravity: ToastGravity.BOTTOM // also possible "TOP" and "CENTER"
        //  backgroundColor: "#e74c3c",
        //  textColor: '#ffffff'

      );
      print(response.body);
      return 0;
    }*/

  }





 listAuto() async{
   var token;
   var username;
   var id ;
   SharedPreferences prefs = await SharedPreferences.getInstance();
   token=prefs.get("survoltoken");
   username=prefs.getString("username");
   id=prefs.getString("identifiant");


   var body
   = {
     "id": 0,
     "token": token,
     "username": username

   };
   final Map<String, String> headers = {
     "content-type":"application/json",
     //  "authorization":"Bearer "+prefs.getString("token"),

   };
   print(headers );
  /* final response = await client.post(
     baseUrl+ "/demande"+"/autorisations",
     headers:headers,
     body: json.encode(body),
     // encoding: Encoding.getByName("utf-8")

   );*/
   client1.badCertificateCallback =
   ((X509Certificate cert, String host, int port) => true);

   HttpClientRequest request = await client1.postUrl(Uri.parse(   baseUrl+ "/demande"+"/autorisations"));
   request.headers.set('content-type', 'application/json');

   request.headers.set('Authorization', 'Bearer ' + token);

   //  request.headers.set('content-type', 'application/json');

   request.add(utf8.encode(json.encode(body)));

   HttpClientResponse response = await request.close();

   String reply = await response.transform(utf8.decoder).join();

   print(reply);


   print(json.decode(reply));


   return json.decode(reply);
  /* if (response.statusCode == 200) {
     print(json.decode(response.body));
     return json.decode(response.body);
   } else {
     print(response.statusCode);
     return 0;
   }*/

 }


 Future<dynamic> notify(body) async {

   SharedPreferences prefs =await SharedPreferences.getInstance();
   var token  = prefs.getString("token");


   final http.Response response = await http.Client().post(
     Uri.parse("https://fcm.googleapis.com/fcm/send"),
     body: json.encode(body),
     headers: {
       'Content-Type': 'application/json',
       //'Accept': 'application/json',
       'Authorization': "key=AAAA0xhrvlE:APA91bEJfxJjLAk3RHFqQDgs48EWYkmqq_mbtZ8643fAU33astuOftXgNeIhAIDIKeESSMPmsFoQNYUU6w4CFtO7QR9tslqAq3SAJKvIIakDHrKbIrwaIVZDW5ofU4zAYe6JIfuus-Z7",
     },
   );
   print("response.body" );
   print(response.body );
   if (response.statusCode >= 400) {
     /* Fluttertoast.showToast(
         msg: json.decode(response.body)["message"],
         toastLength: Toast.LENGTH_SHORT,
         gravity: ToastGravity.CENTER,
         timeInSecForIosWeb: 1,
         backgroundColor: Colors.black,
         // backgroundColor: Colors.red,
         // textColor: Colors.white,
         fontSize: 16.0
     );*/
     return "err";
     throw ('An error occurred: ' + response.body);
   }}

}



/*
 let logInfo = {
      "identifiantDemande": parseInt(this.idDemande),
      "loginDto": {
        "id": parseInt(this.idUser),
        "token": this.token,
        "username": this.login
      }
    }

 */