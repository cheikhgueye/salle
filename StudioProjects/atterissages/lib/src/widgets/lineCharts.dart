
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;



class MyHomePage extends StatefulWidget {
  final dp;
  final dpo;
  final dpr;
  final de;
  final ist;
  MyHomePage({Key? key, this.dp, this.dpo, this.dpr, this.de, this.ist}) : super(key: key);




  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class ClicksPerYear {
  final String year;
  final int clicks;
  final charts.Color color;

  ClicksPerYear(this.year, this.clicks, Color color)
      : this.color = charts.Color(
      r: color.red, g: color.green, b: color.blue, a: color.alpha);
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    var data = [
      ClicksPerYear( widget.ist?'Dm Pemanantes':"Pemanantes", int.parse(widget.dp), Color(0xfff9e0cb),),
      ClicksPerYear( widget.ist?'Dm Ponctuelles':"Ponctuelles",int.parse(widget.dpo) , Color(0xffe7b7cc)),
      ClicksPerYear( widget.ist?'Dm Préavis':"Préavis", int.parse(widget.dpr), Color(0xff77cbbe)),
      ClicksPerYear( widget.ist?'Dm Escales':"Escales", int.parse(widget.de), Color(0xffd23aed)),
    ];

    var series = [
      charts.Series(

        domainFn: (ClicksPerYear clickData, _) => clickData.year,
        measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
        colorFn: (ClicksPerYear clickData, _) => clickData.color,
        id: 'Clicks',
        data: data,
      ),
    ];

    var chart = charts.BarChart(
      series,
      animate: true,


    );

    var chartWidget = Padding(
      padding: EdgeInsets.all( widget.ist? 32.0:15),
      child: SizedBox(

        height: widget.ist? 390.0:250,
        child: chart,
      ),
    );

    return Scaffold(
      body: Center(
        child: Column(
         mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
        //    Text(''),
        //    Text('$_counter', style: Theme.of(context).textTheme.display1),
            chartWidget,
          ],
        ),
      ),

    );
  }
}