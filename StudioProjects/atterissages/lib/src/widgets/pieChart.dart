

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

import 'indicator.dart';

class PieChartSample2 extends StatefulWidget {
  final dp;
  final dpo;
  final dpr;
  final de;
  final ist;

  const PieChartSample2({Key? key, this.dp, this.dpo, this.dpr, this.de, this.ist}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _PieChart2State();
}

class _PieChart2State extends State<PieChartSample2>  {
  int? touchedIndex;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.3,
      child: Card(
        color: Colors.white,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            const SizedBox(
             // height: 18,
            ),
        Container(
          margin: EdgeInsets.only(top: 30),
          padding: EdgeInsets.all(10),
          child:
              widget.ist?
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,

              children: const <Widget>[
                Indicator(
                  color: Color(0xfff9e0cb),
                  text: 'Demandes Permanentes',
                  isSquare: true,
                ),
                SizedBox(
                  height: 4,
                ),
                Indicator(
                  color: Color(0xffe7b7cc),
                  text: 'Demandes Ponctuelles',
                  isSquare: true,
                ),
                SizedBox(
                  height: 4,
                ),
                Indicator(
                  color: Color(0xff77cbbe),
                  text: 'Demandes Préavis',
                  isSquare: true,
                ),
                SizedBox(
                  height: 4,
                ),
                Indicator(
                  color: Color(0xffd23aed),
                  text: 'Demandes Escales',
                  isSquare: true,
                ),

                SizedBox(
                  height: 18,
                ),
              ],
            ):  Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,

                children: const <Widget>[
                  Indicator(
                    color: Color(0xfff9e0cb),
                    text: 'Demandes Permanentes',
                    isSquare: true,
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Indicator(
                    color: Color(0xffe7b7cc),
                    text: 'Demandes Ponctuelles',
                    isSquare: true,
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Indicator(
                    color: Color(0xff77cbbe),
                    text: 'Demandes Préavis',
                    isSquare: true,
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Indicator(
                    color: Color(0xffd23aed),
                    text: 'Demandes Escales',
                    isSquare: true,
                  ),

                  SizedBox(
                    height: 8,
                  ),
                ],
              )



        ),
            const SizedBox(
              width: 0,
            ),
            Expanded(
              child: AspectRatio(
                aspectRatio: 1/9,
                child: PieChart(
                  PieChartData(

                      pieTouchData: PieTouchData(touchCallback: (x,pieTouchResponse) {
                        setState(() {
                          if (pieTouchResponse!.touchedSection is FlLongPressEnd ||
                              pieTouchResponse.touchedSection is FlPanEndEvent) {

                            touchedIndex = -1;
                          } else {
                            touchedIndex = pieTouchResponse.touchedSection!.touchedSectionIndex;
                          }
                        });
                      }),
                      borderData: FlBorderData(

                        show: false,
                      ),
                      sectionsSpace: 0,
                      centerSpaceRadius:widget.ist? 100:30,


                      sections: showingSections(double.parse(widget.dp,),double.parse(widget.dpo,),double.parse(widget.dpr,),double.parse(widget.de))),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<PieChartSectionData> showingSections(dp,dpo,dpr,de) {
    return List.generate(4, (i) {
      final isTouched = i == touchedIndex;
      final double fontSize = isTouched ? 25 : 16;
      final double radius = isTouched ? 60 : 50;
      switch (i) {
        case 0:
          return PieChartSectionData(
            color: const Color(0xfff9e0cb),
            value: dp,
            title:  ((dp/(dpr+dp+de+dpo))*100).toStringAsFixed(0)+"%",
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        case 1:
          return PieChartSectionData(
            color: const  Color(0xff77cbbe),
            value: dpr,
            title: ((dpr/(dpr+dp+de+dpo))*100).toStringAsFixed(0)+"%",
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        case 2:
          return PieChartSectionData(
            color: const Color(0xffe7b7cc),
            value: dpo,
            title: ((dpo/(dpr+dp+de+dpo))*100).toStringAsFixed(0)+"%",
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        case 3:
          return PieChartSectionData(
            color: const  Color(0xffd23aed),
            value: de,
            title:  ((de/(dpr+dp+de+dpo))*100).toStringAsFixed(0)+"%",
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        default:
          return PieChartSectionData(
            color: const  Color(0xffd23aed),
            value: de,
            title:  ((de/(dpr+dp+de+dpo))*100).toStringAsFixed(0)+"%",
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
      }
    });
  }
}