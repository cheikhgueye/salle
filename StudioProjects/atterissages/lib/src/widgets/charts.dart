

import 'package:atterissages/src/widgets/indicator.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class LineChartSample1 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LineChartSample1State();
}

class LineChartSample1State extends State<LineChartSample1> {
  bool? isShowingMainData;

  @override
  void initState() {
    super.initState();
    isShowingMainData = false;
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.23,
      child: Container(
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(18)),
          gradient: LinearGradient(
            colors: [
            Colors.white,
           Colors.white
            ],
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
          ),
        ),
        child: Stack(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                const SizedBox(
                  height: 37,
                ),
                const Text(
                  '',
                  style: TextStyle(
                    color: Color(0xff827daa),
                    fontSize: 16,
                  ),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(
                  height: 4,
                ),
                const Text(
                  '',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 32,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 2),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(
                  height: 37,
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 16.0, left: 6.0),
                    child: LineChart(
                      isShowingMainData! ? sampleData1() : sampleData2(),
                      swapAnimationDuration: const Duration(milliseconds: 250),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
         /*   IconButton(
              icon: Icon(
                Icons.refresh,
                color: Theme.of(context).primaryColor
                //.withOpacity(isShowingMainData ? 1.0 : 0.5),
              ),
              onPressed: () {
                setState(() {
               //   isShowingMainData = !isShowingMainData;
                });
              },

            ),
*/
           Container(
             margin: EdgeInsets.only(top: 30),
          padding: EdgeInsets.all(10),
          child:   Row(
               mainAxisAlignment: MainAxisAlignment.spaceBetween,

               children: const <Widget>[
                 Indicator(
                   color: Color(0xfff9e0cb),
                   text: 'Demandes Permanentes',
                   isSquare: true,
                 ),
                 SizedBox(
                   height: 4,
                 ),
                 Indicator(
                   color: Color(0xffe7b7cc),
                   text: 'Demandes Ponctuelles',
                   isSquare: true,
                 ),
                 SizedBox(
                   height: 4,
                 ),
                 Indicator(
                   color: Color(0xff77cbbe),
                   text: 'Demandes Préavis',
                   isSquare: true,
                 ),
                 SizedBox(
                   height: 4,
                 ),
                 Indicator(
                   color: Color(0xffd23aed),
                   text: 'Demandes Escales',
                   isSquare: true,
                 ),

                 SizedBox(
                   height: 18,
                 ),
               ],
             ),
           )
          ],
        ),
      ),
    );
  }

  LineChartData sampleData1() {
    return LineChartData(
      lineTouchData: LineTouchData(
        touchTooltipData: LineTouchTooltipData(
          tooltipBgColor: Colors.blueGrey.withOpacity(0.8),
        ),
       // touchCallback: (LineTouchResponse touchResponse) {},
        handleBuiltInTouches: true,
      ),
      gridData: FlGridData(
        show: false,
      ),
      titlesData: FlTitlesData(
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          getTextStyles: (value,f) => const TextStyle(
            color: Color(0xff72719b),
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
          margin: 10,
          getTitles: (value) {
            switch (value.toInt()) {
              case 2:
                return 'SEPT';
              case 7:
                return 'OCT';
              case 12:
                return 'DEC';
            }
            return '';
          },
        ),
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (value,f) => const TextStyle(
            color: Color(0xff75729e),
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
          getTitles: (value) {
            switch (value.toInt()) {
              case 1:
                return '1m';
              case 2:
                return '2m';
              case 3:
                return '3m';
              case 4:
                return '5m';
            }
            return '';
          },
          margin: 8,
          reservedSize: 30,
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: const Border(
          bottom: BorderSide(
            color: Color(0xff4e4965),
            width: 4,
          ),
          left: BorderSide(
            color: Colors.transparent,
          ),
          right: BorderSide(
            color: Colors.transparent,
          ),
          top: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
      minX: 0,
      maxX: 14,
      maxY: 4,
      minY: 0,
      lineBarsData: linesBarData1(),
    );
  }

  List<LineChartBarData> linesBarData1() {
    final LineChartBarData lineChartBarData1 = LineChartBarData(
      spots: [
        FlSpot(1, 1),
        FlSpot(3, 1.5),
        FlSpot(5, 1.4),
        FlSpot(7, 3.4),
        FlSpot(10, 2),
        FlSpot(12, 2.2),
        FlSpot(13, 1.8),
      ],
      isCurved: true,
      colors: [
        const Color(0xff4af699),
      ],
      barWidth: 8,
      isStrokeCapRound: true,
      dotData: FlDotData(
        show: false,
      ),
      belowBarData: BarAreaData(
        show: false,
      ),
    );
    final LineChartBarData lineChartBarData2 = LineChartBarData(
      spots: [
        FlSpot(1, 1),
        FlSpot(3, 2.8),
        FlSpot(7, 1.2),
        FlSpot(10, 2.8),
        FlSpot(12, 2.6),
        FlSpot(13, 3.9),
      ],
      isCurved: true,
      colors: [
        const Color(0xffaa4cfc),
      ],
      barWidth: 8,
      isStrokeCapRound: true,
      dotData: FlDotData(
        show: false,
      ),
      belowBarData: BarAreaData(show: false, colors: [
        const Color(0x00aa4cfc),
      ]),
    );
    final LineChartBarData lineChartBarData3 = LineChartBarData(
      spots: [
        FlSpot(1, 2.8),
        FlSpot(3, 1.9),
        FlSpot(6, 3),
        FlSpot(10, 1.3),
        FlSpot(13, 2.5),
      ],
      isCurved: true,
      colors: const [
        Color(0xff27b6fc),
      ],
      barWidth: 8,
      isStrokeCapRound: true,
      dotData: FlDotData(
        show: false,
      ),
      belowBarData: BarAreaData(
        show: false,
      ),
    );
    return [
      lineChartBarData1,
      lineChartBarData2,
      lineChartBarData3,
    ];
  }

  LineChartData sampleData2() {
    return LineChartData(
      lineTouchData: LineTouchData(
        enabled: true,
      ),
      gridData: FlGridData(
        show: true,
      ),
      titlesData: FlTitlesData(
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          getTextStyles: (value,f) => const TextStyle(
            color: Color(0xff72719b),
            fontWeight: FontWeight.bold,
            fontSize: 9,
          ),
          margin: 10,
          getTitles: (value) {
            switch (value.toInt()) {
              case 1:
                return 'Janvier';
              case 2:
                return 'Fevrier';
              case 3:
                return 'Mars';
              case 4:
                return 'Avril';
              case 5:
                return 'Mai';
              case 6:
                return 'Juin';
              case 7:
                return 'Juillet';
              case 8:
                return 'Aout';
              case 9:
                return 'Septembre';
              case 10:
                return 'Octobre';
              case 11:
                return 'Novembre';
              case 12:
                return 'Décembre';
            }
            return '';
          },
        ),
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (value,f) => const TextStyle(
            color: Color(0xff75729e),
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
          getTitles: (value) {
            switch (value.toInt()) {
              case 1:
                return '1.0';
              case 2:
                return '2.0';
              case 3:
                return '3.0';
              case 4:
                return '5.0';
              case 5:
                return '6.0';
            }
            return '';
          },
          margin: 8,
          reservedSize: 30,
        ),
      ),
      borderData: FlBorderData(
          show: true,
          border: const Border(
            bottom: BorderSide(
              color: Color(0xff4e4965),
              width: 4,
            ),
            left: BorderSide(
              color: Colors.transparent,
            ),
            right: BorderSide(
              color: Colors.transparent,
            ),
            top: BorderSide(
              color: Colors.transparent,
            ),
          )),
      minX: 0,
      maxX: 14,
      maxY: 6,
      minY: 0,
      lineBarsData: linesBarData2(),
    );
  }

  List<LineChartBarData> linesBarData2() {
    return [
      LineChartBarData(
        spots: [
          FlSpot(1, 1),
          FlSpot(2, 4),
          FlSpot(3, 1.8),
          FlSpot(4, 5),
          FlSpot(5, 2),
          FlSpot(6, 2.2),
          FlSpot(7, 1.8),
          FlSpot(8, 4),
          FlSpot(9, 1.8),
          FlSpot(10, 5),
          FlSpot(11, 2),
          FlSpot(12, 2.2),
        ],
        isCurved: true,
        curveSmoothness: 0,
        colors: const [
          Color(0xfff9e0cb),
        ],
        barWidth: 4,
        isStrokeCapRound: true,
        dotData: FlDotData(
          show: true,
        ),
        belowBarData: BarAreaData(show: false, colors: [
          const Color(0xfff9e0cb),
        ]),
      ),
      LineChartBarData(
        spots: [
          FlSpot(1, 1),
          FlSpot(2, 2.8),
          FlSpot(3, 1.2),
          FlSpot(4, 2.8),
          FlSpot(5, 2.6),
          FlSpot(6, 3.9),
          FlSpot(7, 1),
          FlSpot(8, 2.8),
          FlSpot(9, 2.2),
          FlSpot(10, 2.8),
          FlSpot(11, 5.6),
          FlSpot(12, 4.9),

        ],
        isCurved: false,
        colors: const [
          Color(0xffe7b7cc),
        ],
        barWidth: 4,
        isStrokeCapRound: true,
        dotData: FlDotData(
          show: true,
        ),
        belowBarData: BarAreaData(show: false, colors: [
          const Color(0xffe7b7cc),
        ]),
      ),
      LineChartBarData(
        spots: [
          FlSpot(1, 3.8),
          FlSpot(2, 1.9),
          FlSpot(3, 5),
          FlSpot(4, 3.3),
          FlSpot(5, 4.5),
          FlSpot(6, 3.8),
          FlSpot(7, 1.9),
          FlSpot(8, 5),
          FlSpot(9, 3.3),
          FlSpot(10, 4.5),
          FlSpot(11, 4.5),
          FlSpot(12, 4.5),


        ],
        isCurved: true,
        curveSmoothness: 0,
        colors: const [
          Color(0xff77cbbe),
        ],
        barWidth: 2,
        isStrokeCapRound: true,
        dotData: FlDotData(show: true),
        belowBarData: BarAreaData(show: false, colors: [
          const Color(0xff77cbbe),
        ]),
      ),
      LineChartBarData(
        spots: [
          FlSpot(1, 4.8),
          FlSpot(2, 0.9),
          FlSpot(3, 1),
          FlSpot(4, 0.3),
          FlSpot(5, 0.5),
          FlSpot(6, 0.5),
          FlSpot(7, 0.5),
          FlSpot(8, 0.5),
          FlSpot(9, 0.5),
          FlSpot(10, 0.5),
          FlSpot(11, 0.5),
          FlSpot(12, 0.5),
        ],
        isCurved: true,
        curveSmoothness: 0,
        colors: const [
          Color(0xffaa4cfc),
        ],
        barWidth: 2,
        isStrokeCapRound: true,
        dotData: FlDotData(show: true),
        belowBarData: BarAreaData(show: false, colors: [
          const Color(0xffaa4cfc),
        ]),
      ),
    ];
  }
}