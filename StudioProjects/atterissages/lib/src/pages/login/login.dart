


import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';
import 'package:alert_dialog/alert_dialog.dart';
import 'package:atterissages/src/pages/accueil/home.dart';
import 'package:atterissages/src/pages/dasbord/dashbord.dart';
import 'package:atterissages/src/ressources/orbusApiProvider.dart';
import 'package:awesome_loader/awesome_loader.dart';
//import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
/**
 * Author: Sudip Thapa
 * profile: https://github.com/sudeepthapa
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_alert/flutter_alert.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:ftoast/ftoast.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'package:progress_dialog/progress_dialog.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:shared_preferences/shared_preferences.dart';
class Login extends StatefulWidget {
  static final String path = "lib/src/pages/login/login7.dart";
  @override
  _LoginSevenPageState createState() => _LoginSevenPageState();
}

class _LoginSevenPageState extends State<Login> {
//  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  var onTapRecognizer;

  TextEditingController textEditingController = TextEditingController();
  // ..text = "123456";

  StreamController<ErrorAnimationType>? errorController;
  final api = OrbusApiProvider ();
  String? _token;
  String? username;
  String? password;
  var loader=false;
  SharedPreferences? prefs;
  bool isLoading = false;
  bool isLoggedIn = false;
  final storage = new FlutterSecureStorage();
  var _passwordVisible=false;
  var otp=false;
  var otpText="";
  initTopic () async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var code= prefs.getString("codeProfil");
    var entite=prefs.getString("libelleEntite");
    print("entite++code");
    print(entite!.trim()+'_'+code!.trim());
 //   _firebaseMessaging.unsubscribeFromTopic(entite.trim()+'_'+code.trim());
    prefs.clear();

  }
  initTopic1 () async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var code= prefs.getString("codeProfil");
    var entite=prefs.getString("libelleEntite");
    print("entite++code");
    print(entite!.trim()+'_'+code!.trim());
  //  _firebaseMessaging.subscribeToTopic(entite.trim()+'_'+code.trim());


  }
  @override
  void initState() {
    username="";
    password="";
    initTopic ();

    super.initState();
  }

  @override
  void dispose() {
    textEditingController.dispose();
    super.dispose();
  }
signIn() async {

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        setState(() {
          loader=true;
        });
        var data = await api.signin(username, password);
        print("data");
        print(data);
     /*   print( data);
        if(data["status"]=="false"){
          setState(() {
             loader=false;
          });

          FToast.toast(
            context,
            msg: data["message"],

            /// 配置 subMsg
            ///
            /// set subMsg
            // subMsg: data,
            image: Icon(
              Icons.error,
              color: Colors.red,
            ),


            /// 配置图标相对文本的位置
            ///
            /// set position of icon relative to text


            /// 配置 SubMsg 样式
            ///
            /// set SubMsg style
            subMsgStyle: TextStyle(color: Colors.white, fontSize: 13),


          );

          Navigator.push(context, MaterialPageRoute(
              builder: (_) =>     Login ()
          ));
        }else{
          await initTopic1 ();
          navi();
        }
        print(data);*/
       if(data["status"]=="true"){
          setState(() {
              otp=true;
          });
        }else{
          FToast.toast(
            context,
            msg: data["message"],

            /// 配置 subMsg
            ///
            /// set subMsg
            // subMsg: data,
            image: Icon(
              Icons.error,
              color: Colors.red,
            ),


            /// 配置图标相对文本的位置
            ///
            /// set position of icon relative to text


            /// 配置 SubMsg 样式
            ///
            /// set SubMsg style
            subMsgStyle: TextStyle(color: Colors.white, fontSize: 13),


          );
        }
        print(data["status"]);


        setState(() {
       //   otp=true;
        });
      }
    } on SocketException catch (_) {
      FToast.toast(
        context,
        msg: "Probleme de connexion!",

        /// 配置 subMsg
        ///
        /// set subMsg
        // subMsg: data,
        image: Icon(
          Icons.error,
          color: Colors.red,
        ),


        /// 配置图标相对文本的位置
        ///
        /// set position of icon relative to text


        /// 配置 SubMsg 样式
        ///
        /// set SubMsg style
        subMsgStyle: TextStyle(color: Colors.white, fontSize: 13),


      );
    }


}
  verifOtp(otp1) async {
 
    var data = await api.verifOtp(otp1,username);
    print(data);
    if(data==0){
      setState(() {
       // loader=false;
      });

      FToast.toast(
        context,
        msg: "Code OTP incorrecte!",

        /// 配置 subMsg
        ///
        /// set subMsg
        // subMsg: data,
        image: Icon(
          Icons.error,
          color: Colors.red,
        ),


        /// 配置图标相对文本的位置
        ///
        /// set position of icon relative to text


        /// 配置 SubMsg 样式
        ///
        /// set SubMsg style
        subMsgStyle: TextStyle(color: Colors.white, fontSize: 13),


      );

      Navigator.push(context, MaterialPageRoute(
          builder: (_) =>     Login ()
      ));
    }else{
      await initTopic1 ();
      navi();
    }
    print(data);
  

  }
navi(){
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) =>Dashbord ()),
  );
}
  Future<bool> _onBackPressed(){
    if(otp){
      setState(() {
       // otp=false;
      });

    }else{
      exit(0);
    }
    return  exit(0);

    print("wouuuuy");
    /// Navigator.of(context).pop(true);
  }
  startTime() async {
    var _duration = new Duration(seconds: 60);
    return new Timer(_duration, navi());
  }


  @override
  Widget build(BuildContext context) {

    return

      new WillPopScope(
        onWillPop: _onBackPressed,
        child:
      Scaffold(

         /* appBar:AppBar(
            automaticallyImplyLeading: false,
          ),*/
      backgroundColor:Colors.grey[200],

      body:
      SafeArea(
        child:
       Center( child:Container(
         // alignment: Alignment.center,

        width:
            getValueForScreenType<double>(
              context: context,
              mobile: MediaQuery.of(context).size.width * 0.9,
              tablet:MediaQuery.of(context).size.width * 0.7,
              desktop: 60,
            ),
    height:

           getValueForScreenType<double>(
             context: context,
             mobile: 450,
             tablet:690,
             desktop: 60,
           ),

    //padding: const EdgeInsets.all(10.0),
    decoration: myBoxDecoration() , //
          //  padding: EdgeInsets.only(top: 0),

  child:  ListView(

        children: <Widget>[


          Container(
            margin: EdgeInsets.only(top: 0),
            color: Theme.of(context).accentColor,
            alignment: Alignment.center,
            height: getValueForScreenType<double>(
              context: context,
              mobile: 199,
              tablet:260,
              desktop: 60,
            ),
            width: getValueForScreenType<double>(
              context: context,
              mobile: 50,
              tablet:100,
              desktop: 60,
            ),

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(height: 30,),
                Center(
                  child: Text('République du sénégal',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 18),),
                ),
                Image.asset(
                  'assets/logo.png',
                   height:100,
                   width: 100,
                  fit: BoxFit.contain,
                ),
                Center(
                  child: Text('Un Peuple - Un But - Une Foi',style: TextStyle(color: Colors.white,fontSize: 12),),
                ),
                SizedBox(height: 30,),
              ],
            )
          ),

          SizedBox(

            height:
            getValueForScreenType<double>(
              context: context,
              mobile: 20,
              tablet:30,
              desktop: 60,
            ),
          ),
       /*   Text(
            "FORMULAIRE DE CONNEXION",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize:
              getValueForScreenType<double>(
              context: context,
              mobile: 10,
              tablet:30,
              desktop: 60,
            ),



            ),
          ),*/
          SizedBox(
            height:
            getValueForScreenType<double>(
              context: context,
              mobile: 10,
              tablet:45,
              desktop: 60,
            ),
          ),
          otp?Container() :
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 32),
            child: Material(
              elevation: 2.0,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: TextField(
                onChanged: (String value){
                  setState(() {
                    username=value;
                  });
                },
                cursorColor: Theme.of(context).primaryColor,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),

                   labelText: "login",
                    isDense: true,
                    hintText: "Veuillez saisir votre login",
                    prefixIcon: Material(
                      elevation: 0,
                      color:Colors.transparent ,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      child: Icon(
                        Icons.person,
                      //  color: Colors.blue,
                        size:   getValueForScreenType<double>(
                          context: context,
                          mobile: 12,
                          tablet:18,
                          //  desktop: 60,
                        ),
                      ),
                    ),
                    contentPadding:
                    EdgeInsets.symmetric(horizontal: 25, vertical: 13)),
              ),
            ),
          ),
          SizedBox(
            height:
            getValueForScreenType<double>(
              context: context,
              mobile: 15,
              tablet:40,
              desktop: 60,
            ),
          ),
          otp?

          loader?
          Container(
            height:100,
            width:100,
            child: LoadingIndicator(indicatorType: Indicator.ballPulse,colors: [HexColor("#fa6400")],),
          )



              :

          Container(
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                SizedBox(
                  height: 5,
                ),
                Text("Renseigner le code OTP envoyé dans votre mail",style: TextStyle(fontWeight: FontWeight.bold),),
                SizedBox(
                  height: 25,
                ),
                PinCodeTextField(
                 // autoFocus: true,
                  length: 6,

                  obscureText: true,
                  animationType: AnimationType.fade,
                  pinTheme: PinTheme(
                    shape: PinCodeFieldShape.box,
                    borderRadius: BorderRadius.circular(5),
                    fieldHeight:
                    getValueForScreenType<double>(
                      context: context,
                      mobile: 40,
                      tablet:50,
                      desktop: 60,
                    ),
                    fieldWidth:  getValueForScreenType<double>(
                      context: context,
                      mobile: 40,
                      tablet:50,
                      desktop: 60,
                    ),
                    activeFillColor: Colors.white,
                    // activeColor: Colors.white,
                    selectedFillColor: Colors.white,
                    disabledColor: Colors.grey,
                    inactiveFillColor: Colors.orange[100],

                  ),
                  animationDuration: Duration(milliseconds: 300),
                  backgroundColor: Colors.white,
                  enableActiveFill: true,
                  errorAnimationController: errorController,
                  controller: textEditingController,
                  onCompleted: (v) {

                    setState(() {
                      loader=true;
                      otpText=v;
                    });
                    verifOtp(v.toString());
                    print(v);
                  },
                  onChanged: (value) {
                    print(value);
                    setState(() {
                      //   currentText = value;
                    });
                  },
                  keyboardType: TextInputType.number,
                  beforeTextPaste: (text) {
                    print("Allowing to paste $text");
                    //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                    //but you can show anything you want here, like your pop up saying wrong paste format or etc
                    return true;
                  }, appContext: context,
                ),
              ],
            )
          ) :
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 32),
            child: Material(
              elevation: 2.0,

              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: TextField(

             
                obscureText: !_passwordVisible,
                onChanged: (String value){
                  setState(() {
                    password=value;
                  });
                },
                cursorColor: Theme.of(context).primaryColor,
                decoration: InputDecoration(

                    suffixIcon: IconButton(
                      icon: Icon(

                        // Based on passwordVisible state choose the icon
                        _passwordVisible
                            ? Icons.visibility
                            : Icons.visibility_off,
                      //  color: Theme.of(context).primaryColorDark,
                        size:  getValueForScreenType<double>(
                          context: context,
                          mobile: 12,
                            tablet:18,
                          //  desktop: 60,
                        ),
                      ),
                      onPressed: () {
                        // Update the state i.e. toogle the state of passwordVisible variable
                        setState(() {
                          _passwordVisible = !_passwordVisible;
                        });
                      },
                    ),

                    border: OutlineInputBorder(),


                    isDense: true,
                    labelText: "Mot de passe",
                    hintText: "Mot de passe",
                    prefixIcon: Material(
                      color:Colors.transparent ,
                      elevation: 0,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      child: Icon(
                        Icons.lock,
                       // color: Colors.blue,
                        size:  getValueForScreenType<double>(
                          context: context,
                          mobile: 12,
                          tablet:18,
                          //  desktop: 60,
                        ),
                      ),
                    ),

                    contentPadding:
                    EdgeInsets.symmetric(horizontal: 25, vertical: 13)),
              ),
            ),
          ),
          SizedBox(
            height: getValueForScreenType<double>(
              context: context,
              mobile: 10,
              tablet:30,
              desktop: 60,
            ),
          ),
          otp?Container() :
          new InkWell(
              onTap: () => {
                print("ok"),
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>Login()),
                )
              },
              child:
         Padding(
         padding: EdgeInsets.symmetric(horizontal: 32),
         child:

         Text("Recommencer?",
           textAlign: TextAlign.right,
           style: TextStyle(
               fontSize:
             getValueForScreenType<double>(
             context: context,
             mobile: 10,
             tablet:18,
             desktop: 60,
           ),


           ),),
    )),
          SizedBox(
            height:
            getValueForScreenType<double>(
              context: context,
              mobile: 10,
              tablet:45,
              desktop: 60,
            ),
          ),

          otp?Container() :



          Padding(
              padding: EdgeInsets.symmetric(horizontal: 32),
              child: Container(
                height:
                getValueForScreenType<double>(
                  context: context,
                  mobile: 40,
                  tablet:50,
                  desktop: 60,
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color:HexColor("#fa6400"),),
                child:
                loader==true?Container(
                  alignment: Alignment.center,
                  child:LoadingIndicator(indicatorType: Indicator.lineScalePulseOutRapid,colors: [Colors.white],)
                ):




                FlatButton(
                  child: Text(
                    "SE CONNECTER",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontSize:
                      getValueForScreenType<double>(
                      context: context,
                      mobile: 10,
                      tablet:18,
                      desktop: 60,
                    ),


                    ),
                  ),
                  onPressed: () async {
                    if(!username!.isEmpty  & !password!.isEmpty){
                     /// navi();
                     await signIn();
                    }else{
                      FToast.toast(
                        context,
                        msg: "Tous les champs sont obligatoires!",

                        /// 配置 subMsg
                        ///
                        /// set subMsg
                        // subMsg: data,
                        image: Icon(
                          Icons.error,
                          color: Colors.red,
                        ),


                        /// 配置图标相对文本的位置
                        ///
                        /// set position of icon relative to text


                        /// 配置 SubMsg 样式
                        ///
                        /// set SubMsg style
                        subMsgStyle: TextStyle(color: Colors.white, fontSize: 13),


                      );
                    }
                    setState(()  {
                      loader=false;
                    });
              // Login();



                  },
                ),
              )),
          SizedBox(
            height:
            getValueForScreenType<double>(
              context: context,
              mobile: 10,
              tablet:40,
              desktop: 60,
            ),
          ),
          Center(
            child: Text("", style: TextStyle(color:Colors.red,fontSize: 12 ,fontWeight: FontWeight.w700),),
          ),
          SizedBox(
            height:
            getValueForScreenType<double>(
              context: context,
              mobile: 10,
              tablet:40,
              desktop: 60,
            ),
          ),
    /* Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Creer un compte patient ? ", style: TextStyle(color:Colors.black,fontSize: 12 ,fontWeight: FontWeight.normal),),

              GestureDetector(
                  child:   Text("S'enroller", style: TextStyle(color:Colors.red, fontWeight: FontWeight.w500,fontSize: 12,
                      decoration: TextDecoration.underline ),),
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (_) =>  Inscrire()));

                    // do what you need to do when "Click here" gets clicked
                  }
              )


            ],
          )::*/
        ],
      )),
    ))));
  }
  BoxDecoration myBoxDecoration() {
    return BoxDecoration(
      color: Colors.white,
      border: Border.all(
          color: Colors.grey,
          width: 1.3
      ),
      borderRadius: BorderRadius.all(
          Radius.circular(5) //         <--- border radius here
      ),

    );
  }
}

class WaveClipper1 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(0.0, size.height - 50);

    var firstEndPoint = Offset(size.width * 0.6, size.height - 29 - 50);
    var firstControlPoint = Offset(size.width * .25, size.height - 60 - 50);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    var secondEndPoint = Offset(size.width, size.height - 60);
    var secondControlPoint = Offset(size.width * 0.84, size.height - 50);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

class WaveClipper3 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(0.0, size.height - 50);

    var firstEndPoint = Offset(size.width * 0.6, size.height - 15 - 50);
    var firstControlPoint = Offset(size.width * .25, size.height - 60 - 50);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    var secondEndPoint = Offset(size.width, size.height - 40);
    var secondControlPoint = Offset(size.width * 0.84, size.height - 30);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

class WaveClipper2 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(0.0, size.height - 50);

    var firstEndPoint = Offset(size.width * .7, size.height - 40);
    var firstControlPoint = Offset(size.width * .25, size.height);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    var secondEndPoint = Offset(size.width, size.height - 45);
    var secondControlPoint = Offset(size.width * 0.84, size.height - 50);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }

}
/*
,
          Padding(
            padding: EdgeInsets.only(left: 115,right: 115),
            child: LiteRollingSwitch(
              //initial value

              value: true,
              textOn: 'Approuvé',
              textOff: '',
              colorOn: Colors.greenAccent[700],
              colorOff: Colors.redAccent[700],
              iconOn: Icons.done,
              iconOff: Icons.remove_circle_outline,
              textSize: 15.0,
              onChanged: (bool state) {
                //Use it to manage the different states
                print('Current State of SWITCH IS: $state');
              },
            ),
          ),
 */
class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}