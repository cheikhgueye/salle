


import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:animated_widgets/widgets/translation_animated.dart';
import 'package:atterissages/cer.dart';
import 'package:atterissages/src/pages/accueil/home.dart';
import 'package:atterissages/src/pages/dasbord/dashbord.dart';
import 'package:atterissages/src/pages/details/details.dart';
import 'package:atterissages/src/pages/details/detailsEscale.dart';
import 'package:atterissages/src/pages/details/detailsPreavis.dart';
import 'package:atterissages/src/pages/login/login.dart';
import 'package:atterissages/src/ressources/orbusApiProvider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
/*Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
  print("OKKKKK");


  // Or do other work.
}*/
class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => new SplashScreenState();
}
class SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
// final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
 final api = OrbusApiProvider ();
  var _visible = true;
  var demande;
  AnimationController? animationController;
  Animation<double>? animation;
  startTime() async {
    var _duration = new Duration(seconds:4);
    return new Timer(_duration, navigationPage);
  }

 getDemande(id) async{
   SharedPreferences prefs = await SharedPreferences.getInstance();
   var data=await api.details(id);
   return data;


 }
  void navigationPage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token =prefs.getString("survoltoken");
   if(token == null){
      Navigator.push(context, MaterialPageRoute(
          builder: (_) =>     Login ()
      ));



    } else {

      Navigator.push(context, MaterialPageRoute(
          builder: (_) => Dashbord ()
      ));
      // print("okkk"),
      // t.cancel()

    }

  }

 var alertStyle = AlertStyle(
  // overlayColor: Colors.transparent,
   animationType: AnimationType.fromTop,


   isCloseButton: true,
   isOverlayTapDismiss: true,
   descStyle: TextStyle(fontWeight: FontWeight.bold),
   animationDuration: Duration(milliseconds: 400),
   alertBorder: RoundedRectangleBorder(
     borderRadius: BorderRadius.circular(50.0),
     side: BorderSide(
       color: Colors.grey,
     ),
   ),
   titleStyle: TextStyle(
     color: Color.fromRGBO(91, 55, 185, 1.0),
   ),
 );


  @override
  void initState() {
    super.initState();
    animationController = new AnimationController(
      vsync: this,
      duration: new Duration(seconds: 2),
    );
    animation =
    new CurvedAnimation(parent: animationController!, curve: Curves.easeOut);

    animation!.addListener(() => this.setState(() {}));
    animationController!.forward();

    setState(() {
      _visible = !_visible;
    });



////////cloud messaging///
  /*  _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        String profil;
        print("onMessage: $message");
        SharedPreferences prefs = await SharedPreferences.getInstance();
        String libelleEntite=prefs.getString("libelleEntite")!.trim();
        Map data= await getDemande(message["data"]["body"]);
        setState(() {
         demande=data;
        });

        String pro=prefs.getString("codeProfil")!.trim();
        profil=message["data"]["title"];
        print(await getDemande(message["data"]["body"]));
        if((libelleEntite+'_'+pro).trim()==profil ){
         var data= await getDemande(message["data"]["body"]);
         print(data);
          Alert(
            context: context,
            style: alertStyle,
            type: AlertType.info,
            title: "Nouvelle demande",
            desc: message["notification"]["body"],
            buttons: [
              DialogButton(
                child: Text(
                  "Voir détails",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                onPressed: () => {
                  Navigator.pop(context),

                  if(message["data"]["content_available"]=="1"){
                  //  print(await getDemande(message["data"]["body"]))


       Navigator.push(context, MaterialPageRoute(
        builder: (_) => Details (demande: demande["demande"],type: "1",)
        ))
                  } else if(message["data"]["content_available"]=="2"){
                    Navigator.push(context, MaterialPageRoute(
                        builder: (_) => DetailsEscale (demande:   demande["demande"],type: "2",)
                    ))
                  }

                  else if(message["data"]["content_available"]=="3"){
                      Navigator.push(context, MaterialPageRoute(
                          builder: (_) => DetailsPreavis (demande:  demande["demande"],type: "3",)
                      ))
                    }
                else if(message["data"]["content_available"]=="4"){

                        Navigator.push(context, MaterialPageRoute(
                            builder: (_) =>
                                DetailsPreavis(
                                  demande: demande["demande"], type: "4",)
                        ))
                      }




                },
                color: Theme.of(context).accentColor,
                radius: BorderRadius.circular(10.0),
              ),
            ],
          ).show();

        }

        //   _showItemDialog(message);
      },
       // onBackgroundMessage: myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("merde");
       /* print("onLaunch: $message");
        Map data= await getDemande(message["data"]["body"]);
        setState(() {
          demande=data;
        });
        if(message["data"]["content_available"]=="1"){
          //  print(await getDemande(message["data"]["body"]))
         // Navigator.pop(context)

        Navigator.push(context, MaterialPageRoute(
        builder: (_) => Details (demande: demande["demande"],type: "1",)
        ));
        } else if(message["data"]["content_available"]=="2"){
        Navigator.push(context, MaterialPageRoute(
        builder: (_) => DetailsEscale (demande:   demande["demande"],type: "2",)
        ));
        }

        else if(message["data"]["content_available"]=="3"){
        Navigator.push(context, MaterialPageRoute(
        builder: (_) => DetailsPreavis (demande:  demande["demande"],type: "3",)
        ));
        }
        else if(message["data"]["content_available"]=="4"){

        Navigator.push(context, MaterialPageRoute(
        builder: (_) =>
        DetailsPreavis(
        demande: demande["demande"], type: "4",)
        ));
        }


        print("onLaunch: $message");*/
        //   _navigateToItemDetail(message);
        //  FlutterAppBadger.updateBadgeCount(1);
      },
      onResume: (Map<String, dynamic> message) async {
        print("merde");
        print("onResume: $message");
       Map data= await getDemande(message["data"]["body"]);
        setState(() {
          demande=data;
        });
        if(message["data"]["content_available"]=="1"){
          //  print(await getDemande(message["data"]["body"]))
          // Navigator.pop(context)

          Navigator.push(context, MaterialPageRoute(
              builder: (_) => Details (demande: demande["demande"],type: "1",)
          ));
        } else if(message["data"]["content_available"]=="2"){
          Navigator.push(context, MaterialPageRoute(
              builder: (_) => DetailsEscale (demande:   demande["demande"],type: "2",)
          ));
        }

        else if(message["data"]["content_available"]=="3"){
          Navigator.push(context, MaterialPageRoute(
              builder: (_) => DetailsPreavis (demande:  demande["demande"],type: "3",)
          ));
        }
        else if(message["data"]["content_available"]=="4"){

          Navigator.push(context, MaterialPageRoute(
              builder: (_) =>
                  DetailsPreavis(
                    demande: demande["demande"], type: "4",)
          ));
        }


        //  _navigateToItemDetail(message);
        // FlutterAppBadger.updateBadgeCount(1);
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      print("Push Messaging token: $token");
    });*/
  ///    _firebaseMessaging.subscribeToTopic("AGINI");
    startTime();
   /// i();


  }
  i() async{
    ByteData data = await rootBundle.load('assets/raw/CA_btrust360_rootca.crt');
    SecurityContext context = SecurityContext.defaultContext;
    context.setTrustedCertificatesBytes(data.buffer.asUint8List());
    HttpOverrides.global = new MyHttpOverrides();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
      Positioned.fill(
      child: Align(
      alignment: Alignment.topCenter,
          child:Container(

            child:
            TranslationAnimatedWidget.tween(

              enabled: true,
              translationDisabled: Offset(500, 0),
              translationEnabled: Offset(0, 0),
              duration: Duration(milliseconds: 1000),
              //  curve: Curves.easeInBack,
              child:

            Padding(
              padding: EdgeInsets.only(top: 0.0),
              child:   new Container(
                width: MediaQuery.of(context).size.width,
                height:
                getValueForScreenType<double>(
                  context: context,
                  mobile: 200,
                  tablet: 300,
                  desktop: MediaQuery.of(context).size.width/2- 40,
                ),

                child:Image.asset('assets/airsn.png'),
                decoration: new BoxDecoration(
                  color: Colors.white,
               /*   image: new DecorationImage(
                      fit: BoxFit.fill,
                      colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.dstATop),
                     // image: AssetImage(

                       //   'assets/airsn.png'
                     // )
                  ),*/
                ),
              ),)
          ,

      )))),
            new Column(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
           /*   Padding(
                padding: EdgeInsets.only(bottom: 10.0),
                child: new Image.asset(
                  'assets/baniere.jpg',
                  height: 100.0,
                 width: double.infinity,
                  fit: BoxFit.scaleDown,
                ),
              ),*/

    Padding(
    padding: EdgeInsets.only(top: 0.0),
    child:
    TranslationAnimatedWidget.tween(

    enabled: true,
    translationDisabled: Offset(500, 0),
    translationEnabled: Offset(0, 0),
    duration: Duration(milliseconds: 1000),
    //  curve: Curves.easeInBack,
    child:
    new Container(
      width: MediaQuery.of(context).size.width,
    height:  getValueForScreenType<double>(
      context: context,
      mobile: 200,
      tablet: 300,
      desktop: MediaQuery.of(context).size.width/2- 40,
    ),

      child:Image.asset('assets/bateau.png'),
    decoration: new BoxDecoration(
    color: Colors.white,
   /* image: new DecorationImage(
    fit: BoxFit.fill,
    colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.dstATop),
    image: AssetImage(

        'assets/bateau.png'
    )
    ),*/
    ),
    ),))

            ],
          ),
          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Image.asset(
                'assets/logo.png',
                width: animation!.value * 200,
                height: animation!.value *200,
              ),
              SizedBox(height: 3,),
              Text('République du sénégal',style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,


                  fontSize:
    getValueForScreenType<double>(
    context: context,
    mobile: 14,
    tablet:18,
    desktop: MediaQuery.of(context).size.width/2- 40,
    ),


              ),),
              SizedBox(height: 3,),
              Text('Un Peuple - Un But - Une Foi',style: TextStyle(color: Colors.black,

                  fontSize:    getValueForScreenType<double>(
    context: context,
    mobile: 10,
    tablet:12,
    desktop: MediaQuery.of(context).size.width/2- 40,
    ),


    fontWeight: FontWeight.w300),),

            ],
          ),
        ],
      ),
    );
  }
}
