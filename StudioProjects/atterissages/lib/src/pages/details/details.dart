

import 'dart:io';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
//import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:atterissages/src/pages/accueil/home.dart';
import 'package:atterissages/src/pages/dEscale/dEscale.dart';
import 'package:atterissages/src/pages/dPonctuelles/dPonctuelles.dart';
import 'package:atterissages/src/pages/dPreavis/dpreavis.dart';
import 'package:atterissages/src/pages/details/pdf.dart';
import 'package:atterissages/src/ressources/orbusApiProvider.dart';
import 'package:date_format/date_format.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
import 'package:ftoast/ftoast.dart';
import 'package:gx_file_picker/gx_file_picker.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:path_provider/path_provider.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
//import 'package:pdf/widgets.dart' as pw;
//import 'package:flutter_pdf_viewer/flutter_pdf_viewer.dart' as pdfv;

import 'dart:typed_data';
import 'dart:io';

import 'package:http/http.dart' as http;
class Details extends StatefulWidget {
  final demande;
  final type;

  const Details({Key? key, this.demande, this.type}) : super(key: key);
  @override
  _StepperExState createState() => _StepperExState();
}

class _StepperExState extends State<Details> {
  final api = OrbusApiProvider ();
  HttpClient client1 = new HttpClient();
  var isT=false;

  int _currentStep = 0;
  var appareils=[];
  String image64="";
  String com="";
  String fichetechniqueDemande="";
  String noteverbaleDemande="";
  String manifest="";
  StepperType stepperType = StepperType.vertical;
  var vlabel="";
  var rlabel="";
  var buttonstate=false;
  bool loading=false;
  bool loading1=false;
  var demande;


  File? _image;

  String? codeProfil;

  bool isPBU=false;

  switchStepType() {
    setState(() => stepperType == StepperType.vertical
        ? stepperType = StepperType.horizontal
        : stepperType = StepperType.vertical);
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("appareils");
    print(widget.demande["appareilsAutorisesDtoCollection"] );

    getDemande();
    initLabel();
  // notify();

  }
  void displayBottomSheet(BuildContext context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(25.0)),
        ),        context: context,
        builder: (ctx) {
          return Container(
            height: MediaQuery.of(context).size.height  * 0.8,
            decoration: BoxDecoration(
                borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(10.0),
                    topRight: const Radius.circular(10.0))),

            child:SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: 5,),
               Container(
                 child:  SizedBox(
                   width: 250.0,
                   child:


                   ColorizeAnimatedTextKit(
                     onTap: () {
                       print("Tap Event");
                     },
                     text: [
                       "Commentaires",


                     ],
                     textStyle: TextStyle(
                         fontSize: 30.0,
                         fontFamily: "Horizon",
                       fontWeight: FontWeight.bold
                     ),
                     colors: [
                      Theme.of(context).primaryColor,
                     Theme.of(context).accentColor
                     ],
                   ),
                 ),
               ),
                  SizedBox(height: 5,),
                  for (var tdm in demande["traitementDtoCollection"])
                 Container(
                    child: ListTile(
                      title: Text(tdm [ "utiDto"] ["identifiantProfil"]["proLibelle"].toString()+" - "+tdm ["utiDto"]["entite"]["entNom"],style: TextStyle(fontWeight: FontWeight.bold),),
                      subtitle: Column(
                        children: [
                          SizedBox(height: 5,),
                          Container(
                            alignment: Alignment.topLeft,
                            child:   RichText(
                              text: TextSpan(
                                text: "Statut: ",
                                style: TextStyle(color: Colors.black,fontSize: 12),
                                children: <TextSpan>[
                                  TextSpan(
                                      text:tdm ["statutTraitement"]["libelleStatutTraitement"],
                                      style: TextStyle(color:tdm ["statutTraitement"]["libelleStatutTraitement"]=="Refusé"?Colors.red:Colors.green ,fontSize: 12)


                                  ),
                                ],
                              ),
                            )
                          ),
                          SizedBox(height: 5,),
                         Container(
                           alignment: Alignment.topLeft,
                            child:  RichText(
          text: TextSpan(
          text: "Commmentaire: ",
          style: TextStyle(color: Colors.black,fontSize: 12),
          children: <TextSpan>[
          TextSpan(
          text:tdm ["commentaire"],
          style: TextStyle(
            color:tdm ["statutTraitement"]["libelleStatutTraitement"]=="Refusé"?Colors.red:Colors.black ,fontSize: 12,
            //  decoration:tdm ["statutTraitement"]["libelleStatutTraitement"]=="Refusé"? TextDecoration.underline:null,
            /// decorationStyle: tdm ["statutTraitement"]["libelleStatutTraitement"]=="Refusé"?TextDecorationStyle.dashed:null,
            //   decorationColor:tdm ["statutTraitement"]["libelleStatutTraitement"]=="Refusé"? Colors.red:null,
            //   decorationThickness: 4,


          )


          ),
          ],
          ),
          )





                          )
                        ],
                      ),
                    )
                  )

                ],
              ),
            )
          );
        });
  }


  notify() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String libelleEntite=prefs.getString("libelleEntite")!.trim();
    print(libelleEntite);
    setState(() {
      codeProfil= prefs.getString("codeProfil");
    });

    var nextProfil="";



    switch (codeProfil){

      case  "AGINI":{

        nextProfil="GOAA_AGOPE";

      }
      break;

      case  'AGOPE' :{
        if(libelleEntite=="GOAA"){
          nextProfil="GOAA_VDTEC";
        } else  if(libelleEntite=="EMAIR"){
          nextProfil="EMAIR_VDTEC";
        }else  if(libelleEntite=="EMGA"){
          nextProfil="EMGA_VDTEC";
        }

        else  if(libelleEntite=="MFA"){
          nextProfil="MFA_VDTEC";
        }

        else  if(libelleEntite=="ANACIM"){
          nextProfil="ANACIM_VDTEC";
        }


        else  if(libelleEntite=="MTTA"){
          nextProfil="MTTA_VDTEC";
        }
        else  if(libelleEntite=="MAESE"){
          nextProfil="MAESE_VDTEC";
        }
      }
      break;

      case  'AGOPM' :{
        nextProfil="MAESE_VDTEM";

      }
      break;

      case  'AGSUI' :{


      }
      break;


      case  'AGSUM' :{
        nextProfil="";

      }
      break;


      case  'VDTEC' :{
        if(libelleEntite=="GOAA"){
          nextProfil="EMAIR_AGOPE";
        }else  if(libelleEntite=="EMAIR"){
          nextProfil="EMGA_AGOPE";
        }
        else if(libelleEntite=="EMGA"){
          nextProfil="MFA_AGOPE";
        }
        else if(libelleEntite=="MFA"){
          nextProfil="MFA_VDHIE";
        }

        else if(libelleEntite=="ANACIM"){
          nextProfil="MTTA_AGOPE";
        }

        else if(libelleEntite=="MTTA"){
          nextProfil="MTTA_VDHIE";
        }
        else if(libelleEntite=="MAESE"){
          nextProfil="MAESE_VDHIE";
        }

      }
      break;


      case  'VDTEM' :{
        if(libelleEntite=="MAESE"){
          nextProfil="MAESE_VDHIM";
        }

      }
      break;


      case  'VDHIE' :{
        if(libelleEntite=="MFA"){
          nextProfil="ANACIM_AGOPE";
        } else if(libelleEntite=="MTTA"){
          nextProfil="MAESE_AGOPE";
        }

      }
      break;


      case  'VDHIM' :{
        nextProfil="";

      }
      break;


    }
    print("nextProfil");
    print(nextProfil);
    var body = {
      "to": "/topics/"+nextProfil.trim(),
      "notification": {
        "body": "Vous avez reçu une nouvelle demande a traiter",
        "content_available": "1",
        "priority": "high",
        "title": "",

      },
  //  "click_action": "FLUTTER_NOTIFICATION_CLICK",
      "data": {
        "body": demande["id"].toString() ,
        "content_available": "1",
        "priority": "high",
        "title": nextProfil.trim(),
        'click_action' : 'FLUTTER_NOTIFICATION_CLICK'
      },
      "apns": {
        "payload": {
          "aps": {
            "mutable-content": 1
          }
        },
        "fcm_options": {
          "image": "https://firebasestorage.googleapis.com/v0/b/cheikh-761f9.appspot.com/o/airsn.png?alt=media&token=073203db-6d5d-4709-a718-e569ff9024b6"
        }
      }
    };
    print(body );

    await  api.notify(body);
  }
  uploadFile() async{
    List<File> files = await FilePicker.getMultiFile(
      type: FileType.custom,
      allowedExtensions: ['jpg', 'pdf', 'doc'],
    );
    print(files);
  }

  accepter() async{
   var data=await api.valider(widget.demande["id"] , com);
   notify();    setState(() {
   //   appareils=data["demande"] ["appareilsAutorisesDtoCollection"];
    });
   getDemande();

  }
  transmettre() async{
    var data=await api.transmettre(widget.demande["id"] , com);
    notify();
    setState(() {
      //   appareils=data["demande"] ["appareilsAutorisesDtoCollection"];
    });
    getDemande();
  }
  retourner() async{
    var data=await api. retournerDemande(widget.demande["id"] , com);
    notify();
    getDemande();
  }
  valider() async{
    var data=await api.valider(widget.demande["id"] , com);
    notify();
    getDemande();

  }
  prevalider() async{
    var data=await api. prevalider(widget.demande["id"] , com);
    notify();


  }
  traitementFavorable() async{
    var data=await api. traitementFavorable(widget.demande["id"] , com);
    notify();
    getDemande();
  }
 refuser() async{
   var data=await api.refuse(widget.demande["id"] , com);
   print(data);
   setState(() {
     //   appareils=data["demande"] ["appareilsAutorisesDtoCollection"];
   });
   getDemande();
  }
  refuserPre() async{
    var data=await api.refusPrevalider(widget.demande["id"] , com);
    notify();
    setState(() {
      //   appareils=data["demande"] ["appareilsAutorisesDtoCollection"];
    });
    getDemande();
  }
  traitementDefavorable() async{
    var data=await api.traitementDefavorable(widget.demande["id"] , com);
    print(data);
    setState(() {
      //   appareils=data["demande"] ["appareilsAutorisesDtoCollection"];
    });
    getDemande();
  }
  invalider() async{
    var data=await api.invalider(widget.demande["id"] , com);
    print(data);
    setState(() {
      //   appareils=data["demande"] ["appareilsAutorisesDtoCollection"];
    });
    getDemande();
  }
  miseEnAttente() async{
    var data=await api.miseEnAttente(widget.demande["id"] , com);
    print(data);
    setState(() {
      //   appareils=data["demande"] ["appareilsAutorisesDtoCollection"];
    });
    getDemande();
  }
  delivrer() async{
    var data=await api.delivranceMaese(widget.demande["id"] , com);
    notify();
    setState(() {
      //   appareils=data["demande"] ["appareilsAutorisesDtoCollection"];
    });
    getDemande();
  }
  traitementFavorableMaes() async{
    var data=await api.traiterParMAES(widget.demande["id"] , com);
    notify();
    setState(() {
      //   appareils=data["demande"] ["appareilsAutorisesDtoCollection"];
    });
    getDemande();
  }
  commenter() async{
    var data=await api.commenterSuivi(widget.demande["id"] , com);
    print(data);
    setState(() {
      //   appareils=data["demande"] ["appareilsAutorisesDtoCollection"];
    });
    getDemande();
  }

  initLabel(){
    switch (codeProfil){

      case  "AGINI":{
        setState(() {
          vlabel="Transmettre";
          rlabel="Retourner";
        });
        print("ksdsfskdsdsjds");
        print(demande);

        if(demande["stdIdDemande"]["stdLibelle"]!="Soumise"){
setState(() {
  buttonstate=true;
});
        }

      }
      break;

      case  'AGOPE' :{
        setState(() {
          vlabel="Accepter";
          rlabel="Refuser";
        });

      }
      break;

      case  'AGOPM' :{
        setState(() {
          vlabel="Accepter";
          rlabel="Refuser";
        });
      }
      break;

      case  'AGSUI' :{
        setState(() {
          vlabel="Commenter";
          rlabel="";
        });

      }
      break;


      case  'AGSUM' :{
        setState(() {
          vlabel="Commenter";
          rlabel="";
        });
      }
      break;


      case  'VDTEC' :{
        setState(() {
          vlabel="Accepter";
          rlabel="Refuser";
        });
      }
      break;


      case  'VDTEM' :{
        setState(() {
          vlabel="Accepter";
          rlabel="Refuser";
        });
      }
      break;


      case  'VDHIE' :{
        setState(() {
          vlabel="Accepter";
          rlabel="Refuser";
        });

        if(demande["stdIdDemande"]["stdLibelle"]!= "Validée"){
          setState(() {
          ///  buttonstate=true;
          });
        }
      }
      break;


      case  'VDHIM' :{
        setState(() {
          vlabel="Delivrer";
          rlabel="Mettre en attente";
        });


      }
      break;


    }


  }
  _openPdf(url,type) async {
   //var r= Uri.decodeComponent("http://10.3.80.166:7070/survolatt/demande/files/download?file=fichetechnique_2020-10-21T15_33_26.331Z_YNyM2UFyRMvEA7E4bj8nDocumentation du code front.pdf&fileType=fichetechnique");
    ////
   // var data = await  api.doc(url,"fichetechnique");

    downloadFile(type,url);
  // Navigator.push(context,MaterialPageRoute(builder: (context) => Pdf(url: r,)));
  }
  erro(p){



  }
  lunchDialog(v){
    var  pr1 =  ProgressDialog(context,);
    pr1.style(
        message: 'Téléchargement du document...' ,

        borderRadius: 10.0,
        backgroundColor: Colors.transparent,
        progressWidget: LoadingIndicator(indicatorType: Indicator.ballClipRotateMultiple,),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        //    textDirection: TextDirection.rtl,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.white, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    if(v==1){
      pr1.show();
    }else {
      pr1.hide();
    }

  }

  downloadFile(String type,fname, {String? filename}) async {
    client1.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
    lunchDialog(2);

try {

  List<int> _downloadData =[];
  client1.getUrl(Uri.parse("${OrbusApiProvider().baseUrl}demande/files/download?file="+fname+"&fileType="+type))
      .then((HttpClientRequest request) {


    return request.close();
  })
      .then((HttpClientResponse response) {
    lunchDialog(1);
    response.listen((d) => _downloadData.addAll(d),
      //  onError: erro(2),
        onDone: () async {
          print(_downloadData);
          if(_downloadData.length==0)
            FToast.toast(
              context,
              msg: "Ce document est vide",

              /// 配置 subMsg
              ///
              /// set subMsg
              // subMsg: data,
              image: Icon(
                Icons.error,
                color: Colors.red,
              ),


              /// 配置图标相对文本的位置
              ///
              /// set position of icon relative to text


              /// 配置 SubMsg 样式
              ///
              /// set SubMsg style
              subMsgStyle: TextStyle(color: Colors.white, fontSize: 13),


            );
          Navigator.pop(context);
          // await file.writeAsBytes(bytes);
          // var pdf1=file.readAsBytesSync();
          //  print(pdf1);
          String dir = (await getApplicationDocumentsDirectory()).path;
          File file = new File('$dir/$filename');
          await file.writeAsBytes(_downloadData);
          print(_downloadData);
          if(_downloadData.length!=0){
            Navigator.push(context,MaterialPageRoute(builder: (context) => PDFScreen(pathPDF: file.path,)));
          } else {
    FToast.toast(
    context,
    msg: "Ce document est vide",

    /// 配置 subMsg
    ///
    /// set subMsg
    // subMsg: data,
    image: Icon(
    Icons.error,
    color: Colors.red,
    ),


    /// 配置图标相对文本的位置
    ///
    /// set position of icon relative to text


    /// 配置 SubMsg 样式
    ///
    /// set SubMsg style
    subMsgStyle: TextStyle(color: Colors.white, fontSize: 13),


    );
    }

          // fileSave.writeAsBytes(_downloadData);
        }
    );
  }).catchError((e)=>{
    print(e.toString()),

  });


} catch(e){

  print(e);
  erro(2);
}

  }

  getDemande() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

    var data=await api.details(widget.demande["id"] );
    setState(() {
      codeProfil=prefs.getString("codeProfil");
    });
  print(codeProfil);
  print(data["demande"] ["isProcessedByUser"]);

  print(widget.demande["id"] );
  print(data["demande"] ["processing"]);
  setState(() {
     fichetechniqueDemande=data["demande"] ["fichetechniqueDemande"];
     noteverbaleDemande=data["demande"] ["noteverbaleDemande"];
     isPBU=data["demande"] ["isProcessedByUser"];
    // manifst=data["demande"] ["manifestDemande"];

    appareils=data["demande"] ["appareilsAutorisesDtoCollection"];
    demande=data["demande"];
  });
    initLabel();

}
  Future chooseFile() async {
    var image = await ImagePicker.pickImage(source:ImageSource.gallery );

   // var img1=  testCompressFile(image);
    setState(() {
      _image = image;
    });
    // final bytes = Io.File(_image).readAsBytesSync();

    var img64 = base64Encode(_image!.readAsBytesSync());
    print(img64.substring(0, 100));

    setState(() {
      image64 = img64 ;
    });

  }
  static Future<bool> isTablet(BuildContext context) async {

    if (Platform.isIOS) {
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      print( iosInfo.model.toLowerCase() == "ipad");

      return iosInfo.model.toLowerCase() == "ipad";
    } else {
      // The equivalent of the "smallestWidth" qualifier on Android.
      var shortestSide = MediaQuery.of(context).size.shortestSide;
      print(shortestSide > 600);

      // Determine if we should use mobile layout or not, 600 here is
      // a common breakpoint for a typical 7-inch tablet.
      return shortestSide > 600;
    }
  }
  @override
  Widget build(BuildContext context) {
    isTablet(context).then((value) => isT=value);

    return Scaffold(
      resizeToAvoidBottomInset: false,
     // resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        toolbarHeight: 70,
        backgroundColor: Theme.of(context).accentColor,

        //  backgroundColor: Colors.blue[900],
        title: Container(
            alignment: Alignment.topRight,
            child:Column(
              children: [
                Image.asset(
                  'assets/logo.png',
                  height:30,
                  width: 30,
                  fit: BoxFit.contain,
                ),
                SizedBox(height: 3,),
                Text('République du sénégal',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 9),),
                SizedBox(height: 3,),
                Text('Un Peuple - Un But - Une Foi',style: TextStyle(color: Colors.white,fontSize: 6,fontWeight: FontWeight.w300),),

              ],
            )
        ),
      ),
      body:
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                  color: Colors.grey,
                  width: 1.3
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5) //         <--- border radius here
              ),

            ),
            margin: EdgeInsets.only(top: 20,left: 20,right: 20,bottom: 20),
    child:  Column(
        children: <Widget>[

        /*  SizedBox(height: 10,),
      Padding(
        padding: EdgeInsets.all(10),
        child:
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                child:   Padding(
                  padding: EdgeInsets.only(top: 0.0),
                  child:   new Container(
                    width: MediaQuery.of(context).size.width/2,
                    height: 200,
                    child: new Text(
                      '',
                      style: Theme.of(context).textTheme.display4,
                      textAlign: TextAlign.center,

                    ),
                    decoration: new BoxDecoration(

                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      //  color: Colors.teal,
                      border: Border.all(
                          color: Colors.grey,
                          width: 0.5
                      ),
                    //  color: Colors.white,
                      image: new DecorationImage(
                          fit: BoxFit.fill,
                        //  colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.dstATop),
                          image: AssetImage(
                              'assets/request.png'
                          )
                      ),
                    ),
                  ),),
              ),
              Container(
                height: 200,
                padding: EdgeInsets.all(10),
                decoration: new BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  //  color: Colors.teal,
                  border: Border.all(
                      color: Colors.grey,
                      width: 0.5
                  ),
                  //  color: Colors.white,

                ),
                width: MediaQuery.of(context).size.width/2- 30,
                child: Column(
                  children: [
                    Text("Titre",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),textAlign: TextAlign.left,),
                    SizedBox(height: 10,),


                  Expanded(
                    child: SingleChildScrollView(
                        child: Text(
                          "Le lorem ipsum est, en imprimerie, une suite de mots sans signification utilisée à titre provisoire pour calibrer une mise en page, le texte définitif venant remplacer le faux-texte dès qu'il est prêt ou que la mise en page est achevée. Généralement, on utilise un texte en faux latin, le Lorem ipsum ou Lipsum."
                          , style: TextStyle(fontSize: 15),)),



              ),
                  ],
                ),


              )
            ],
          )),*/
        Container(
          height: 70,
          color:Theme.of(context).accentColor,
          child: Center(
            child:

            Text("Validation de la demande N° "+widget.demande["numeroDemande"],style: TextStyle(color: Colors.white),),
          ),

        ),
          Expanded(
            child: Stepper(

              controlsBuilder: (BuildContext context,f) {
                // {VoidCallback onStepContinue, VoidCallback onStepCancel}
                return Row(
                  children: <Widget>[
                    Container(
                      child: null,
                    ),
                    Container(
                      child: null,
                    ),
                  ],
                );
              },
              //physics: ClampingScrollPhysics(),
              steps: _stepper(),
              type: stepperType,
              currentStep: this._currentStep,
              onStepTapped: (step) {
                setState(() {
                  this._currentStep = step;
                });
              },
              onStepContinue: () {
                setState(() {
                  if (this._currentStep < this._stepper().length - 1) {
                    this._currentStep = this._currentStep + 1;
                  } else {
                    //Logic
                    print('complete');
                  }
                });
              },
              onStepCancel: () {
                setState(() {
                  if (this._currentStep > 0) {
                    this._currentStep = this._currentStep - 1;
                  } else {
                    this._currentStep = 0;
                  }
                });
              },
            ),
          ),
        ],
      ),
      /* floatingActionButton: FloatingActionButton(
        child: Icon(Icons.swap_horizontal_circle),
        onPressed: switchStepType,
      ),*/
          ) );
  }

  List<Step> _stepper() {
    List<Step> _steps = [

      Step(
          title: Text('Info sur la demande'),
          content:
          Column(
    children: [


      Container(
        height: 200,
        decoration:  BoxDecoration(

          border: Border.all(
              color: Colors.grey,
              width: 1
          ),
          borderRadius: BorderRadius.all(
              Radius.circular(5) //         <--- border radius here
          ),

        ),

        /*
             SizedBox(height: 10,),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Column(children: [
                    Text('Fiche technique'),
                    SizedBox(height: 10,),

                   Image.asset("assets/pdf.png",width: 40,height: 50,)
                  ],),
                  Column(children: [
                    Text('Le manifeste'),
                    SizedBox(height: 10,),
                    Image.asset("assets/pdf.png",width: 40,height: 50,)
                  ],),
                  Column(children: [
                    Text('Note verbale'),
                    SizedBox(height: 10,),
                    Image.asset("assets/pdf.png",width: 40,height: 50,)
                  ],)


                ],
              )

             */
        child: SafeArea(
          child: Row(
            children: [

              Container(padding: EdgeInsets.only(left: 20), height: 290 , width:isT?100:60, decoration:  BoxDecoration(
                color:HexColor("#c7f6ec"),
                border: Border(
                    right: BorderSide(
                        color: Colors.grey,
                        width: 1
                    )
                ),


              ),
                  child:Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Ref :",style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize:isT?12:8),),
                      Text("Pays :",style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize:isT?12:8),),
                      Text("Date :" ,style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize:isT?12:8),),
                      Text("Statut :" ,style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize:isT?12:8),),
                   //   Text("Détails :" ,style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e")),),




                    ],
                  )

              ),

              Container(padding: EdgeInsets.only(left: 20), height: 290 , decoration:  BoxDecoration(
                //  color:HexColor("#c7f6ec"),


              ),
                  child:Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(widget.demande["numeroDemande"],style: TextStyle(fontSize: isT?12:8),),
                      Text(widget.demande["proprietaireDemande"]!=null?widget.demande["proprietaireDemande"]["paysProprietaire"]["nomPays"]:"",style: TextStyle(fontSize: isT?12:8),),
                      Text(  formatDate( DateTime.parse(widget.demande["dateDemande"]), [dd, '-', mm, '-', yyyy]).toString(), style: TextStyle(fontSize: isT?12:8),),
                      Text(demande!=null?demande["stdIdDemande"]["stdLibelle"]:"",style: TextStyle(fontSize: isT?12:8),),
                   //   Text("Voir +" ,style: TextStyle(color: Colors.blue),),


                    ],
                  )

              )
            ],
          ),
        ),


      ),



      SizedBox(height: 20,),
      Row(
      children: [
        Text("Commentaire demande : " ,style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize:   getValueForScreenType<double>(
              context: context,
              mobile: 9,
              tablet:12,
              desktop: MediaQuery.of(context).size.width/2- 40,
            ),



        ),
        ),
       Expanded(
         child:Text(

             demande!=null?(demande["commentaireDemande"]!=null?demande["commentaireDemande"]:"Pas de commentaire"):"",

           style: TextStyle(

             textBaseline: TextBaseline.ideographic,
             fontSize:   getValueForScreenType<double>(
               context: context,
               mobile: 9,
               tablet:10,
               desktop: MediaQuery.of(context).size.width/2- 40,
             ),
           ),
       textAlign: TextAlign.justify,



         )
       ),

      ],
      ),
      SizedBox(height: 20,),

    Row(
    mainAxisAlignment: MainAxisAlignment.spaceAround,
    children: [
      fichetechniqueDemande==null?SizedBox():
    InkWell(
    onTap: ()=>{
    _openPdf(fichetechniqueDemande,"fichetechnique")
    // downloadFile("fichetechnique",fichetechniqueDemande)
    },
    child:
    Column(children: [
    Text('Fiche technique',style: TextStyle(fontSize:isT?12:8),),
    SizedBox(height: 10,),
    Image.asset("assets/pdf.png",width: 40,height: 50,),


    ],),),
    /*InkWell(
    onTap: ()=>{
    _openPdf(manifest,"manifest")
    // downloadFile("fichetechnique",fichetechniqueDemande)
    },
    child:
    Column(children: [
    Text('Le manifeste',style: TextStyle(fontSize:isT?12:8)),
    SizedBox(height: 10,),

    Image.asset("assets/pdf.png",width: 40,height: 50,)
    ],),),*/
      noteverbaleDemande==null?SizedBox():
    InkWell(
    onTap: ()=>{
    _openPdf(noteverbaleDemande,"noteverbale")
    // downloadFile("fichetechnique",fichetechniqueDemande)
    },
    child:
    Column(children: [
    Text('Note verbale',style: TextStyle(fontSize:isT?12:8),),
    SizedBox(height: 10,),

    Image.asset("assets/pdf.png",width: 40,height: 50,)
    ],))


    ],
    )
    ],
    )
,





          /* Column(
            children: <Widget>[
              Container(
                color: Colors.white,
                padding: EdgeInsets.all(20.0),
                child: Table(

                  border: TableBorder.all(color: Colors.black),
                  children: [

                    TableRow(

                        children: [
                          TableCell(

                              child:
                Container(

                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    border: Border.all(color: Colors.grey),
                   // borderRadius: BorderRadius.all(Radius.circular(16))
                ),
              padding: const EdgeInsets.all(8.0),
                              child:
                              Text('Ref',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),)



    )),
                          TableCell(child:   Container(

    decoration: BoxDecoration(
    color: Theme.of(context).primaryColor,
    border: Border.all(color: Colors.grey),
    // borderRadius: BorderRadius.all(Radius.circular(16))
    ),
    padding: const EdgeInsets.all(8.0),

                              child: Text('Pays',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),))),
                          TableCell(child:   Container(

                              decoration: BoxDecoration(
                                color: Theme.of(context).primaryColor,
                                border: Border.all(color: Colors.grey),
                                // borderRadius: BorderRadius.all(Radius.circular(16))
                              ),
                              padding: const EdgeInsets.all(8.0),
                              child: Text('Date',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),))),
                          TableCell(child:  Container(

                              decoration: BoxDecoration(
                                color: Theme.of(context).primaryColor,
                                border: Border.all(color: Colors.grey),
                                // borderRadius: BorderRadius.all(Radius.circular(16))
                              ),
                              padding: const EdgeInsets.all(8.0),
                              child: Text('Statut',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),))),

                    ]),
                    TableRow(children: [
                      TableCell(child:  Container(
                           alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.grey),
                            // borderRadius: BorderRadius.all(Radius.circular(16))
                          ),
                          padding: const EdgeInsets.all(8.0),
                          child: Text('2019/12',style: TextStyle(color: Colors.black),))),
                      TableCell(child:  Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.grey),
                            // borderRadius: BorderRadius.all(Radius.circular(16))
                          ),
                          padding: const EdgeInsets.all(8.0),
                          child: Text('France',style: TextStyle(color: Colors.black),))),
                      TableCell(child:  Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.grey),
                            // borderRadius: BorderRadius.all(Radius.circular(16))
                          ),
                          padding: const EdgeInsets.all(8.0),
                          child: Text('12/08/2020',style: TextStyle(color: Colors.black),))),
                      TableCell(child:  Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.grey),
                            // borderRadius: BorderRadius.all(Radius.circular(16))
                          ),
                          padding: const EdgeInsets.all(8.0),
                          child: Text('Soumis',style: TextStyle(color: Colors.black),))),







                    ])
                  ],
                ),
              ),
              SizedBox(height: 10,),
              Container(
                child: Text("Détail sur la demande"),
              ),
              SizedBox(height: 10,),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Column(children: [
                    Text('Fiche technique'),
                    SizedBox(height: 10,),

                   Image.asset("assets/pdf.png",width: 40,height: 50,)
                  ],),
                  Column(children: [
                    Text('Le manifeste'),
                    SizedBox(height: 10,),
                    Image.asset("assets/pdf.png",width: 40,height: 50,)
                  ],),
                  Column(children: [
                    Text('Note verbale'),
                    SizedBox(height: 10,),
                    Image.asset("assets/pdf.png",width: 40,height: 50,)
                  ],)


                ],
              )



            ],
          ),*/
          isActive: _currentStep >= 0,
          state: StepState.indexed),
      Step(

          title: Text('Info demandeur'),


          content:Container(
            height: 200,
            decoration:  BoxDecoration(

              border: Border.all(
                  color: Colors.grey,
                  width: 1
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5) //         <--- border radius here
              ),

            ),
            child: SafeArea(
              child: Row(
                children: [

                  Container(padding: EdgeInsets.only(left: 20), height: 200
                      ,width:isT?100:60


                      , decoration:  BoxDecoration(
                        color:HexColor("#c7f6ec"),
                        border: Border(
                            right: BorderSide(
                                color: Colors.grey,
                                width: 1
                            )
                        ),


                      ),
                      child:Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Entité :",style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize:isT?12:8),),
                          Text("Adresse :",style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize:isT?12:8),),
                          Text("Contact :" ,style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize:isT?12:8),),


                        ],
                      )

                  ),
                  Expanded(
                      child:
                      Container(padding: EdgeInsets.only(left: 20), height: 200, decoration:  BoxDecoration(
                        //  color:HexColor("#c7f6ec"),


                      ),
                          child:


                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(widget.demande["proprietaireDemande"]!=null?widget.demande["proprietaireDemande"]["structureProprietaire"].toString().trim():"",style: TextStyle(fontSize: isT?12:8),),
                              Text(widget.demande["proprietaireDemande"]!=null?widget.demande["proprietaireDemande"]["paysProprietaire"]["nomPays"]:"",style: TextStyle(fontSize: isT?12:8),),
                              Text(widget.demande["proprietaireDemande"]!=null?widget.demande["proprietaireDemande"]["emailProprietaire"]:"" ,style: TextStyle(fontSize: isT?12:8),),],
                          )
                      )

                  )
                ],
              ),
            ),

          ),
          isActive: _currentStep >= 1,
          state: StepState.indexed),
      Step(
          title: Text('Info Technique'),
          content:Container(
            height: 200,
            decoration:  BoxDecoration(

              border: Border.all(
                  color: Colors.grey,
                  width: 1
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5) //         <--- border radius here
              ),

            ),
            child:
            demande ==null?Container():   demande ["batDemande"]!=null?
            SafeArea(
              child: Row(
                children: [

                  Container(padding: EdgeInsets.only(left: 20), height: 290 ,width:isT?150:90 , decoration:  BoxDecoration(
                    color:HexColor("#c7f6ec"),
                    border: Border(
                        right: BorderSide(
                            color: Colors.grey,
                            width: 1
                        )
                    ),


                  ),
                      child:Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Nom  :",style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize: isT?12:8),),
                          Text("Immatriculation :" ,style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize: isT?12:8),),
                          Text("NumimoBateau:" ,style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize: isT?12:8),),
                          Text("Armateur:" ,style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize: isT?12:8),),




                        ],
                      )

                  ),

                  Expanded(child: Container( height: 290, decoration:  BoxDecoration(
                    //  color:HexColor("#c7f6ec"),


                  ),
                      child:Expanded(
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: [
                              Row(children: [
                                SizedBox(width: 5,),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(demande !=null?demande ["batDemande"]["nomBateau"]!=null?demande ["batDemande"]["nomBateau"]:"ss":""  ,style: TextStyle(fontSize: isT?12:8),),
                                    Text(demande !=null?demande ["batDemande"]["matriculeBateau"]!=null?demande ["batDemande"]["matriculeBateau"]:"":"" ,style: TextStyle(fontSize: isT?12:8),),
                                    Text(demande !=null?demande ["batDemande"]["numimoBateau"]!=null?demande ["batDemande"]["numimoBateau"]:"":"" ,style: TextStyle(fontSize: isT?12:8),),

                                    Text(demande !=null?demande ["batDemande"]["armateurBateau"]!=null?demande ["batDemande"]["armateurBateau"]:"":"" ,style: TextStyle(fontSize: isT?12:8),),

                                  ],
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 10,right: 10),
                                  width: 1,
                                  height: double.maxFinite,
                                  color: Colors.grey,
                                )

                              ],)
                          ],
                        ),
                      )

                  ))
                ],
              ),
            ):
            SafeArea(
              child: Row(
                children: [

                  Container(padding: EdgeInsets.only(left: 20), height: 290 ,width:isT?150:90 , decoration:  BoxDecoration(
                    color:HexColor("#c7f6ec"),
                    border: Border(
                        right: BorderSide(
                            color: Colors.grey,
                            width: 1
                        )
                    ),


                  ),
                      child:Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Type  :",style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize: isT?12:8),),
                          Text("Classe :",style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize: isT?12:8),),
                          Text("Immatriculation :" ,style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize: isT?12:8),),
                          Text("Indicatif d’appel :" ,style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize: isT?12:8),),




                        ],
                      )

                  ),

                 Expanded(child: Container( height: 290, decoration:  BoxDecoration(
                    //  color:HexColor("#c7f6ec"),


                  ),
                      child:Container(
                        padding: EdgeInsets.only(left: 10),
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                      children: [
                        for (var a in appareils)
                          Row(children: [
       Column(
         mainAxisAlignment: MainAxisAlignment.spaceAround,
         crossAxisAlignment: CrossAxisAlignment.start,
         children: [
           Text(a["avionDto"]!=null?a["avionDto"]["typeAvion"]:"ss"  ,style: TextStyle(fontSize: isT?12:8),),
           Text(a["avionDto"]!=null?a["avionDto"]["classeAvion"]["cavLibelle"]:"" ,style: TextStyle(fontSize: isT?12:8),),
           Text(a["avionDto"]!=null?a["avionDto"]["matriculeAvion"]:"" ,style: TextStyle(fontSize: isT?12:8),),
           Text(a["avionDto"]!=null?a["avionDto"]["indicationAvion"]:"" ,style: TextStyle(fontSize: isT?12:8),),


         ],
       ),
       Container(
         margin: EdgeInsets.only(left: 10,right: 10),
         width: 1,
         height: double.maxFinite,
         color: Colors.grey,
       )

     ],)
                          ],
                        ),
                      )

                  ))
                ],
              ),
            ),

          ),









          /* Column(
            children: <Widget>[

              Container(
                color: Colors.white,
                padding: EdgeInsets.all(20.0),
                child: Table(

                  border: TableBorder.all(color: Colors.black),
                  children: [

                    TableRow(

                        children: [
                          TableCell(

                              child:
                              Container(

                                  decoration: BoxDecoration(
                                    color: Theme.of(context).primaryColor,
                                    border: Border.all(color: Colors.grey),
                                    // borderRadius: BorderRadius.all(Radius.circular(16))
                                  ),
                                  padding: const EdgeInsets.all(8.0),
                                  child:
                                  Text('Type',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),)



                              )),
                          TableCell(child:   Container(

                              decoration: BoxDecoration(
                                color: Theme.of(context).primaryColor,
                                border: Border.all(color: Colors.grey),
                                // borderRadius: BorderRadius.all(Radius.circular(16))
                              ),
                              padding: const EdgeInsets.all(8.0),

                              child: Text('Classe',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),))),
                          TableCell(child:   Container(

                              decoration: BoxDecoration(
                                color: Theme.of(context).primaryColor,
                                border: Border.all(color: Colors.grey),
                                // borderRadius: BorderRadius.all(Radius.circular(16))
                              ),
                              padding: const EdgeInsets.all(8.0),
                              child: Text('Immatriculation',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),))),
                          TableCell(child:  Container(

                              decoration: BoxDecoration(
                                color: Theme.of(context).primaryColor,
                                border: Border.all(color: Colors.grey),
                                // borderRadius: BorderRadius.all(Radius.circular(16))
                              ),
                              padding: const EdgeInsets.all(8.0),
                              child: Text('Indicatif d appel',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),))),




                        ]),
                    TableRow(children: [
                      TableCell(child:  Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.grey),
                            // borderRadius: BorderRadius.all(Radius.circular(16))
                          ),
                          padding: const EdgeInsets.all(8.0),
                          child: Text('BOING',style: TextStyle(color: Colors.black),))),
                      TableCell(child:  Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.grey),
                            // borderRadius: BorderRadius.all(Radius.circular(16))
                          ),
                          padding: const EdgeInsets.all(8.0),
                          child: Text('Civil',style: TextStyle(color: Colors.black),))),
                      TableCell(child:  Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.grey),
                            // borderRadius: BorderRadius.all(Radius.circular(16))
                          ),
                          padding: const EdgeInsets.all(8.0),
                          child: Text('21202999',style: TextStyle(color: Colors.black),))),
                      TableCell(child:  Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.grey),
                            // borderRadius: BorderRadius.all(Radius.circular(16))
                          ),
                          padding: const EdgeInsets.all(8.0),
                          child: Text('20192GHF',style: TextStyle(color: Colors.black),))),







                    ])
                  ],
                ),
              ),
            ],
          ),*/
          isActive: _currentStep >= 2,
          state: StepState.indexed),
      Step(
          title: Text(' Traitement'),
          content:
              Column(
              //  scrollDirection: Axis.vertical,
                children: [
          Container(
            height: isT?400:300,
            decoration:  BoxDecoration(

              border: Border.all(
                  color: Colors.grey,
                  width: 1
              ),
              borderRadius: BorderRadius.all(
                  Radius.circular(5) //         <--- border radius here
              ),

            ),
            child: SafeArea(
              child: Row(
                children: [

                  Container(padding: EdgeInsets.only(left: 5), height: 400 ,width:isT?140:90 , decoration:  BoxDecoration(
                    color:HexColor("#c7f6ec"),
                    border: Border(
                        right: BorderSide(
                            color: Colors.grey,
                            width: 1
                        )
                    ),


                  ),
                      child:Column(
                       // mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: isT?30:50,),
                          Text("Commentaires précédant:",style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize: isT?12:8),),
                          SizedBox(height: isT?180:100,),
                          Text("Commentaire:",style: TextStyle(fontWeight: FontWeight.bold,color: HexColor("#20569e"),fontSize: isT?12:8),),

                        ],
                      )

                  ),

          Expanded(
            child:
            Container(padding: EdgeInsets.only(left: 2), height: 400 ,
                child:Flex(
                  direction: Axis.vertical,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                  InkWell(
    child:  Card(
    child: ListTile(
    leading:isT? Icon(Icons.comment):null,

    title: Text(isT?'Voir les commentaires':"Commentaires",style: TextStyle(fontSize:isT?12:8 ),),
    trailing:isT? null:Icon(Icons.comment,size: 8,),

    ),),
    onTap:(){
    displayBottomSheet( context);
    },

    ),



                    Container(
                        child: TextField(
                          onChanged: (String value){
                            setState(() {
                              com=value;
                            });
                          },
                          keyboardType: TextInputType.multiline,
                          maxLines: null,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 20,top: 20),
                            hintText: "Taper votre commentaire ici"
                          ),
                          // expands: true,

                        )),
                    SizedBox(height: 30,),



                  ],
                )

            )
          )
                ],
              ),
            ),

          ),
          SizedBox(height: 40,),

        // buttonstate==true?Container() :

          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              loading?
                  Container(
                    height:40,
                    width:100,
                    child: LoadingIndicator(indicatorType: Indicator.ballPulse, colors: [HexColor("#419d9e")],),
                  )

                  :


              Container(
                child:       IgnorePointer(
                    ignoring: isPBU,
                    child: FlatButton(
                  child: Text(
                    vlabel,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                      fontSize:
                      getValueForScreenType<double>(
                        context: context,
                        mobile: 10,
                        tablet:18,
                        desktop: 60,
                      ),


                    ),
                  ),
                  onPressed: () async {
                    setState(() {
                      loading=true;

                    });
                    codeProfil=="AGINI"?
                    await transmettre():(codeProfil== 'AGOPE'  ||codeProfil==  'VDTEC'  )?
                    await traitementFavorable():(codeProfil=='VDHIM'?
                    await delivrer():(codeProfil=='VDTEM'?
                    await valider():(codeProfil=='AGSUI' || codeProfil== 'AGSUM'?
                    await commenter():(codeProfil=='VDHIE'?
                    await  prevalider():(codeProfil== 'AGOPM' ?await traitementFavorableMaes():null ))
                    )));
                    setState(() {
                      loading=false;
                    });
                // await   notify();
                   if(widget.type=="1"){
                     Navigator.push(context, MaterialPageRoute(
                         builder: (_) => Home ()
                     ));
                   } else if(widget.type=="2"){
                     Navigator.push(context, MaterialPageRoute(
                         builder: (_) => DEscale ()
                     ));
                   } else if(widget.type=="3"){
                      Navigator.push(context, MaterialPageRoute(
                          builder: (_) =>  DPonctuelles ()
                      ));
                    }else
                    if(widget.type=="4"){
                      Navigator.push(context, MaterialPageRoute(
                          builder: (_) => DPreavis ()
                      ));
                    }
                  },
                )),
                height:
                getValueForScreenType<double>(
                  context: context,
                  mobile: 40,
                  tablet:40,
                  desktop: 60,
                ),
                width:   getValueForScreenType<double>(
                  context: context,
                  mobile: 100,
                  tablet:200,
                  desktop: 60,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color:isPBU?HexColor("#419d9e").withOpacity(0.5):HexColor("#419d9e"),



                ),
              ),
              SizedBox(width: 20,)    ,
              loading1?
              Container(
                height:40,
                width:100,
                child: LoadingIndicator(indicatorType: Indicator.ballPulse, colors: [HexColor("#ff1919")],),
              )



                  :
        rlabel==""?Container():
              Container(
                child:
                IgnorePointer(
                  ignoring: isPBU,
                    child:
                FlatButton(

                  child: Text(
                    rlabel,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                      fontSize:
                      getValueForScreenType<double>(
                        context: context,
                        mobile: 10,
                        tablet:18,
                        desktop: 60,
                      ),


                    ),
                  ),
                  onPressed: () async {
                    setState(() {
                      loading1=true;
                    });
                    codeProfil=="AGINI"?await  retourner():
                    (codeProfil== 'AGOPM' ||codeProfil=='AGOPM'   || codeProfil=='VDTEC' || codeProfil=='AGOPE'?
                       await  traitementDefavorable():(
                        codeProfil== 'VDTEM' ?await invalider():
                        (
                            codeProfil== 'VDHIE'?await refuserPre():(
                                codeProfil== 'VDHIM'?await miseEnAttente():null
                            )
                        )
                    )

                    );
               notify();
                        setState(() {
                      loading1=false;
                    });
                    if(widget.type=="1"){
                      Navigator.push(context, MaterialPageRoute(
                          builder: (_) => Home ()
                      ));
                    } else if(widget.type=="2"){
                      Navigator.push(context, MaterialPageRoute(
                          builder: (_) => DEscale ()
                      ));
                    } else if(widget.type=="3"){
                      Navigator.push(context, MaterialPageRoute(
                          builder: (_) =>  DPonctuelles ()
                      ));
                    }else
                    if(widget.type=="4"){
                      Navigator.push(context, MaterialPageRoute(
                          builder: (_) => DPreavis ()
                      ));
                    }




                  },
                )),
                height:
                getValueForScreenType<double>(
                  context: context,
                  mobile: 40,
                  tablet:40,
                  desktop: 60,
                ),

                width:
                getValueForScreenType<double>(
                  context: context,
                  mobile: 100,
                  tablet:200,
                  desktop: 60,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color:isPBU?HexColor("#ff1919").withOpacity(0.5):HexColor("#ff1919"),),
              ),


            ],
          )


    ],
    ),



          /* Column(
            children: <Widget>[
              Card(
                child: ListTile(
                  leading: Icon(Icons.attach_file),
                  title: Text('Cliquez ici pour joinde un ficher'),

                ),),
              Card(
                child: TextField(
                  keyboardType: TextInputType.multiline,
                  maxLines: 10,
                 // expands: true,

                )),
              SizedBox(height: 30,),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
    Container(
      child:  FlatButton(
        child: Text(
          "Accepter",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w700,
            fontSize:
            getValueForScreenType<double>(
              context: context,
              mobile: 10,
              tablet:18,
              desktop: 60,
            ),


          ),
        ),
        onPressed: () async {




        },
      ),
    height:
    getValueForScreenType<double>(
    context: context,
    mobile: 40,
    tablet:40,
    desktop: 60,
    ),
    width: 200,
    decoration: BoxDecoration(
    borderRadius: BorderRadius.all(Radius.circular(10)),
    color:HexColor("#419d9e"),),
    ),
          SizedBox(width: 20,)    ,

                  Container(
                    child:  FlatButton(
                      child: Text(
                        "Retourner",
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize:
                          getValueForScreenType<double>(
                            context: context,
                            mobile: 10,
                            tablet:18,
                            desktop: 60,
                          ),


                        ),
                      ),
                      onPressed: () async {




                      },
                    ),
                    height:
                    getValueForScreenType<double>(
                      context: context,
                      mobile: 40,
                      tablet:40,
                      desktop: 60,
                    ),
                    width: 200,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color:HexColor("#ff1919"),),
                  ),


                ],
              )

            ],
          ),*/
          isActive: _currentStep >= 3,
          state: StepState.indexed),

    ];
    return _steps;
  }
}
class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
