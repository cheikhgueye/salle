
import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:atterissages/src/pages/accueil/home.dart';
import 'package:atterissages/src/pages/autorisations/auto.dart';
import 'package:atterissages/src/pages/dEscale/dEscale.dart';
import 'package:atterissages/src/pages/dPonctuelles/dPonctuelles.dart';
import 'package:atterissages/src/pages/dasbord/dashbord.dart';
import 'package:atterissages/src/pages/details/details.dart';
import 'package:atterissages/src/pages/details/detailsPreavis.dart';
import 'package:atterissages/src/pages/login/login.dart';
import 'package:atterissages/src/ressources/orbusApiProvider.dart';
import 'package:badges/badges.dart';
import 'package:date_format/date_format.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();



class ReceivedNotification {
  ReceivedNotification({
 this.id,
 this.title,
    this.body,
   this.payload,
  });

  final int? id;
  final String? title;
  final String? body;
  final String? payload;
}

/// IM
class MailContent {
  String subject;
  String time;
  String sender;
  String message;
  bool isOpened;

  MailContent(this.subject, this.sender, this.time, this.message,this.isOpened);
  String getSubject() => this.subject;
  String getSender() => this.sender;
  String getTime() => this.time;
  String getMessage() => this.message;
  bool getIsOpened()=>this.isOpened;
}
class MailGenerator {
  static var mailList = [
    MailContent("Atterissage", "Cheikh gueye", "11 Oct",
        "Le lorem ipsum est, en imprimerie, une suite de mots sans signification utilisée .",false),
    MailContent("Atterissage", "Omar Faye", "11 Oct",
        "Le lorem ipsum est, en imprimerie, une suite de mots sans signification utilisée ",false),
    MailContent("Atterissage", "Cheikh gueye", "11 Oct",
        "Le lorem ipsum est, en imprimerie, une suite de mots sans signification utilisée ",true),
    MailContent("Atterissage", "Cheikh gueye", "11 Oct",
        "Le lorem ipsum est, en imprimerie, une suite de mots sans signification utilisée ",true),

  ];
  static MailContent getMailContent(int position) => mailList[position];
  static int mailListLength = mailList.length;
}
class DPreavis extends StatefulWidget {
  @override
  _StepperExState createState() => _StepperExState();
}

class _StepperExState extends State< DPreavis> {
  final api = OrbusApiProvider ();
  List demandesList=[  ];
  var isT=false;
  var drawerIcons = [
    Image.asset('assets/dash.png',width: 30,height: 30,),
    Image.asset('assets/paper.jpg',width: 30,height: 30,),
    Image.asset('assets/paper.jpg',width: 30,height: 30,),
    Image.asset('assets/paper.jpg',width: 30,height: 30,),
    Image.asset('assets/paper.jpg',width: 30,height: 30,),
    Image.asset('assets/paper.jpg',width: 30,height: 30,),
    Image.asset('assets/po.png',width: 30,height: 30,)
    /*Icon(Icons.move_to_inbox),
    Icon(Icons.inbox),
    Icon(Icons.people),
    Icon(Icons.local_offer),
    Icon(Icons.star),
    Icon(Icons.access_time),
    Icon(Icons.label),
    Icon(Icons.send),
    Icon(Icons.send),
    Icon(Icons.note),
    Icon(Icons.mail),
    Icon(Icons.error),
    Icon(Icons.delete),
    Icon(Icons.label),
    Icon(Icons.label),
    Icon(Icons.settings),
    Icon(Icons.help),*/
  ];

  var drawerText = [
    "Tableau De Bord",
    "Demandes Permanentes",
    "Preavis Survol / Atter",
    "Demandes Ponctuelles",
    "Demandes Escales",
    "Liste autorisations",
    "Se déconnecter"
    /* "All inboxes",
    "Primary",
    "Social",
    "Promotions",
    "Starred",
    "Snoozed",
    "Important",
    "Sent",
    "Outbox",
    "Drafts",
    "All mail",
    "Spam",
    "Bin",
    "[Imap]/Sent",
    "[Imap]/Trash",
    "Settings",
    "Help & feedback"*/
  ];

  String? nom;
  String? profil;
  String codeProfil="";
  Drawer _getMailAccountDrawerr() {
    Text email = new Text(
      "kheuche@gmail.com",
      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15.0),
    );

    Text name = new Text(
      "Cheikh gueye",
      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15.0),
    );

    return Drawer(
        child: Column(
          children: <Widget>[
            Container(
              height: 200,
              color: Theme.of(context).accentColor,
              child: Column(
                children: [
                  Container(
                    decoration:  BoxDecoration(


                      border: Border(

                        bottom: BorderSide( //                    <--- top side
                          color: Colors.grey,
                          width: 1.0,
                        ),
                      ),),

                    height: 100,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top:25),
                          child:    Image.asset(
                            'assets/logo.png',
                            height:65,
                            width: 60,
                            alignment: Alignment.bottomCenter,
                            fit: BoxFit.contain,
                          ),
                        ),
                        SizedBox(width: 10,),
                        Column(
                          children: [
                            SizedBox(height: 40,),
                            Text('République du sénégal',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 18),),
                            SizedBox(height: 10,),
                            Text('Un Peuple - Un But - Une Foi',style: TextStyle(color: Colors.white,fontSize: 12,fontWeight: FontWeight.w300),),

                          ],
                        )                      ],
                    ),
                  ),
                  Container(
                    height: 100,
                    child: Row(
                      children: [
                        Icon(
                          Icons.account_circle,
                          size: 50.0,
                          color: Colors.white,
                        ),
                        SizedBox(width: 10,),
                        Text(nom!,style: TextStyle(color: Colors.white,fontSize: 12,fontWeight: FontWeight.bold),),
                        SizedBox(width: 10,),
                    Expanded(
                      child:
                        Text(profil!,style: TextStyle(color: Colors.white,fontSize: 12,fontWeight: FontWeight.w300),)),
                      ],
                    ),
                  )
                ],
              ),
            ),






            /*  UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Theme.of(context).primaryColor),
              accountName: name,
              accountEmail: email,
              currentAccountPicture: Icon(
                Icons.account_circle,
                size: 50.0,
                color: Colors.white,
              ),
            ),*/
            Expanded(
              flex: 2,
              child: ListView.builder(
                  padding: EdgeInsets.only(top:0.0),
                  itemCount: drawerText.length,
                  itemBuilder: (context, position) {
                    return


                      ListTile(
                      leading: drawerIcons[position],
                      title: Text(drawerText[position],
                          style: TextStyle(fontSize: 15.0)),
                      onTap: () {
                        this.setState(() {
                          titleBarContent = drawerText[position];
                        });
                        if(drawerText[position]== "Se déconnecter"){
                          print(drawerText[position]);

                          Navigator.push(context, MaterialPageRoute(
                              builder: (_) => Login ()
                          ));

                        } else if(drawerText[position]== "Demandes Permanentes"){
                          Navigator.push(context, MaterialPageRoute(
                              builder: (_) => Home ()
                          ));

                        } else if(drawerText[position]=="Demandes Ponctuelles"){
                          Navigator.push(context, MaterialPageRoute(
                              builder: (_) => DPonctuelles ()
                          ));
                        } else if( drawerText[position]== "Preavis Survol / Atter"){
                          Navigator.push(context, MaterialPageRoute(
                              builder: (_) => DPreavis ()
                          ));
                        }
                        else if( drawerText[position]=="Demandes Escales"){
                          Navigator.push(context, MaterialPageRoute(
                              builder: (_) => DEscale ()
                          ));
                        }
                        else if( drawerText[position]== "Tableau De Bord"){
                          Navigator.push(context, MaterialPageRoute(
                              builder: (_) => Dashbord()
                          ));
                        }
                        else if( drawerText[position]== "Liste autorisations"){
                          Navigator.push(context, MaterialPageRoute(
                              builder: (_) => Autori()
                          ));
                        }
                        else{
                          Navigator.pop(context);

                        }
                      },
                    );
                  }),
            )
          ],
        ));
  }
  var titleBarContent = "Primary";
  int _currentStep = 0;
  StepperType stepperType = StepperType.vertical;

  switchStepType() {
    setState(() =>
    stepperType == StepperType.vertical
        ? stepperType = StepperType.horizontal
        : stepperType = StepperType.vertical);
  }
  void navigationPage(i) async {
    // SharedPreferences prefs = await SharedPreferences.getInstance();
    // var token =prefs.getString("token");
    //   if(token == null){
    Navigator.push(context, MaterialPageRoute(
        builder: (_) =>DetailsPreavis(demande:  demandesList[i],type: "4",)
    ));}

  demandes() async {

    var data = await api.dPreavisList();
    if(data==0){
      Navigator.push(context, MaterialPageRoute(
          builder: (_) =>     Login ()
      ));

    }
    setState(() {
      demandesList=data.reversed.toList();
    });

    print(data);


  }
  var   _start=0;
  Timer? _timer;
  @override
  initState(){
    super.initState();
    demandes();
   // _Notification();
    initData();
    timer();
  }

  timer(){
    const oneSec=const Duration(seconds: 1);
    _timer=new Timer.periodic(
        oneSec,
            (Timer timer){

          if(_start==12){
            setState(() {
              timer.cancel();
            });
          }else{
            print(   _start);
            setState(() {
              _start++;
            });
          }

        }

    );

  }

  Future<void> _Notification() async {


    initData();
  }
  initData() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      codeProfil=prefs.getString("codeProfil")!;
    });
    setState(() {
      nom=prefs.getString("nom")!=null?prefs.getString("nom"):"";
      profil=prefs.getString("profil")!=null?prefs.getString("profil"):"";
    });

  }
  static Future<bool> isTablet(BuildContext context) async {

    if (Platform.isIOS) {
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      print( iosInfo.model.toLowerCase() == "ipad");

      return iosInfo.model.toLowerCase() == "ipad";
    } else {
      // The equivalent of the "smallestWidth" qualifier on Android.
      var shortestSide = MediaQuery.of(context).size.shortestSide;
      print(shortestSide > 600);

      // Determine if we should use mobile layout or not, 600 here is
      // a common breakpoint for a typical 7-inch tablet.
      return shortestSide > 600;
    }
  }
  @override
  Widget build(BuildContext context) {
    isTablet(context).then((value) => isT=value);
    return Scaffold(

        drawer: _getMailAccountDrawerr() ,
        // resizeToAvoidBottomPadding: false,

        appBar: AppBar(
          backgroundColor: Theme.of(context).accentColor,
          toolbarHeight: 70,
          iconTheme: IconThemeData(
              color: Colors.white,
              size: 400 // This isn't performing any changes
          ),

          //  automaticallyImplyLeading: false,

          // backgroundColor: Colors.blue[900],
          // backgroundColor: Colors.blue[900],
          // backgroundColor: Colors.blue[900],
          title: Container(
            // alignment: Alignment.topRight,
              child:Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Demandes de préavis",style: TextStyle(fontWeight: FontWeight.w400,
                      fontSize:  getValueForScreenType<double>(
                        context: context,
                        mobile: 10,
                        tablet:15,
                        desktop: MediaQuery.of(context).size.width/2- 40,
                      ),


                  ),),
                  Column(
                    children: [
                      Image.asset(
                        'assets/logo.png',
                        height:30,
                        width: 30,
                        fit: BoxFit.contain,
                      ),
                      SizedBox(height: 3,),
                      Text('République du sénégal',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 9),),
                      SizedBox(height: 3,),
                      Text('Un Peuple - Un But - Une Foi',style: TextStyle(color: Colors.white,fontSize: 6,fontWeight: FontWeight.w300),),

                    ],
                  )
                ],
              )
          ),

          /* actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 13.0),
            child: Icon(
              Icons.help,
              size: 25.0,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 13.0),
            child: Icon(
              Icons.translate,
              size: 25.0,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 13.0),
            child:
    Badge(
      badgeColor: Colors.red,
      shape: BadgeShape.circle,
      borderRadius: 20,
      toAnimate: false,
    badgeContent: Text('3'),

    child:  Icon(
      Icons.notifications,
      size: 25.0,
    ),
    )



          ),
        ],*/
        ),
        body:
        SafeArea(
          child: Container(
            margin: EdgeInsets.all(10),
            decoration: myBoxDecoration() ,

            child:

            Column(
              children: <Widget>[
                ///   SizedBox(height: 10,),
                Padding(
                    padding: EdgeInsets.all(10),
                    child:
                    Container(
                      //  color: Theme.of(context).accentColor,
                        decoration: BoxDecoration(
                          color: Theme.of(context).accentColor,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.4),
                              spreadRadius: 10,
                              blurRadius: 5,
                              offset: Offset(0, 7), // changes position of shadow
                            ),
                          ],
                        ),
                        child:
                        Row(
                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(width:30 ,),
                            Container(
                              child:   Padding(
                                padding: EdgeInsets.only(top: 0.0),
                                child:   new Container(
                                  width: MediaQuery.of(context).size.width/3,
                                  height:isT?  200:100,
                                  child: new Text(
                                    '',
                                  //  style: Theme.of(context).textTheme.display4,
                                    textAlign: TextAlign.center,

                                  ),
                                  decoration: new BoxDecoration(
                                    color: Theme.of(context).accentColor,
                                    borderRadius: BorderRadius.all(Radius.circular(10)),
                                    //  color: Colors.teal,

                                    //  color: Colors.white,
                                    image: new DecorationImage(
                                        fit: BoxFit.contain,
                                        //  colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.dstATop),
                                        image: AssetImage(

                                            'assets/a.png'
                                        )
                                    ),
                                  ),
                                ),),
                            ),
                            Container(
                              height:isT?  200:100,
                              padding: EdgeInsets.all(10),
                              decoration: new BoxDecoration(
                                color: Theme.of(context).accentColor,
                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                //  color: Colors.teal,

                                //  color: Colors.white,

                              ),
                              width:
                              getValueForScreenType<double>(
                                context: context,
                                mobile: MediaQuery.of(context).size.width/2- 40,
                                tablet: MediaQuery.of(context).size.width/2- 30,
                                desktop: MediaQuery.of(context).size.width/2- 40,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("Liste des demandes de préavis ",style: TextStyle(fontWeight: FontWeight.bold,fontSize:isT? 16:8,color: Colors.white),textAlign: TextAlign.left,),
                                  SizedBox(height: isT?10:5,),


                                  Center(

                                      child:Column(

                                        children: [

                                          TyperAnimatedTextKit(
                                            pause: Duration(milliseconds: 7000),
                                            onTap: () {
                                              // print("Tap Event");
                                            },
                                            text: [
                                              "Vous avez la possibilité de cliquer sur le titre de la demande ou l’icone oeil afin de voir les details et les les pièces-joints."
                                                  "Merci!",

                                            ],
                                            textStyle: TextStyle(
                                                fontSize:isT? 16:8,
                                                fontFamily: "Bobbers",
                                                color: Colors.white
                                            ),
                                          ),

                                        //  Text(,style: TextStyle( fontSize:isT? 12:5,color: Colors.white),textAlign: TextAlign.center,),



                                        ],
                                      )


                                  ),
                                ],
                              ),


                            )
                          ],
                        )
                    )


                ),
                SizedBox(height: 10,),
                demandesList.length==0?

                _start>=5?

                Center(
                  child: Column(
                    children: [
                      Image.asset("assets/empty.png"),
                      SizedBox(height: 5,),
                      Text("Pas de demandes",style: TextStyle(color: Colors.grey),)
                    ],
                  )
                )

                    :



                Container(

                    child:   Expanded(
                      child: Shimmer.fromColors(
                        baseColor: Colors.grey.shade300,
                        highlightColor: Colors.grey.shade100,

                        enabled: true,
                        child: ListView.builder(
                          itemBuilder: (_, __) => Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: 48.0,
                                  height: 48.0,
                                  color: Colors.white,
                                ),
                                const Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        width: double.infinity,
                                        height: 8.0,
                                        color: Colors.white,
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.symmetric(vertical: 2.0),
                                      ),
                                      Container(
                                        width: double.infinity,
                                        height: 8.0,
                                        color: Colors.white,
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.symmetric(vertical: 2.0),
                                      ),
                                      Container(
                                        width: 40.0,
                                        height: 8.0,
                                        color: Colors.white,
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          itemCount: 20,
                        ),
                      ),
                    )
                ):
                Expanded(
                  child:ListView.builder(
                      itemCount:demandesList.length,
                      itemBuilder: (context, position) {
                        return Column(children: <Widget>[
                          InkWell(
                              onTap:() =>{
                                navigationPage(position),

                              },

                              child:
                              Padding(
                                padding: EdgeInsets.only(
                                    left: 14.0, right: 14.0, top: 5.0, bottom: 5.0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    /*  Icon(
                              Icons.account_circle,
                              size: 55.0,
                             color: Theme.of(context).primaryColor,
                            ),*/
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.only(left: 10.0),
                                        child: Column(
                                          children: <Widget>[
                                            /*  Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          mailContent.sender,
                                          style: TextStyle(
                                              fontWeight:mailContent.isOpened? FontWeight.w400:FontWeight.bold,
                                              color: Colors.black87,
                                              fontSize: 17.0),
                                        ),
                                        Text(
                                          mailContent.time,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w400,
                                              color: Colors.black54,
                                              fontSize: 13.5),
                                        ),
                                      ],
                                    ),*/
                                            Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Row(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Text(
                                                      demandesList[position]["numeroDemande"],

                                                      style: TextStyle(
                                                          fontWeight:( codeProfil=="AGINI" && demandesList[position]["stdIdDemande"]["stdLibelle"]=="Soumise")|| demandesList[position]["processing"]==1? FontWeight.bold:FontWeight.w400,
                                                          color:( codeProfil=="AGINI" && demandesList[position]["stdIdDemande"]["stdLibelle"]=="Soumise")|| demandesList[position]["processing"]==1? Colors.black:Colors.black54,
                                                          fontSize: 15.5),
                                                    ),
                                                    SizedBox(width: 20,),

                                                    Text(
                                                      formatDate( DateTime.parse(demandesList[position]["dateDemande"]), [dd, '/', mm, '/', yyyy]).toString(),

                                                      style: TextStyle(
                                                          fontWeight:( codeProfil=="AGINI" && demandesList[position]["stdIdDemande"]["stdLibelle"]=="Soumise")|| demandesList[position]["processing"]==1? FontWeight.bold:FontWeight.w400,
                                                          color:( codeProfil=="AGINI" && demandesList[position]["stdIdDemande"]["stdLibelle"]=="Soumise")|| demandesList[position]["processing"]==1? Colors.black:Colors.black54,
                                                          fontSize: 15.5),
                                                    ),
                                                    SizedBox(width: 20,),
                                                    isT?
                                                    Text(
                                                      demandesList[position]["stdIdDemande"]["stdLibelle"],
                                                      style: TextStyle(
                                                          fontWeight:( codeProfil=="AGINI" && demandesList[position]["stdIdDemande"]["stdLibelle"]=="Soumise")|| demandesList[position]["processing"]==1? FontWeight.bold:FontWeight.w400,
                                                          color:    // demandesList[position]["isPressecedByUser"]==true?Colors.black54:Colors.black,


                                                          demandesList[position]["stdIdDemande"]["stdLibelle"]=="Soumise"?Colors.blue:
                                                          (   demandesList[position]["stdIdDemande"]["stdLibelle"]=="En traitement"?Colors.black:
                                                          ( demandesList[position]["stdIdDemande"]["stdLibelle"]=="Retournée"?Colors.red:Colors.green
                                                              //   ()

                                                          )




                                                          ),
                                                          fontSize: 15.5),
                                                    ):SizedBox(),
                                                    SizedBox(width: 20,),
                                                    isT?
                                                    demandesList[position]["proprietaireDemande"]!=null?     Text(
                                                      demandesList[position]["proprietaireDemande"]["paysProprietaire"]["codePays"],
                                                      style: TextStyle(
                                                          fontWeight:( codeProfil=="AGINI" && demandesList[position]["stdIdDemande"]["stdLibelle"]=="Soumise")|| demandesList[position]["processing"]==1? FontWeight.bold:FontWeight.w400,
                                                          color:( codeProfil=="AGINI" && demandesList[position]["stdIdDemande"]["stdLibelle"]=="Soumise")|| demandesList[position]["processing"]==1? Colors.black:Colors.black54,
                                                          fontSize: 15.5),
                                                    ):SizedBox():SizedBox()
                                                  ],
                                                ),
                                                Icon(Icons.remove_red_eye, size: 18.0,                                                           color:( codeProfil=="AGINI" && demandesList[position]["stdIdDemande"]["stdLibelle"]=="Soumise")|| demandesList[position]["processing"]==1? Colors.black:Colors.black54,

                                                ),


                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )),
                          Divider(
                            thickness: 2,
                          ),
                        ]);
                      }),
                ),
              ],
            ),
            /* floatingActionButton: FloatingActionButton(
        child: Icon(Icons.swap_horizontal_circle),
        onPressed: switchStepType,
      ),*/
          ),
        ) );
  }
  BoxDecoration myBoxDecoration() {
    return BoxDecoration(
      color: Colors.white,
      border: Border.all(
          color: Colors.grey,
          width: 1.3
      ),
      borderRadius: BorderRadius.all(
          Radius.circular(5) //         <--- border radius here
      ),

    );
  }
}
