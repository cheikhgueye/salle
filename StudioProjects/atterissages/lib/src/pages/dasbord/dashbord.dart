
import 'dart:async';
import 'dart:io';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:animated_widgets/widgets/translation_animated.dart';
import 'package:atterissages/src/pages/accueil/home.dart';
import 'package:atterissages/src/pages/autorisations/auto.dart';
import 'package:atterissages/src/pages/dEscale/dEscale.dart';
import 'package:atterissages/src/pages/dPonctuelles/dPonctuelles.dart';
import 'package:atterissages/src/pages/dPreavis/dpreavis.dart';
import 'package:atterissages/src/pages/details/details.dart';
import 'package:atterissages/src/pages/login/login.dart';
import 'package:atterissages/src/ressources/orbusApiProvider.dart';
import 'package:atterissages/src/widgets/bar.dart';
import 'package:atterissages/src/widgets/charts.dart';
import 'package:atterissages/src/widgets/lineCharts.dart';
import 'package:atterissages/src/widgets/pieChart.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:ftoast/ftoast.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Dashbord extends StatefulWidget {
  @override
  _SplashScreenState createState() => new _SplashScreenState();
}
class _SplashScreenState extends State<Dashbord >
    with SingleTickerProviderStateMixin {
 // final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  //final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  var _visible = true;
  AnimationController? animationController;
  final api = OrbusApiProvider ();

  Animation<double>? animation;
  var nom="";
  var profil="";
  var nbrPerm="0";
  var nbrPreavis="0";
  var nbrPonc="0";
  var  nbrEsc="0";
  String codeProfil="";
  var drawerIcons = [
    Image.asset('assets/dash.png',width: 30,height: 30,),
    Image.asset('assets/paper.jpg',width: 30,height: 30,),
    Image.asset('assets/paper.jpg',width: 30,height: 30,),
    Image.asset('assets/paper.jpg',width: 30,height: 30,),
    Image.asset('assets/paper.jpg',width: 30,height: 30,),
    Image.asset('assets/paper.jpg',width: 30,height: 30,),
    Image.asset('assets/po.png',width: 30,height: 30,)
    /*Icon(Icons.move_to_inbox),
    Icon(Icons.inbox),
    Icon(Icons.people),
    Icon(Icons.local_offer),
    Icon(Icons.star),
    Icon(Icons.access_time),
    Icon(Icons.label),
    Icon(Icons.send),
    Icon(Icons.send),
    Icon(Icons.note),
    Icon(Icons.mail),
    Icon(Icons.error),
    Icon(Icons.delete),
    Icon(Icons.label),
    Icon(Icons.label),
    Icon(Icons.settings),
    Icon(Icons.help),*/
  ];
  var isT=false;
  var drawerText = [
    "Tableau De Bord",
    "Demandes Permanentes",
    "Preavis Survol / Atter",
    "Demandes Ponctuelles",
    "Demandes Escales",
    "Liste autorisations",
    "Se déconnecter"
    /*
     "All inboxes",
    "Primary",
    "Social"
    "Promotions",
    "Starred",
    "Snoozed",
    "Important",
    "Sent",
    "Outbox",
    "Drafts",
    "All mail",
    "Spam",
    "Bin",
    "[Imap]/Sent",
    "[Imap]/Trash",
    "Settings",
    "Help & feedback"
    */
  ];
  startTime() async {
    var _duration = new Duration(seconds:4);
    return new Timer(_duration, navigationPage);
  }
  dash() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
   var code= prefs.getString("codeProfil");
   if(code=="ADMIN"){
     Navigator.push(context, MaterialPageRoute(
         builder: (_) =>     Login ()
     ));
   }


    var data = await api.dashBord();
    if(data==0){
      Navigator.push(context, MaterialPageRoute(
          builder: (_) =>     Login ()
      ));

    }
    setState(() {
      codeProfil=prefs.getString("codeProfil")!;
    });
    setState(() {
      nbrPerm=data["nbrPerm"].toString();
      nbrPreavis=data["nbrPreavis"].toString();
      nbrPonc=data["nbrPonc"].toString();
      nbrEsc=data["nbrEsc"].toString();
    });


  }
  void navigationPage() async {
    // SharedPreferences prefs = await SharedPreferences.getInstance();
    // var token =prefs.getString("token");
    //   if(token == null){
    Navigator.push(context, MaterialPageRoute(
        builder: (_) => Login ()
    ));

/*

    } else {

      Navigator.push(context, MaterialPageRoute(
          builder: (_) => Home()
      ));
      // print("okkk"),
      // t.cancel()

    }*/

  }

  @override
  void initState() {
    super.initState();
    dash();
   // initTopic ();
/*

////////cloud messaging///
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        //   _showItemDialog(message);
      },
      //  onBackgroundMessage: myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        //   _navigateToItemDetail(message);
        //  FlutterAppBadger.updateBadgeCount(1);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        //  _navigateToItemDetail(message);
        // FlutterAppBadger.updateBadgeCount(1);
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      print("Push Messaging token: $token");
    });
    _firebaseMessaging.subscribeToTopic("matchscore");*/
    //startTime();

    initData();

  }
  static Future<bool> isTablet(BuildContext context) async {

    if (Platform.isIOS) {
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      print( iosInfo.model.toLowerCase() == "ipad");

      return iosInfo.model.toLowerCase() == "ipad";
    } else {
      // The equivalent of the "smallestWidth" qualifier on Android.
      var shortestSide = MediaQuery.of(context).size.shortestSide;
      print(shortestSide > 600);

      // Determine if we should use mobile layout or not, 600 here is
      // a common breakpoint for a typical 7-inch tablet.
      return shortestSide > 600;
    }
  }
  initData() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      nom=prefs.getString("nom")!;
      profil=prefs.getString("profil")!;
    });

  }
  Drawer _getMailAccountDrawerr() {
    Text email = new Text(
      "kheuche@gmail.com",
      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15.0),
    );

    Text name = new Text(
      "Cheikh gueye",
      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15.0),
    );

    return Drawer(
        child: Column(
          children: <Widget>[
            Container(
              height: 200,
              color: Theme.of(context).accentColor,
              child: Column(
                children: [
                  Container(
                    decoration:  BoxDecoration(


                      border: Border(

                        bottom: BorderSide( //                    <--- top side
                          color: Colors.grey,
                          width: 1.0,
                        ),
                      ),),

                    height: 100,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top:25),
                          child:    Image.asset(
                            'assets/logo.png',
                            height:65,
                            width: 60,
                            alignment: Alignment.bottomCenter,
                            fit: BoxFit.contain,
                          ),
                        ),
                        SizedBox(width: 10,),
                        Column(
                          children: [
                            SizedBox(height: 40,),
                            Text('République du sénégal',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 18),),
                            SizedBox(height: 10,),
                            Text('Un Peuple - Un But - Une Foi',style: TextStyle(color: Colors.white,fontSize: 12,fontWeight: FontWeight.w300),),

                          ],
                        )                      ],
                    ),
                  ),
                  Container(
                    height: 100,
                    child: Row(
                      children: [
                        Icon(
                          Icons.account_circle,
                          size: 50.0,
                          color: Colors.white,
                        ),
                        SizedBox(width: 10,),
                        Text(nom.toString(),style: TextStyle(color: Colors.white,fontSize: 12,fontWeight: FontWeight.bold),),
                        SizedBox(width: 10,),
                        Expanded(
    child:
    Text(profil!=null?profil:"",style: TextStyle(color: Colors.white,fontSize: 12,fontWeight: FontWeight.w300),)),


                      ],
                    ),
                  )
                ],
              ),
            ),






            /*  UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Theme.of(context).primaryColor),
              accountName: name,
              accountEmail: email,
              currentAccountPicture: Icon(
                Icons.account_circle,
                size: 50.0,
                color: Colors.white,
              ),
            ),*/
            Expanded(
              flex: 2,
              child: ListView.builder(
                  padding: EdgeInsets.only(top:0.0),
                  itemCount: drawerText.length,
                  itemBuilder: (context, position) {
                    return




                      ListTile(
                      leading: drawerIcons[position],
                      title: Text(drawerText[position],
                          style: TextStyle(fontSize: 15.0)),
                      onTap: () async{
                        SharedPreferences prefs = await SharedPreferences.getInstance();

                        this.setState(() {
                    //      titleBarContent = drawerText[position];
                        });
                        if(drawerText[position]== "Se déconnecter"){
                          prefs.remove("survoltoken");
                          print(drawerText[position]);

                          Navigator.push(context, MaterialPageRoute(
                              builder: (_) => Login ()
                          ));

                        } else if(drawerText[position]== "Demandes Permanentes"){
                          Navigator.push(context, MaterialPageRoute(
                              builder: (_) => Home ()
                          ));

                        } else if(drawerText[position]=="Demandes Ponctuelles"){
                          Navigator.push(context, MaterialPageRoute(
                              builder: (_) => DPonctuelles ()
                          ));
                        } else if( drawerText[position]== "Preavis Survol / Atter"){
                          Navigator.push(context, MaterialPageRoute(
                              builder: (_) => DPreavis ()
                          ));
                        }
                        else if( drawerText[position]=="Demandes Escales"){
                          Navigator.push(context, MaterialPageRoute(
                              builder: (_) => DEscale ()
                          ));
                        }
                        else if( drawerText[position]== "Tableau De Bord"){
                          Navigator.push(context, MaterialPageRoute(
                              builder: (_) => Dashbord()
                          ));
                        }
                        else if( drawerText[position]== "Liste autorisations"){
                          Navigator.push(context, MaterialPageRoute(
                              builder: (_) => Autori()
                          ));
                        }



                        else{
                        Navigator.pop(context);

                        }
                      },
                    );
                  }),
            )
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
   // print(isTablet(context));


    isTablet(context).then((value) => {
      isT=value,
      print(value)
    });
    return Scaffold(
   drawer: _getMailAccountDrawerr(),
        appBar: AppBar(


          backgroundColor: Theme.of(context).accentColor,
          toolbarHeight: 70,
        /*  iconTheme: IconThemeData(
              color: Colors.white,
              size: 400 // This isn't performing any changes
          ),*/

          //  automaticallyImplyLeading: false,

          // backgroundColor: Colors.blue[900],
          // backgroundColor: Colors.blue[900],
          // backgroundColor: Colors.blue[900],
          title: Container(
            // alignment: Alignment.topRight,
              child:Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Tableau De Bord",style: TextStyle(fontWeight: FontWeight.w400,
                      fontSize:  getValueForScreenType<double>(
                        context: context,
                        mobile: 10,
                        tablet:15,
                        desktop: MediaQuery.of(context).size.width/2- 40,
                      ),


                  ),),
                  Column(
                    children: [
                      Image.asset(
                        'assets/logo.png',
                        height:30,
                        width: 30,
                        fit: BoxFit.contain,
                      ),
                      SizedBox(height: 3,),
                      Text('République du sénégal',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 9),),
                      SizedBox(height: 3,),
                      Text('Un Peuple - Un But - Une Foi',style: TextStyle(color: Colors.white,fontSize: 6,fontWeight: FontWeight.w300),),

                    ],
                  )
                ],
              )
          ),



        ),
      backgroundColor: Colors.white,
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
             Container(
               color: Colors.transparent,
               height: 200,
               margin: EdgeInsets.only(top: 50,),
               child:
               ListView(
                 scrollDirection: Axis.horizontal,
                 children: [
                   SizedBox(width: 10,),
                   InkWell(
                     onTap: ()=>{
                     Navigator.push(context, MaterialPageRoute(
                     builder: (_) =>Home ()   /// Login ()
                     ))
                     },
                     child: top(context,"Demandes Permanentes","#ff9900","#ff9900",nbrPerm,"assets/1.png"),
                   ),
                   SizedBox(width: 10,),
               InkWell(
                   onTap: ()=>{
                     Navigator.push(context, MaterialPageRoute(
                         builder: (_) =>DPonctuelles ()   /// Login ()
                     ))
                   },
                 child:
                 top(context,"Demandes Ponctuelles","#f26ca6","#b2195c",nbrPonc,"assets/2.png")),
                   SizedBox(width: 10,),
               InkWell(
                   onTap: ()=>{
                     Navigator.push(context, MaterialPageRoute(
                         builder: (_) =>DPreavis ()   /// Login ()
                     ))
                   },
                 child:
                 top(context,"Préavis","#1bb7ea","#0d8873",nbrPreavis,"assets/3.png")),
                   SizedBox(width: 10,),
               InkWell(
                   onTap: ()=>{
                     Navigator.push(context, MaterialPageRoute(
                         builder: (_) =>DEscale ()   /// Login ()
                     ))
                   },
                 child:
                 top(context,"Demandes escales","#a7ff49","#345f06",nbrEsc,"assets/4.png")),
                   SizedBox(width: 10,),


                 ],
               ),
             ),
              SizedBox(height: 20,),
              Container(
                color: Colors.transparent,
                height:MediaQuery.of(context).size.height/2,
                margin: EdgeInsets.only(top: 20,),
                child:
                ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                  //  SizedBox(width: 10,),
                    bottum(context,"pie","#ffffff","#000000",isT),
                //    SizedBox(width: 10,),
                //    bottum(context,"pie","#ffffff","#0091ff"),
                //    SizedBox(width: 10,),


                  ],
                ),
              ),

              SizedBox(height: 20,),
              Container(
                color: Colors.transparent,
                height:MediaQuery.of(context).size.height/2,
                margin: EdgeInsets.only(top: 20,),
                child:
                ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    //  SizedBox(width: 10,),,is
                    bottum(context,"pies","#ffffff","#000000",isT),
                    //    SizedBox(width: 10,),
                    //    bottum(context,"pie","#ffffff","#0091ff"),
                    //    SizedBox(width: 10,),


                  ],
                ),
              ),
              SizedBox(height: 30,),
            ],
          ),
        ),
      )

    );
  }

  Widget top(BuildContext context,text,bg,borc,nbr,img){

    return Container(
      width: 187,//MediaQuery.of(context).size.width/4-10,
      height:100,  //MediaQuery.of(context).size.height/3-10,
      decoration: myBoxDecoration(bg,"#808080") ,
      child: Center(
        child:Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 10,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Material(
                elevation: 20.0,

                borderRadius: BorderRadius.all(Radius.circular(100)),
            child:
            Container(

              width: 90,
              height: 90,
              decoration: BoxDecoration(
               color: HexColor ("#FFFFFF"),
                border: Border.all(
                    color:Colors.grey,
                    width: 1.3
                ),
                borderRadius: BorderRadius.all(
                    Radius.circular(100) //         <--- border radius here
                ),

              ),
              alignment: Alignment.center,
              child: Center(
                child:Image.asset(img),
              ),
            )),SizedBox(width: 20,),Center(child: Text(nbr,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),)
          ],),
            SizedBox(height: 32,),

           Container(
           //  margin: EdgeInsets.only(top: 30),
             child:  ColorizeAnimatedTextKit(
               onTap: () {
                 print("Tap Event");
               },
               text: [
                 text,


               ],
               textStyle: TextStyle(
                   fontSize: 15.0,
                   fontFamily: "Horizon",
                   fontWeight: FontWeight.bold
               ),
               colors: [
                 Colors.black,
                 Color(0xfff9e0cb),
                 Color(0xffe7b7cc),
                 Color(0xff77cbbe),
                 Color(0xffd23aed)

               ],
             ),
           ),
            SizedBox(height: 10,),
            Container(height: 1,color: Colors.grey,width: 150,),
          ],
        )
      )



    );

  }
  /*
   var nbrPerm="10";
      var nbrPreavis="8";
      var nbrPonc="100";
      var  nbrEsc="27";
   */
  Widget bottum(BuildContext context,text,bg,borc,ist){
    return Container(
      margin: EdgeInsets.only(left:10,right: 10 ),
      width: MediaQuery.of(context).size.width-20,
      height:MediaQuery.of(context).size.height/2,
      decoration: myBoxDecoration(bg,borc) ,
      child: Container(

        child:text=="pie"?MyHomePage (dp:nbrPerm ,dpo: nbrPonc,dpr:nbrPreavis ,de:  nbrEsc,ist:ist)//BarChartSample3()//
            :(text=="pies"?PieChartSample2(dp:nbrPerm ,dpo: nbrPonc,dpr:nbrPreavis ,de:  nbrEsc,ist:ist):LineChartSample1())
      ),



    );

  }
  BoxDecoration myBoxDecoration(colors,borcolors) {
    return BoxDecoration(
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.06),
          spreadRadius: 10,
          blurRadius: 1,
          offset: Offset(0, 19), // changes position of shadow
        ),
      ],
      color:Colors.white,// HexColor (colors),
      border: Border.all(
          color: HexColor (borcolors),
          width: 1.3
      ),
      borderRadius: BorderRadius.all(
          Radius.circular(5) //         <--- border radius here
      ),

    );
  }
}
class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}