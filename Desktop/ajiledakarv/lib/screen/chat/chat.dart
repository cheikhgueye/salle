import 'package:ajiledakarv/bottom_navigation_bar.dart';
import 'package:ajiledakarv/common/color.dart';
import 'package:ajiledakarv/models/Region.dart';
import 'package:ajiledakarv/models/chat_model.dart';
import 'package:ajiledakarv/screen/explorer/explore.dart';
import 'package:ajiledakarv/screen/results/resultats.dart';

import 'package:ajiledakarv/service/sizeconfig.dart';
import 'package:ajiledakarv/widgets/msg.dart';
import 'package:ajiledakarv/widgets/userMsg.dart';
import 'package:delayed_display/delayed_display.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:intl/intl.dart';

import '../../services/apiService.dart';

class ChatPage extends StatefulWidget {
  RegionModel region;
   ChatPage({super.key, required this.region});

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {


  bool  choosing =false;
  int step=-1;
  int typeId =0;
  int placeId=0;

  int zoneId=0;
  List centres=[];
  List places=[];
  List zones=[];

  bool _isSelected =false;
  String selectedcentre="";
  String selectedzone="";
  String selectedplace="";
  var show;
  final ScrollController _scrollController = ScrollController();



  getMess(path) async{
    print(path);
    var response;
    List msg =[];
    await  ApiService().getData(path).then((value) =>{
      response=value

    });
    setState(() {
    step=step+1;
    });
    if(step==0){
      setState(() {
       centres=response["data"];
      });
    } else if(step==1){
      setState(() {
        zones=response["data"];
      });
    } else if(step==2){
      setState(() {
        places=response["data"];
      });
    } else {
      setState(() {
        show=response["data"];
      });

    }
    /*if(step<2){
      msg=response["data"];
      if(msg.length>0){
        setState(() {
          step=step+1;
        });
      }
    }

    msg.forEach((e) {
      setState(() {

        messages.add(Message(
          text:step==0? e["type"]:(step==1?e["zone_name"]:"${e["name"]}-${e["phone"]}"),
          isUser: false,
          id: e["id"],
        ));
      });
    });
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
    });
    setState(() {
      choosing=false;
    });*/
   /* WidgetsBinding.instance.addPostFrameCallback((_) {
      _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
    });*/

  }
  final List<Message> messages = [
 /*   Message(
      text: 'Bonjour ! Comment puis-je vous aider ?',
      isUser: false,
      id: 0,
    ),*/
  ];

  final TextEditingController _textController = TextEditingController();

  void _sendMessage(String text) {
    /*  setState(() {
      _textController.clear();
      _messages.add(Message(
        text: 'Je suis un chatbot. Désolé, je ne sais pas comment vous aider pour cela.',
        isUser: false,
      ));
    });

      // simulate chatbot response after a short delay
      Future.delayed(Duration(seconds: 1), () {
        _messages.add(Message(text: text, isUser: true));
    });*/
  }
  userMessage(id,msg) async{
   /* var   path="";

    if(!choosing){
      setState(() {
        messages.add(Message(text: msg, isUser: true, id: id,));
      });
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
      });
      setState(() {
        choosing=true;
      });
      if(step==0){
        path = "centres/zone/list/${id}/";
        setState(() {
          type=id;
        });
      } else if(step==1){
        path = "places/search/${type}/${id}";
        setState(() {
          zone=id;
        });
      } else if(step==2){
        path = "places/show/${id}";
        setState(() {
          zone=id;
        });
      }


      await  getMess(path);*/


   // }




  }

  @override
  void initState() {
    // TODO: implement initState
    getMess("centres/list");
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
  var  blockSizeHorizontal = MediaQuery.of(context).size.width / 100;
 var   blockSizeVertical =MediaQuery.of(context).size.height / 100;
  double tailleListe;
  print("tailleListe");
  //print(tailleListe!);
 // print(snapshot.data.length ~/ 3);

  if (centres.length ~/ 3 == 0) {
    tailleListe =blockSizeVertical! * 8;
  } else if (centres.length ~/ 3 <= 1) {
    tailleListe = (centres.length ~/ 3).toDouble() * blockSizeVertical *11;
  } else if (centres.length ~/ 3 == 2) {
    tailleListe = (centres.length ~/ 3).toDouble() *
       blockSizeVertical *
        8;
  } else {
    tailleListe = (centres.length ~/ 3).toDouble() *
      blockSizeVertical *
        5;
  }
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(onPressed: () {
           Navigator.of(context).pop();
        }, icon: Icon(Icons.arrow_back_ios , color: Colors.black,)),
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text('Retour' , style: TextStyle(
          color: Colors.black,
        ),),
      ),
      body: SafeArea(
        child: ListView(
          controller: _scrollController,
          children: [
            Msg(
              msg: 'Salut , ravi de vous retrouver !',
              msg1: "Aidez-moi à vous guider en choisissant ci-dessous votre centre d'intérêt.",
            ),

            /////type grid

                    DelayedDisplay(
                    delay: Duration(seconds: 1),
    child: Container(
    margin: EdgeInsets.only(
    top: 15,
    left: blockSizeHorizontal * 5),
    padding: EdgeInsets.all(
    blockSizeVertical * 3),
    width: blockSizeHorizontal! * 90,
    //height: tailleListe,
    //color: Colors.white,
    decoration: BoxDecoration(
    boxShadow: [
    BoxShadow(
    color: Colors.grey.withOpacity(0.5),
    spreadRadius: 0.5,
    blurRadius: 2,
    offset: Offset(
    0, 1), // changes position of shadow
    ),


    ],
        color: Colors.grey.shade200,

    borderRadius:
    BorderRadius.all(Radius.circular(20))),
    child: GridView.count(
      //childAspectRatio: 0.7, // set the aspect ratio to control the height of items


      //   color: Colors.grey[200], // set the background color of the grid
    //controller: controller,
      shrinkWrap: true,
    primary: false,
    crossAxisCount: 3,
   childAspectRatio: 1.5,
    children:
    List.generate(centres.length, (index) {

    //print(_selectedIndexs);
    // print(_isSelected);
    // print(index);
    return GestureDetector(
    onTap: () {
    setState(() {
      setState(() {

        selectedcentre=centres[index]["type"];
        setState(() {
          typeId=centres[index]["id"];
        });
        WidgetsBinding.instance.addPostFrameCallback((_) {
          _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
        });
        var path= "centres/zone/list/${centres[index]["id"]}/${widget.region.id}";
        print(path);
        getMess(path);
      });
    });
    },
    child: DelayedDisplay(
    delay: Duration(seconds: 1),
    child: Container(

      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: selectedcentre==centres[index]["type"]?Colors.orangeAccent:Colors.white,
         borderRadius: BorderRadius.circular(5),
      ),
       padding: EdgeInsets.all(5),
      margin: EdgeInsets.all(5),
      child:  Text(centres[index]["type"],
      style: TextStyle(
      color:  selectedcentre==centres[index]["type"]?Colors.white:Colors.black,fontSize: 12
      ,)
    ),
    /*style: TextStyle(
    color: Colors.transparent,
    fontSize: blockSizeHorizontal *
    2.5,
    shadows: [
    Shadow(
    color: selectedcentre==centres[index]["type"]?

    Colors.yellow
        : Colors.blue,
    offset: Offset(0, -3))
    ],
    decorationColor: _isSelected
    ? Colors.yellow[800]
        : Color.fromARGB(
    255, 63, 156, 232),
    decoration:
    TextDecoration.underline)*/),
    ),
    );
    }),
    ),
    )),/////

            ///Affiche le typpe selected
            Visibility(
              visible: selectedcentre!="",
    child: UserMsg(msg:  selectedcentre,)


    )

    ,
    Visibility(
    visible: selectedcentre!="",
    child:
            Msg(msg: 'Vous avez choisis ${selectedcentre}',
              msg1: 'Choisissez une zone',)),

            ///grid zones
            Visibility(
              visible: zones.length!=0 && selectedcentre!="",
              child:                     /////type grid

              DelayedDisplay(
                  delay: Duration(seconds: 1),
                  child: Container(
                    margin: EdgeInsets.only(
                        top: 15,
                        left: blockSizeHorizontal * 5),
                    padding: EdgeInsets.all(
                        blockSizeVertical * 3),
                    width: blockSizeHorizontal! * 90,
                    //height: tailleListe,
                    //color: Colors.white,
                    decoration: BoxDecoration(
                      /*  boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 0.5,
                            blurRadius: 2,
                            offset: Offset(
                                0, 1), // changes position of shadow
                          ),


                        ],*/
                        color: Colors.grey.shade200,

                        borderRadius:
                        BorderRadius.all(Radius.circular(20))),
                    child: GridView.count(
                      //controller: controller,
                      shrinkWrap: true,
                      primary: false,
                      crossAxisCount: 3,
                      childAspectRatio: 1.5,
                      children:
                      List.generate(zones.length, (index) {

                        //print(_selectedIndexs);
                        // print(_isSelected);
                        // print(index);
                        return GestureDetector(
                          onTap: () {
                          var  path = "places/search/${typeId}/${zones[index]["id"]}";
                          print( path );
                           getMess(path);

                              setState(() {
                                zoneId=zones[index]["id"];
                               selectedzone=zones[index]["zone_name"];
                                WidgetsBinding.instance.addPostFrameCallback((_) {
                                  _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
                                });

                            });

                          },
                          child: DelayedDisplay(
                            delay: Duration(seconds: 1),
                            child: Container(
                            //  width: 100,

                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                        color: selectedzone==zones[index]["zone_name"]?Colors.orangeAccent:Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        ),
                        padding: EdgeInsets.all(5),
                        margin: EdgeInsets.all(5),
                        child:  Text(
                            zones[index]["zone_name"],
                            style: TextStyle(
                              color: selectedzone==zones[index]["zone_name"]?Colors.white:Colors.black,

                          ),
                        )


                            /*Text(zones[index]["zone_name"],
                                style: TextStyle(
                                    color: Colors.transparent,
                                    fontSize: blockSizeHorizontal! *
                                        2.5,
                                    shadows: [
                                      Shadow(
                                          color: _isSelected
                                          //Colors.yellow![800]
                                              ? Color(1)
                                              : Color.fromARGB(
                                              255, 63, 156, 232),
                                          offset: Offset(0, -3))
                                    ],
                                    decorationColor: _isSelected
                                        ? Colors.yellow[800]
                                        : Color.fromARGB(
                                        255, 63, 156, 232),
                                    decoration:
                                    TextDecoration.underline)),*/
                            )  ),
                        );
                      }),
                    ),
                  )),/////

            ),

            ///Affiche la  zone selected


            Visibility(
                visible: selectedzone!="",
                child: UserMsg(msg: selectedzone,)

                ),
            Visibility(
                visible: selectedzone!="",
                child: Msg(msg: "Super! vous avez choisi ${selectedzone}", msg1: 'Voici la liste des ${selectedcentre} qui se trouve à ${selectedzone}',)

            ),
        Visibility(
          visible: selectedzone!="",
          child:
            DelayedDisplay(
                delay: Duration(seconds: 2),
                child: TextButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (_) {
                            return Resultat(idType: typeId, idZone:zoneId,zone: selectedzone,);
                          },
                        ),
                      );
                    },
                    child: Container(
                        margin: EdgeInsets.only(
                            top: 30,
                            left: 50),
                        height:blockSizeVertical! * 5,
                        width:blockSizeHorizontal! * 20,
                        decoration: BoxDecoration(
                            color: Color.fromARGB(255, 114, 110, 110),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.1),
                                spreadRadius: 1,
                                blurRadius: 1,
                                offset: Offset(
                                    0, 3), // changes position of shadow
                              ),
                            ],
                            borderRadius:
                            BorderRadius.all(Radius.circular(15))),
                        child: Center(
                          child: Text(
                            "Explorer",
                            style: TextStyle(color: Colors.white),
                          ),
                        ))))),


            SizedBox(height: 30,)
            ///grid place
          /*  Visibility(
              visible: places.length!=0 && selectedzone!="",
              child:                     /////type grid

              DelayedDisplay(
                  delay: Duration(seconds: 2),
                  child: Container(
                    margin: EdgeInsets.only(
                        top: 15,
                        left: blockSizeHorizontal * 5),
                    padding: EdgeInsets.all(
                        blockSizeVertical * 3),
                    width: blockSizeHorizontal! * 90,
                    //height: tailleListe,
                    //color: Colors.white,
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 0.5,
                            blurRadius: 2,
                            offset: Offset(
                                0, 1), // changes position of shadow
                          ),


                        ],
                        color: Colors.grey.shade200,

                        borderRadius:
                        BorderRadius.all(Radius.circular(20))),
                    child: GridView.count(
                      //controller: controller,
                      shrinkWrap: true,
                      primary: false,
                      crossAxisCount: 3,
                      childAspectRatio: 2.5,
                      children:
                      List.generate(places.length, (index) {

                        //print(_selectedIndexs);
                        // print(_isSelected);
                        // print(index);
                        return GestureDetector(
                          onTap: () {

                         var  path = "places/show/${places[index]["id"]}";
                            getMess(path);
                            print(show);

                            setState(() {
                              placeId=zones[index]["id"];
                              selectedplace=places[index]["name"];
                              WidgetsBinding.instance.addPostFrameCallback((_) {
                                _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
                              });

                            });

                          },
                          child: DelayedDisplay(
                            delay: Duration(seconds: 1),
                            child: Container(

                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                        color: selectedplace==places[index]["name"]?Colors.orangeAccent:Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        ),
                        padding: EdgeInsets.all(5),
                        margin: EdgeInsets.all(10),
                        child: Text(
                          places[index]["name"].toString(),
                        style: TextStyle(
                        color: selectedplace==places[index]["name"]?Colors.white:Colors.black,
                        ),
                        ),)




                          ),
                        );
                      }),
                    ),
                  )),/////

            ),*/






          ],
        ),
      ),
    );
  }
}


