import 'package:ajiledakarv/auth/begin.dart';
import 'package:ajiledakarv/auth/login.dart';
import 'package:ajiledakarv/utils/const.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
class MonCompte extends StatefulWidget {

  const MonCompte({super.key});

  @override
  State<MonCompte > createState() => _MonCompteState();
}

class _MonCompteState extends State<MonCompte > {
  String? userName;
  initData() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {

      userName=prefs.getString(APIConstants.CONNECTED_USER_NAME);

    });
  }

@override
  void initState() {
    // TODO: implement initState
  initData();
  super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:userName==null? Center(child:GestureDetector(
        onTap: () =>    Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Login())),
        child: Container(
          width: MediaQuery.of(context).size.width * 0.7,
          height: 45,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.orangeAccent,
          ),
          child: Center(
            child: Text(
              'Connexion',
              style: GoogleFonts.inter(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
          ),
        ),
      )):


      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
              height: 100,
              width: 100,
              child: const CircleAvatar(
               backgroundImage: AssetImage('assets/avatar.png'),
              ),
              
            ),
            Text(userName!=null?userName!:"" ,


              style: GoogleFonts.inter(
              fontSize: 14,
              fontWeight: FontWeight.w600,
            ),

            ),
            const SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {
                
              },
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 20 , vertical: 5),
                padding: const EdgeInsets.all(15),
                decoration: BoxDecoration(
                              color: Colors.grey[300],
            
                  borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Inviter des amis' , style: GoogleFonts.inter( 
                      fontSize: 14,
            
                    ),),
                    const Icon(Icons.arrow_forward_ios)
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                
              },
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 20 , vertical: 5),
                padding: const EdgeInsets.all(15),
                decoration: BoxDecoration(
                              color: Colors.grey[300],
            
                  borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Mes Préférences' , style: GoogleFonts.inter( 
                      fontSize: 14,
            
                    ),),
                    const Icon(Icons.arrow_forward_ios)
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: ()  async{
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.clear();

                Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Begin ()));
              },
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 20 , vertical: 5),
                padding: const EdgeInsets.all(15),
                decoration: BoxDecoration(
                              color: Colors.grey[300],
            
                  borderRadius: BorderRadius.circular(10)
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Se deconnecter' , style: GoogleFonts.inter( 
                      fontSize: 14,
            
                    ),),
                    const Icon(Icons.arrow_forward_ios)
                  ],
                ),
              ),
            ),
          ],
        ),


    );
  }
}