import 'package:ajiledakarv/auth/login.dart';
import 'package:ajiledakarv/common/color.dart';
import 'package:ajiledakarv/models/Country.dart';
import 'package:ajiledakarv/screen/explorer/explore.dart';
import 'package:ajiledakarv/services/apiService.dart';
import 'package:ajiledakarv/utils/HexaColor.dart';
import 'package:ajiledakarv/widgets/loader.dart';
import 'package:country_list_pick/country_list_pick.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:motion_toast/motion_toast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ajiledakarv/utils/const.dart';

import '../bottom_navigation_bar.dart';



class Begin  extends StatefulWidget {
  const Begin  ({super.key});

  @override
  State<Begin > createState() => _beginState();
}

class _beginState extends State<Begin  > {

  List<CountryModel> countries=[];
  bool _loading=false;
  final _countryKey = GlobalKey<FormFieldState>();


  getDatas() async{
    setState(() {
      _loading=true;


    });

    var response;
    await  ApiService().getData("countrie/list").then((value) =>{
      response=value

    });
    setState(() {
      _loading=false;
      countries=CountryModel.fromJsonList(response["data"]);

    });
  }


  @override
  void initState() {
    // TODO: implement initState
    getDatas();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {


   
     var h = MediaQuery.of(context).size.height;
    var w = MediaQuery.of(context).size.width;
    return
      Loader(
      loadIng: _loading,
      color: Colors.black,
      opacity: .6,
      child:Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 50),
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(vertical: w * 0.14),
              
                child:
                    Image(image: AssetImage('assets/images/logo-agil.png')),
                ),
                Form(
                  child: Column(
                    children: [
                      DropdownButtonFormField(
                        key: _countryKey,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          filled: true,
                          fillColor: HexColor("#F6F6F6"),
                          alignLabelWithHint: true,
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          contentPadding: EdgeInsets.fromLTRB(10, 0, 5, 10),
                          labelText: 'Pays',
                          hintText: 'Choisir un pays',
                        ),

                        //  value: dropdownvalue,
                        icon: const Icon(Icons.keyboard_arrow_down),
                        /*  validator: FormBuilderValidators.compose([
                                                                  FormBuilderValidators.required(
                                                                      errorText: "la région est  obligatoire!!!"),
                                                                  /*  FormBuilderValidators.minLength(3,
                                                           errorText:
                                                      'Le prenom doit contenir minimum 3 carateres !!!'),*/
                                                             ]),*/
                        items: countries.map((CountryModel items) {
                          return DropdownMenuItem(
                            value: items,
                            child:Row(
                              children: [
                                Image.network(
                                  "${APIConstants.IMG_BASE_URL}${items.icon}",
                                  width: 24,
                                  height: 24,
                                ),
                                SizedBox(width: 8),
                                Text(items.nom_fr_fr),
                              ],
                            ),



                          );
                        }).toList(),
                        // After selecting the desired option,it will
                        // change button value to selected value
                        onChanged: (CountryModel? newValue) async {
                           setState((){
                             _loading=true;
                           });
                           Future.delayed(const Duration(milliseconds: 500), () {
                             Navigator.push(
                                 context,
                                 MaterialPageRoute(
                                     builder: (context) =>  bnv(country_d_code:newValue,)));

                           });


                          setState(() {
                       //     country=newValue;
                          });
                       //   await getRegionsByCountries();


                        },
                      )


                      /*Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width * 0.9,
                        padding: EdgeInsets.symmetric(horizontal: 12),
                        decoration: BoxDecoration(
                          color: Colors.grey[300],
                            borderRadius: BorderRadius.circular(100),
                            ),
                        child: CountryListPick(



                          appBar: AppBar(
                            backgroundColor: Colors.orangeAccent,
                            title: Text('Choisir un pays'),
                          ),
                          theme: CountryTheme(
                            isShowFlag: true,
                            isShowTitle: true,
                            isShowCode: false,
                            isDownIcon: true,
                            showEnglishName: true,
                          ),
                          //

                           useSafeArea: true,
                            // Set default value
                            initialSelection: '+221',
                            // or
                            // initialSelection: 'US'
                            onChanged: (CountryCode? code) async {
                              var response;
                              final SharedPreferences prefs = await SharedPreferences.getInstance();
                              prefs.setString(APIConstants.COUNTRY_CODE, code!.code!);
                              prefs.setString(APIConstants.COUNTRY_D_CODE, code.dialCode!);
                             await  ApiService().getData("countrie/${code.code}").then((value) =>{
                                response=value

                              });
                              print(response);
                              if(response["data"]==null){
                                MotionToast.error(
                                    title:  Text("Erreur"),
                                    description:  Text("Ce pays n'est pas pris en charge")
                                ).show(context);
                              } else if(response["code"]==200){
                                prefs.setString(APIConstants.COUNTRY_ID, CountryModel.fromJson(response["data"]).id.toString());
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>  bnv(country_d_code: code.dialCode!,)));

                              }
                           /*  */
                              print(code.name);
                              print(code.code);
                              print(code.dialCode);
                              print(code.flagUri);
                            },
                            // Whether to allow the widget to set a custom UI overlay
                            useUiOverlay: true,
                            // Whether the country list should be wrapped in a SafeArea


                        ),
                      ),*/,
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 45, 0, 8),
                        child: Text(
                          'Choisissez votre pays!',
                          style:GoogleFonts.inter(
                            fontSize: 20,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      ),
                      SizedBox(height: 12,),
                      Container(
                        child: Text(
                          'Votre guide digital culturel , touristique  , culinaire ... africain',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.inter(
                            fontSize: 12,
                            fontWeight: FontWeight.w300
                          ),
                        ),
                      ),
                      SizedBox(
                       height: h * .052,
                      ),
                    GestureDetector(
                      onTap: () =>    Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Login())),
                      child: Container(
                       width: w * 0.7,
                        height: 45,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.orangeAccent,
                        ),
                        child: Center(
                          child: Text(
                            'Connexion',
                            style: GoogleFonts.inter(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                    ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "j'ai deja un compte :",
                              style:GoogleFonts.inter(
                                fontSize: 12,
                                fontWeight: FontWeight.w300
                              ),
                            ),
                            TextButton(
                              onPressed: () {
                               Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Login()));
                                  
                              },
                              child: Text('me connecter !',
                                  style: GoogleFonts.inter(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                                color: primary
                              ),),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    ));
  }
}
