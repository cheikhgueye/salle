import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class RewardAdDialog extends StatefulWidget {
 // final RewardedAd rewardedAd;

  RewardAdDialog();

  @override
  _RewardAdDialogState createState() => _RewardAdDialogState();
}

class _RewardAdDialogState extends State<RewardAdDialog> {
  @override
  void initState() {
    super.initState();
 /*   widget.rewardedAd.fullScreenContentCallback =
        FullScreenContentCallback(onAdDismissedFullScreenContent: (ad) {
          ad.dispose();
          Navigator.pop(context, true); // utilisateur a regardé la pub
        }, onAdFailedToShowFullScreenContent: (ad, error) {
          ad.dispose();
          Navigator.pop(context, false); // utilisateur n'a pas regardé la pub
        });*/

  }

  @override
  void dispose() {
  //  widget.rewardedAd.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Regarder une publicité'),
      content: Text('Voulez-vous regarder une publicité en récompense pour continuer ?'),
      actions: <Widget>[
        TextButton(
          child: Text('NON'),
          onPressed: () {
            Navigator.pop(context, false); // utilisateur n'a pas regardé la pub
          },
        ),
        ElevatedButton(

          child: Text('OUI'),
          onPressed: () async {
            Navigator.pop(context, true); // utilisateur n'a pas regardé la pub

            /*  await widget.rewardedAd.show(
              onUserEarnedReward: (ad, reward) {
                // récompenser l'utilisateur ici
              },*/


          //  );
          },
        ),
      ],
    );
  }
}
