import 'dart:convert';

class SNVModel {
  bool isPub;
  String img;
  String text_fr;
  String text_en;
  String audio;
  String video;
  String link;
  String btext;
  SNVModel(
      {this.isPub: false,
      this.img: "",
      this.text_fr: "",
      this.text_en: "",
      this.audio: "",
      this.video: "",
      this.link: "",
      this.btext: ""});

  SNVModel.fromJson(Map<String, dynamic> map)
      : isPub = map['isPub'] ?? false,
        img = map['img'] ?? "",
        text_fr = map['text_fr'] ?? "",
        text_en = map['text_en'] ?? "",
        audio = map['audio'] ?? "",
        video = map['video'] ?? "",
        btext = map['btext'] ?? "",
        link = map['link'] ?? "";

  Map<String, dynamic> toJson() => {
        "isPub": isPub,
        "img": img,
        "text_fr": text_fr,
        "text_en": text_en,
        "audio": audio,
        "video": video,
        "link": link,
        "btext": btext
      };
  static List<SNVModel> fromJsonList(List list) {
    if (list == null) return [];
    return list.map((item) => SNVModel.fromJson(item)).toList();
  }
}
