import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:snapvoyance/repository.dart';
import 'package:snapvoyance/tani.dart';
import 'package:snapvoyance/utils/HexaColor.dart';
import 'package:snapvoyance/widgets/loader.dart';
import 'package:app_tracking_transparency/app_tracking_transparency.dart';
import 'package:paydunya/paydunya.dart';
import 'package:vpn_check/vpn_check.dart';
import 'package:panara_dialogs/panara_dialogs.dart';
import 'package:app_settings/app_settings.dart';
import 'package:flutter/services.dart';

class BeginPage extends StatefulWidget {
  final ResponseRepository? rRepository;
  BeginPage({
    Key? key,
    this.rRepository,
  }) : super(key: key);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<BeginPage> {
  bool _loading = false;
  String _authStatus = 'Unknown';
  final vpnChecker = VPNCheck();
  bool isVpnActive = false;

  checkVpn() async {
    try {
      isVpnActive = await vpnChecker.isVPNEnabled();
    } on VPNUnhandledException catch (e) {
      print(e);
    }

    /*  vpnChecker.vpnActiveStream.listen(
      (isActive) {
        setState(() {
          isVpnActive = isActive;
        });
        print('is vpn active: $isActive');
      },
      cancelOnError: false,
    );*/
  }

  @override
  void initState() {
    super.initState();

    initPlugin();
  }

  confirm() {
    PanaraConfirmDialog.show(
      context,
      title: "VPN",
      message: "Vous devez activer votre vpn ",
      confirmButtonText: "Confirmer",
      cancelButtonText: "Annuler",
      onTapCancel: () {
        Navigator.pop(context);
      },
      onTapConfirm: () async {
        AppSettings.openVPNSettings(
          asAnotherTask: true,
        );
        SystemNavigator.pop();
      },
      panaraDialogType: PanaraDialogType.warning,
      barrierDismissible: false, // optional parameter (default is true)
    );
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlugin() async {
    final TrackingStatus status =
        await AppTrackingTransparency.trackingAuthorizationStatus;
    setState(() => _authStatus = '$status');
    // If the system can show an authorization request dialog
    if (status == TrackingStatus.notDetermined) {
      // Show a custom explainer dialog before the system dialog
      await showCustomTrackingDialog(context);
      // Wait for dialog popping animation
      await Future.delayed(const Duration(milliseconds: 200));
      // Request system's tracking authorization dialog
      final TrackingStatus status =
          await AppTrackingTransparency.requestTrackingAuthorization();
      setState(() => _authStatus = '$status');
    }

    final uuid = await AppTrackingTransparency.getAdvertisingIdentifier();
    print("UUID: $uuid");
  }

  Future<void> showCustomTrackingDialog(BuildContext context) async =>
      await showDialog<void>(
        context: context,
        builder: (context) => AlertDialog(
          title: const Text('Dear User'),
          content: const Text(
            'We care about your privacy and data security. We keep this app free by showing ads. '
            'Can we continue to use your data to tailor ads for you?\n\nYou can change your choice anytime in the app settings. '
            'Our partners will collect data and use a unique identifier on your device to show you ads.',
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('Continue'),
            ),
          ],
        ),
      );

  _toggle() async {
    try {
      // les clés d'acces API et  d'environnement
      final keysApi = KeysApi(
        mode: Environment.test,
        masterKey: 'iFqQfy8g-XgsE-TSeu-zkGL-2YxOx4LJ5bjm',
        privateKey: 'test_private_YdtnYj4Vo2WBuT2ksFnY9MyJZ9i',
        token: '0MIyPYdYtbyO8IeRFfXl',
      );

      final paydunya = Paydunya(keysApi: keysApi);

      // la facturation
      const store = Store(name: 'Royal Event');
      const invoice = Invoice(totalAmount: 2000);

      const billing = Billing(
        store: store,
        invoice: invoice,
      );

      final checkoutInvoice =
          await paydunya.createChekoutInvoice(billing: billing);

      debugPrint("Token: ${checkoutInvoice.token}");

      // Renseigner les informations du Paiement
      final payerInfo = PayerInfo(
        fullName: 'Cheikh Gueye',
        phone: '774262278',
        otp: 808656,
        paymentToken: checkoutInvoice.token,
      );

      // Effectuer le paiement avec une méthode de paiement
      final response = await paydunya.pay(
        paymentMethod: PaymentMethod.orangeMoneySenegal,
        payerInfo: payerInfo,
      );

      // Afficher l'url de paiement de wave
      debugPrint("wave url: ${response.url}");

      // Vérifier le status du paiement
      final statusPaiement = await paydunya.verifyStatePayment(
        invoiceToken: checkoutInvoice.token,
      );

      // Afficher le status du paiement\
      debugPrint("Status: ${statusPaiement.status}");
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Loader(
        loadIng: _loading,
        color: Colors.black,
        opacity: .5,
        child: Scaffold(
          body: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/set.jpg'),
                  fit: BoxFit.cover,
                ),
                //   color: Colors.white.withOpacity(0.3), // 50% d'opacité
              ),
              child: Opacity(
                  opacity: 0.5, // 50% d'opacité pour l'image
                  child: Container(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    color: Colors.white,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          /*  Image.asset(
                'assets/logo.png',
                height: 150,
                width: 150,
              ),*/
                          SizedBox(height: 50),
                          Center(
                            child: Text(
                              'Choisissez une langue / Pàll naa bii jàng ci ',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          SizedBox(height: 30),
                          ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.black,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 30, vertical: 10),
                                textStyle: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              onPressed: () async {
                                setState(() {
                                  _loading = true;
                                });
                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();
                                prefs.setString("lg", "fr");
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Tani(
                                            rRepository: widget.rRepository!,
                                          )),
                                ).then((value) => {
                                      setState(() {
                                        _loading = false;
                                      })
                                    });

                                //Action pour choisir la langue française
                              },
                              child: Row(
                                children: [
                                  ClipOval(
                                    child: SizedBox.fromSize(
                                      size: Size.fromRadius(48), // Image radius
                                      child: Image.asset('assets/fr.png',
                                          fit: BoxFit.cover),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    'Français',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              )),
                          SizedBox(height: 20),
                          ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.black,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 30, vertical: 10),
                                textStyle: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              onPressed: () async {
                                setState(() {
                                  _loading = true;
                                });
                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();
                                prefs.setString("lg", "wo");
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Tani(
                                            rRepository: widget.rRepository!,
                                          )),
                                ).then((value) => {
                                      setState(() {
                                        _loading = false;
                                      })
                                    });

                                //Action pour choisir la langue française
                              },
                              child: Row(
                                children: [
                                  ClipOval(
                                    child: SizedBox.fromSize(
                                      size: Size.fromRadius(48), // Image radius
                                      child: Image.asset('assets/sn.png',
                                          fit: BoxFit.cover),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    'Wolof',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              )),
                          SizedBox(height: 20),
                          ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.black,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 30, vertical: 10),
                                textStyle: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              onPressed: () async {
                                if (!isVpnActive) {
                                  setState(() {
                                    _loading = true;
                                  });
                                  SharedPreferences prefs =
                                      await SharedPreferences.getInstance();
                                  prefs.setString("lg", "en");
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Tani(
                                              rRepository: widget.rRepository!,
                                            )),
                                  ).then((value) => {
                                        setState(() {
                                          _loading = false;
                                        })
                                      });
                                } else {
                                  confirm();
                                }

                                //Action pour choisir la langue française
                              },
                              child: Row(
                                children: [
                                  ClipOval(
                                    child: SizedBox.fromSize(
                                      size: Size.fromRadius(48), // Image radius
                                      child: Image.asset('assets/en.png',
                                          fit: BoxFit.cover),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    'English',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              )),
                          SizedBox(height: 20),
                          /*  ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.black,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 30, vertical: 10),
                                textStyle: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              onPressed: () async {
                                await _toggle();
                                //Action pour choisir la langue française
                              },
                              child: Row(
                                children: [
                                  ClipOval(
                                    child: SizedBox.fromSize(
                                      size: Size.fromRadius(48), // Image radius
                                      child: Image.asset('assets/en.png',
                                          fit: BoxFit.cover),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    'Payer',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              )),*/
                        ],
                      ),
                    ),
                  ))),
        ));
  }
}
