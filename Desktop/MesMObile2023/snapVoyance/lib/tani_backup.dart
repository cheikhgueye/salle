import 'dart:async';
import 'dart:io';
import 'dart:math';
import "dart:core";
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:just_audio/just_audio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:snapvoyance/repository.dart';
import 'package:snapvoyance/widgets/RewardAdDialog.dart';
import 'package:snapvoyance/widgets/loader.dart';
import 'package:app_tracking_transparency/app_tracking_transparency.dart';
import 'package:panara_dialogs/panara_dialogs.dart';

const String testDevice = 'YOUR_DEVICE_ID';
const int maxFailedLoadAttempts = 3;

class Tani extends StatefulWidget {
  final ResponseRepository rRepository;

  Tani({
    Key? key,
    required this.rRepository,
  }) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Tani> {
  var audio = AudioPlayer();
  bool _loading = false;
  final colorArr = [
    0xffd61745,
    0xff3569bc,
    0xffffffff,
    0xfff4862a,
    0xffeaed19,
    0xff329f64,
    0xff000000
  ];
//playArr is used to store values from 0-5 which corresponds each color => colorArr[playArr[n]]
  var playArr = [];

  String horoscope = "";

  var i = 0;

  bool watchReward = false;
  int currentIndex = 0;

  String sunsign = "Sunsign", time = "Time of Horoscope";

  // generates a new Random object

  //ads

  var titre = "Hypnotize votre amour";

  var result = "";
  var typead = "2";

  var pubImg;
  var pubLink;
  var voix = "";

  List<int> codes = [1, 2, 3];

  bool _isSpeck = false;
  bool _refusedReward = false;
  bool _rewardIsWatched = false;
  final _random = new Random();
  //serveurs
  var snTel = "";
  var snImg = "";
  var intTel = "";
  var intImg = "";
  String _authStatus = 'Unknown';

  List results = [];

  var flutterTts = FlutterTts();

  static final AdRequest request = AdRequest(
//    keywords: <String>['foo', 'bar'],
      //   contentUrl: 'http://foo.com/bar.html',
      //  nonPersonalizedAds: true,
      );

  //admob

  static const interstitialButtonText = 'InterstitialAd';
  static const rewardedButtonText = 'RewardedAd';
  static const rewardedInterstitialButtonText = 'RewardedInterstitialAd';
  static const fluidButtonText = 'Fluid';
  static const inlineAdaptiveButtonText = 'Inline adaptive';
  static const anchoredAdaptiveButtonText = 'Anchored adaptive';
  static const nativeTemplateButtonText = 'Native template';

  InterstitialAd? _interstitialAd;
  int _numInterstitialLoadAttempts = 0;

  RewardedAd? _rewardedAd;
  int _numRewardedLoadAttempts = 0;

  RewardedInterstitialAd? _rewardedInterstitialAd;
  int _numRewardedInterstitialLoadAttempts = 0;
  //end
  bool _bannerLoaded = false;
  parler(text) async {
    await flutterTts.setVolume(1);
    await flutterTts.setSpeechRate(0.4);
    await flutterTts.setPitch(0.9);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("prefs.getString(");
    print(prefs.getString("lg"));

    if (prefs.getString("lg") == "en") {
      await flutterTts.setLanguage("en-US");
    } else {
      await flutterTts.setLanguage("fr");
    }

    setState(() {});
    var result = flutterTts.speak(text);
    // setState(() => _isSpeck = true);
    result.then((value) => {setState(() => _isSpeck = true)});

    // if (result == 1) setState(() => ttsState = TtsState.playing);
  }

  wakh(url) async {
    setState(() => _isSpeck = true);
    await audio.setUrl(url);
    await audio.play();
  }

  stop() async {
    var result = await flutterTts.stop();
    audio.stop();

    setState(() {
      voix = "";
    });
    setState(() => _isSpeck = false);
  }

  void getResults() async {
    setState(() {
      _loading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var snp = "snp.json";
    if (prefs.get("lg") == "wo") {
      snp = "snp.json";
    } else if (prefs.get("lg") == "fr") {
      snp = "fr_snp.json";
    } else if (prefs.get("lg") == "en") {
      snp = "en_snp.json";
    }

    var index = _random.nextInt(codes.length);

    var res = [];
    await widget.rRepository.fetchResponse(snp).then((value) => res = value);
    print(res);

    setState(() {
      _loading = false;
      results = res;
    });
  }

  getNum() async {}

  _genereateList() {
    playArr = [
      "assets/cauri.png",
      "",
      "assets/cent.png",
      "assets/cauri.png",
      "",
      "assets/cauri.png",
      "assets/cauri.png",
      "assets/cauri.png",
      "",
      "assets/cauri.png",
      "assets/cauri.png",
      "",
      "assets/cauri.png",
      "",
      "assets/cauri.png",
      "assets/cola.png",
      "",
      "assets/cauri.png",
      "assets/corne.png",
      "assets/cauri.png",
      "",
      "assets/cauri.png",
      "",
      "assets/cauri.png",
      "assets/cauri.png",
    ];

    setState(() {});
  }

  _shuffle() {
    playArr.shuffle(Random());
    setState(() {});
  }

  getPubs() async {}
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _createInterstitialAd();
    _createRewardedAd();
    _createBanner();

    _genereateList();
    _shuffle();

    getResults();
    getPubs();

    getNum();

    //  initPlugin();

    _bannerAd!.load();

    position = Offset(0.0, height - 20);
  }

  double width = 100.0, height = 100.0;
  Offset? position;
  var countryCode;
  int intersIndex = 0;
  BannerAd? _bannerAd;

  String? _allowsVOIP;
  String? _carrierName;
  var _isoCountryCode = "";

  @override
  void dispose() {
    super.dispose();
    _interstitialAd?.dispose();
    _rewardedAd?.dispose();
    _rewardedInterstitialAd?.dispose();
    _bannerAd!.dispose();
  }

  void _createBanner() {
    _bannerAd = BannerAd(
      adUnitId: Platform.isAndroid
          ? 'ca-app-pub-7248255245937838/8764168418'
          : 'ca-app-pub-7248255245937838/5094502243',
      size: AdSize.banner,
      request: AdRequest(),
      listener: BannerAdListener(
        onAdLoaded: (_) => {
          setState(() {
            _bannerLoaded = true;
          }),
        },
        onAdFailedToLoad: (ad, error) => print('Ad failed to load: $error'),
      ),
    );
    //  _bannerAd!.dispose();
  }

  void _createInterstitialAd() {
    InterstitialAd.load(
        adUnitId: Platform.isAndroid
            ? 'ca-app-pub-7248255245937838/1592255405'
            : 'ca-app-pub-7248255245937838/7529093890',
        request: request,
        adLoadCallback: InterstitialAdLoadCallback(
          onAdLoaded: (InterstitialAd ad) {
            setState(() {
              _loading = false;
            });
            print('$ad loaded');
            _interstitialAd = ad;
            _numInterstitialLoadAttempts = 0;
            _interstitialAd!.setImmersiveMode(true);
            if (intersIndex == 0) {
              _showInterstitialAd();
              setState(() {
                intersIndex = intersIndex + 1;
              });
            }
          },
          onAdFailedToLoad: (LoadAdError error) {
            setState(() {
              _loading = false;
            });
            print('InterstitialAd failed to load: $error.');
            _numInterstitialLoadAttempts += 1;
            _interstitialAd = null;
            if (_numInterstitialLoadAttempts < maxFailedLoadAttempts) {
              _createInterstitialAd();
            }
          },
        ));
  }

  void _showInterstitialAd() {
    if (_interstitialAd == null) {
      print('Warning: attempt to show interstitial before loaded.');
      return;
    }
    _interstitialAd!.fullScreenContentCallback = FullScreenContentCallback(
      onAdShowedFullScreenContent: (InterstitialAd ad) =>
          print('ad onAdShowedFullScreenContent.'),
      onAdDismissedFullScreenContent: (InterstitialAd ad) {
        print('$ad onAdDismissedFullScreenContent.');
        ad.dispose();
        _createInterstitialAd();
      },
      onAdFailedToShowFullScreenContent: (InterstitialAd ad, AdError error) {
        print('$ad onAdFailedToShowFullScreenContent: $error');
        ad.dispose();
        _createInterstitialAd();
      },
    );
    _interstitialAd!.show();
    _interstitialAd = null;
  }

  void _createRewardedAd() {
    RewardedAd.load(
        adUnitId: Platform.isAndroid
            ? 'ca-app-pub-7248255245937838/5339928720'
            : 'ca-app-pub-7248255245937838/7337522202',
        request: request,
        rewardedAdLoadCallback: RewardedAdLoadCallback(
          onAdLoaded: (RewardedAd ad) {
            print('$ad loaded.');
            _rewardedAd = ad;
            _numRewardedLoadAttempts = 0;
          },
          onAdFailedToLoad: (LoadAdError error) {
            print('RewardedAd failed to load: $error');
            _rewardedAd = null;
            _numRewardedLoadAttempts += 1;
            if (_numRewardedLoadAttempts < maxFailedLoadAttempts) {
              _createRewardedAd();
            }
          },
        ));
  }

  void _showRewardedAd() {
    if (_rewardedAd == null) {
      print('Warning: attempt to show rewarded before loaded.');
      return;
    }
    _rewardedAd!.fullScreenContentCallback = FullScreenContentCallback(
      onAdShowedFullScreenContent: (RewardedAd ad) =>
          print('ad onAdShowedFullScreenContent.'),
      onAdDismissedFullScreenContent: (RewardedAd ad) {
        setState(() {
          watchReward = false;
        });
        print('$ad onAdDismissedFullScreenContent.');
        ad.dispose();
        _createRewardedAd();
      },
      onAdFailedToShowFullScreenContent: (RewardedAd ad, AdError error) {
        setState(() {
          watchReward = false;
        });
        print('$ad onAdFailedToShowFullScreenContent: $error');
        ad.dispose();
        _createRewardedAd();
      },
    );

    _rewardedAd!.setImmersiveMode(true);
    _rewardedAd!.show(
        onUserEarnedReward: (AdWithoutView ad, RewardItem reward) {
      print('$ad with reward $RewardItem(${reward.amount}, ${reward.type})');
    });
    _rewardedAd = null;
  }

  void _createRewardedInterstitialAd() {
    RewardedInterstitialAd.load(
        adUnitId: Platform.isAndroid
            ? 'ca-app-pub-3940256099942544/5354046379'
            : 'ca-app-pub-7248255245937838/7337522202',
        request: request,
        rewardedInterstitialAdLoadCallback: RewardedInterstitialAdLoadCallback(
          onAdLoaded: (RewardedInterstitialAd ad) {
            print('$ad loaded.');
            _rewardedInterstitialAd = ad;
            _numRewardedInterstitialLoadAttempts = 0;
          },
          onAdFailedToLoad: (LoadAdError error) {
            print('RewardedInterstitialAd failed to load: $error');
            _rewardedInterstitialAd = null;
            _numRewardedInterstitialLoadAttempts += 1;
            if (_numRewardedInterstitialLoadAttempts < maxFailedLoadAttempts) {
              _createRewardedInterstitialAd();
            }
          },
        ));
  }

  void _showRewardedInterstitialAd() {
    if (_rewardedInterstitialAd == null) {
      print('Warning: attempt to show rewarded interstitial before loaded.');
      return;
    }
    _rewardedInterstitialAd!.fullScreenContentCallback =
        FullScreenContentCallback(
      onAdShowedFullScreenContent: (RewardedInterstitialAd ad) =>
          print('$ad onAdShowedFullScreenContent.'),
      onAdDismissedFullScreenContent: (RewardedInterstitialAd ad) {
        print('$ad onAdDismissedFullScreenContent.');
        ad.dispose();
        _createRewardedInterstitialAd();
      },
      onAdFailedToShowFullScreenContent:
          (RewardedInterstitialAd ad, AdError error) {
        print('$ad onAdFailedToShowFullScreenContent: $error');
        ad.dispose();
        _createRewardedInterstitialAd();
      },
    );

    _rewardedInterstitialAd!.setImmersiveMode(true);
    _rewardedInterstitialAd!.show(
        onUserEarnedReward: (AdWithoutView ad, RewardItem reward) {
      print('$ad with reward $RewardItem(${reward.amount}, ${reward.type})');
    });
    _rewardedInterstitialAd = null;
  }

  askReward() async {
    /* bool? result = await showDialog(
      context: context,
      builder: (context) => RewardAdDialog(),
    );
    if (result != null && result) {
      setState(() {
        _refusedReward = false;
        watchReward = false;
        _rewardIsWatched = true;
      });
      _showRewardedAd();
      setState(() {
        //  currentIndex=currentIndex+1;
        //  watchReward=false;
      });
      // L'utilisateur a regardé la pub, continuer l'exécution normale ici.
    } else {
      setState(() {
        _refusedReward = true;
      });
      // L'utilisateur n'a pas regardé la pub ou il y a eu une erreur.
    }*/

    PanaraInfoDialog.show(
      context,
      title: "Pub",
      message: "Regardez une pub pour continuer",
      buttonText: "Continuer",
      onTapDismiss: () {
        setState(() {
          _refusedReward = false;
          watchReward = false;
          _rewardIsWatched = true;
        });
        _showRewardedAd();
        Navigator.pop(context);
      },
      panaraDialogType: PanaraDialogType.normal,
      barrierDismissible: false, // optional parameter (default is true)
    );
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlugin() async {
    final TrackingStatus status =
        await AppTrackingTransparency.trackingAuthorizationStatus;
    setState(() => _authStatus = '$status');
    // If the system can show an authorization request dialog
    if (status == TrackingStatus.notDetermined) {
      // Show a custom explainer dialog before the system dialog
      await showCustomTrackingDialog(context);
      // Wait for dialog popping animation
      await Future.delayed(const Duration(milliseconds: 200));
      // Request system's tracking authorization dialog
      final TrackingStatus status =
          await AppTrackingTransparency.requestTrackingAuthorization();
      setState(() => _authStatus = '$status');
    }

    final uuid = await AppTrackingTransparency.getAdvertisingIdentifier();
    print("UUID: $uuid");
  }

  Future<void> showCustomTrackingDialog(BuildContext context) async =>
      await showDialog<void>(
        context: context,
        builder: (context) => AlertDialog(
          title: const Text('Dear User'),
          content: const Text(
            'We care about your privacy and data security. We keep this app free by showing ads. '
            'Can we continue to use your data to tailor ads for you?\n\nYou can change your choice anytime in the app settings. '
            'Our partners will collect data and use a unique identifier on your device to show you ads.',
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('Continue'),
            ),
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Loader(
        loadIng: _loading,
        color: Colors.black,
        opacity: .5,
        child: Scaffold(
            floatingActionButton: _isSpeck == false
                ? Container()

                //   _normalDown()

                : Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      if (voix != "")
                        InkWell(
                          onTap: () {
                            wakh(voix);
                          },
                          child: Container(
                            //  margin: EdgeInsets.only(top:MediaQuery.of(context).size.width-200 ),
                            width: 50 * 1.6180339887,
                            height: 50,
                            margin: EdgeInsets.only(bottom: 45),
                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              color: Colors.transparent,
                              borderRadius: BorderRadius.circular(100),
                              image: new DecorationImage(
                                  fit: BoxFit.cover,
                                  image: AssetImage("assets/ecouter.png")),
                            ),
                          ),
                        ),
                      InkWell(
                        onTap: () {
                          stop();
                        },
                        child: Container(
                          //  margin: EdgeInsets.only(top:MediaQuery.of(context).size.width-200 ),
                          width: 50 * 1.6180339887,
                          height: 50,
                          margin: EdgeInsets.only(bottom: 45),
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(100),
                            image: new DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage("assets/relo.png")),
                          ),
                        ),
                      ),
                    ],
                  ),
            backgroundColor: Colors.black,
            appBar: AppBar(
                automaticallyImplyLeading: false,
                backgroundColor: Colors.black,
                title: Center(
                  child: Container(
                    alignment: Alignment.center,
                    //  margin: EdgeInsets.only(top:MediaQuery.of(context).size.width-200 ),
                    width: 320,
                    height: 50,
                    //  margin: EdgeInsets.only(bottom: 45),
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(100),
                      image: new DecorationImage(
                          fit: BoxFit.fill,
                          image: NetworkImage(
                              "https://cheikhgueye.github.io/snp.github.io/snvpub.png")),
                    ),
                  ),
                )),
            body: Stack(
              children: [
                Container(
                  //   padding: EdgeInsets.only(left: 20,right: 20),
                  child: ListView(
                    //   crossAxisAlignment: CrossAxisAlignment.center,

                    /// mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Stack(
                        children: [
                          Container(
                              width: MediaQuery.of(context).size.width,
                              height:
                                  (MediaQuery.of(context).size.height) / 1.2,
                              decoration: BoxDecoration(
                                //borderRadius: BorderRadius.circular(100),
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black,
                                      offset: Offset(0, 10),
                                      blurRadius: 10),
                                ],
                                image: new DecorationImage(
                                    fit: BoxFit.cover,
                                    colorFilter: new ColorFilter.mode(
                                        Colors.black.withOpacity(
                                            _isSpeck == false ? 0.9 : 0.5),
                                        BlendMode.dstATop),
                                    image: AssetImage(
                                      "assets/layou.png",
                                    )),
                              ),
                              child: Center(
                                  child: GridView.builder(
                                      shrinkWrap: true,
                                      itemCount: 25,
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 5),
                                      itemBuilder: (ctx, index) =>
                                          playArr[index] != ""
                                              ? Image.asset(
                                                  playArr[index],
                                                  width: playArr[index] !=
                                                          "assets/cauri.png"
                                                      ? 250
                                                      : 50,
                                                  height: playArr[index] !=
                                                          "assets/cauri.png"
                                                      ? 150
                                                      : 50,
                                                )
                                              : Container()))),
                          _isSpeck == true
                              ? Center(
                                  child: Stack(
                                  children: [
                                    Column(
                                      children: [
                                        Center(
                                          child: Container(
                                            margin: EdgeInsets.only(top: 40),
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            height: (MediaQuery.of(context)
                                                        .size
                                                        .width /
                                                    2) *
                                                1.6180339887,
                                            decoration: BoxDecoration(
                                              shape: BoxShape.rectangle,
                                              color: Colors.transparent,
                                              borderRadius:
                                                  BorderRadius.circular(100),
                                              image: new DecorationImage(
                                                  fit: BoxFit.cover,
                                                  image: AssetImage(
                                                      "assets/laf.gif")),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2,
                                          height: (MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  2) *
                                              1.6180339887,
                                          decoration: BoxDecoration(
                                            //   shape: BoxShape.rectangle,
                                            color: Colors.transparent,

                                            borderRadius:
                                                BorderRadius.circular(100),
                                            image: new DecorationImage(
                                                fit: BoxFit.contain,
                                                image: AssetImage(
                                                    "assets/wakh.gif")),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Center(
                                        child: Container(
                                            margin: EdgeInsets.only(
                                                top: MediaQuery.of(context)
                                                        .size
                                                        .width -
                                                    90),
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width -
                                                20,
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .width -
                                                20 * 1.6180339887,
                                            decoration: BoxDecoration(
                                              shape: BoxShape.rectangle,
                                              color: Colors.transparent,
                                              borderRadius:
                                                  BorderRadius.circular(100),
                                            ),
                                            child: SingleChildScrollView(
                                                child: Text(
                                              horoscope,
                                              style: TextStyle(
                                                fontFamily: 'Canterbury',
                                                fontSize: 44,
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            )
                                                /* MarqueeText(
             text: horoscope,
             style: TextStyle(
               fontFamily: 'Canterbury',

               fontSize: 44,
               color: Colors.white,
               fontWeight: FontWeight.bold,

             ),
             speed: 40,
           ),*/

                                                ))),
                                  ],
                                ))
                              : Container(),
                          _isSpeck == true
                              ? Container()
                              : Positioned(
                                  left: position!.dx,
                                  bottom:
                                      (MediaQuery.of(context).size.width - 20) *
                                          1.6180339887 /
                                          2,
                                  //top: position.dy - height + 20,
                                  child: Draggable(
                                    child: Stack(
                                      children: [
                                        Container(
                                          width: 130,
                                          height: 130 * 1.6180339887,
                                          decoration: BoxDecoration(
                                              shape: BoxShape.rectangle,
                                              color: Colors.transparent,
                                              borderRadius:
                                                  BorderRadius.circular(100),
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Colors.transparent,
                                                    offset: Offset(0, 10),
                                                    blurRadius: 10),
                                              ],
                                              image: new DecorationImage(
                                                  fit: BoxFit.cover,
                                                  // colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.9), BlendMode.dstATop),
                                                  image: AssetImage(
                                                      "assets/hand.png"))),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(top: 120),
                                          child: Image.asset(
                                            "assets/drag.png",
                                            width: 80,
                                          ),
                                        )
                                      ],
                                    ),
                                    feedback: Container(
                                        width: 130,
                                        height: 130 * 1.6180339887,
                                        padding: EdgeInsets.all(30),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          color: Colors.transparent,
                                          borderRadius:
                                              BorderRadius.circular(100),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Colors.transparent,
                                                offset: Offset(0, 10),
                                                blurRadius: 10),
                                          ],
                                          image: new DecorationImage(
                                              fit: BoxFit.cover,
                                              // colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.9), BlendMode.dstATop),
                                              image: AssetImage(
                                                  "assets/hand.png")),
                                        )),
                                    onDraggableCanceled: (Velocity velocity,
                                        Offset offset) async {
                                      SharedPreferences prefs =
                                          await SharedPreferences.getInstance();
                                      _shuffle();

                                      setState(() => position = offset);
                                      print("arafat");
                                      print(results.length);
                                      print(watchReward == true &&
                                          !_refusedReward &&
                                          !_rewardIsWatched);

                                      if (results.length == 0) {
                                        parler(
                                            "Il faut revenir demain , sinon je vous repète la meme chose car je te donne que 5 reponse par jour  ");
                                        Future.delayed(
                                            const Duration(milliseconds: 10),
                                            () {
                                          ///  _showInterstitialAd();
                                        });
                                        getResults();
                                      } else {
                                        if (watchReward == true &&
                                            !_refusedReward &&
                                            !_rewardIsWatched) {
                                          askReward();
                                        } else if (_refusedReward) {
                                          parler(
                                              "Il faut revenir demain , sinon je vous repète la meme chose car je te donne que 5 reponse par jour  ");
                                        } else {
                                          print("tak");
                                          var index =
                                              _random.nextInt(results.length);

                                          var element = results[index];
                                          results.length != 0
                                              ? results.removeAt(index)
                                              : null;

                                          if (currentIndex != 0 &&
                                              currentIndex == 3 &&
                                              !_rewardIsWatched) {
                                            setState(() {
                                              watchReward = true;
                                            });
                                          } else if ((prefs.getString("lg") ==
                                                  "wo") &&
                                              element != "") {
                                            setState(() {
                                              currentIndex = currentIndex + 1;
                                              voix = element;
                                            });

                                            await wakh(element);
                                          } else {
                                            setState(() {
                                              currentIndex = currentIndex + 1;
                                            });

                                            parler(element);
                                          }
                                        }
                                      }
                                    },
                                  ),
                                ),
                        ],
                      )
                    ],
                  ),
                ),

                /*    Positioned(top: 0,
              left: 0,
              right: 0,
              child:


              Container(
                child:  _isAdLoaded1 ==true? AdWidget(ad: _ad):SizedBox(),
                width: _isAdLoaded1 ==true?  _ad.size.width.toDouble():0,
                height: 72.0,
                alignment: Alignment.bottomCenter,
              )


          ),*/
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Column(
                    children: [
                      if (_bannerAd != null &&
                          _bannerAd!.size != null &&
                          _bannerLoaded)
                        Container(
                          width: _bannerAd!.size.width.toDouble(),
                          height: _bannerAd!.size.height.toDouble(),
                          child: AdWidget(ad: _bannerAd!),
                        ),
                      // Add other widgets here
                    ],
                  ),
                )
              ],
            )));
  }
}
