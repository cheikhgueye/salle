// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      throw UnsupportedError(
        'DefaultFirebaseOptions have not been configured for web - '
        'you can reconfigure this by running the FlutterFire CLI again.',
      );
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for macos - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyAaoWB8nuenKe-tr88dUphV9oOfEWrcTlQ',
    appId: '1:337761766016:android:d6890cf652ca58e2947465',
    messagingSenderId: '337761766016',
    projectId: 'wakana-c6f61',
    storageBucket: 'wakana-c6f61.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyD5Zz6jzZAoNsSza5AARzEwUw5cSzKot7E',
    appId: '1:337761766016:ios:8281a50082b2cbb8947465',
    messagingSenderId: '337761766016',
    projectId: 'wakana-c6f61',
    storageBucket: 'wakana-c6f61.appspot.com',
    androidClientId: '337761766016-i7o0ht0t2l86cjcd88dosb099nod4ir6.apps.googleusercontent.com',
    iosClientId: '337761766016-isve63sv65m8qdkoegthqj0m88j6p720.apps.googleusercontent.com',
    iosBundleId: 'com.wakana.1.61803398875',
  );
}
