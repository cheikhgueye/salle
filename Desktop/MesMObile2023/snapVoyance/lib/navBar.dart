import 'dart:async';

import 'package:app_tracking_transparency/app_tracking_transparency.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dialogs/flutter_dialogs.dart';



/*
class NavBarWidget extends StatefulWidget {
  const  NavBarWidget({Key? key}) : super(key: key);

  @override
  State< NavBarWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State< NavBarWidget>
    with SingleTickerProviderStateMixin {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  int _selectedIndex = 0;
  double? lat;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  List<Widget> _bodyView = <Widget>[
    GpListFree(),
    SignInScreen()
  ];
  List<Widget> _icons = const [
    Icon(Icons.airplanemode_active),

    Icon(Icons.login)
  ];
List<String> _labels = ['Gp disponible', 'Se Connecter'];
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  late TabController _tabController;
  User? getCurrentUser() {
    final User? user = _auth.currentUser;
    return user;
  }

  TextEditingController _latController = new TextEditingController();
  TextEditingController _longController = new TextEditingController();

  bool _isSigningIn = false;
  late DatabaseReference _positionsRef;
  bool initialized = false;
  late StreamSubscription<DatabaseEvent> _positionsSubscription;

  Future<void> init() async {
    final database = FirebaseDatabase.instance;
    _positionsRef  = database.ref('positions');
    database.setLoggingEnabled(false);
    if (!kIsWeb) {
      database.setPersistenceEnabled(true);
      database.setPersistenceCacheSizeBytes(10000000);
    }
    if (!kIsWeb) {
      // await _counterRef.keepSynced(true);
    }
    setState(() {
      initialized = true;
    });
    final positionsQuery = _positionsRef.limitToLast(10);
    _positionsSubscription = positionsQuery.onChildAdded.listen(
          (DatabaseEvent event) {
        print('Child added: ${event.snapshot.value}');
      },
      onError: (Object o) {
        final error = o as FirebaseException;
        print('Error: ${error.code} ${error.message}');
      },
    );
  }



  Future<void> deletePositionByUid(String uid) async {
    DatabaseReference positionsRef =
    FirebaseDatabase.instance.ref().child('positions');

  }

  _addPosition(String uid) async {
    Query query = _positionsRef.orderByChild('uid').equalTo(uid);
    DatabaseEvent event = await query.once();
    print(event.snapshot.ref.get() );

    if (event.snapshot.exists) {
      // L'UID existe déjà, nous mettons à jour la position existante
      Map<dynamic, dynamic> positions = event.snapshot.value as Map<dynamic, dynamic>;
      String positionKey = positions.keys.first;

      await _positionsRef.child(positionKey).update({
        "lat": _latController.text,
        "long": _longController.text,
      });
    } else {
      // L'UID est unique, nous créons une nouvelle position
      await _positionsRef.push().set({
        "uid": uid,
        "lat": _latController.text,
        "long": _longController.text,
      });
    }
  }

  Future<Position?> _getLocation() async {
    LocationPermission permission = await Geolocator.checkPermission();
    print(LocationPermission.denied);

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return null;
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return null;
    }

    Position position = await Geolocator.getCurrentPosition();
    print(position.longitude);

    Geolocator.getPositionStream()
        .listen((position) {

      //     _updateLocationMessage(position);


    });

    return position;
  }

  void _updateLocationMessage(Position position) async {

    _latController.text=position.latitude.toString();
    _longController.text=position.longitude.toString();
    print("_latController.text");
    print(_latController.text);



    List<Placemark> placemarks =
    await placemarkFromCoordinates(position.latitude, position.longitude);


    /* setState(() {
      _locationMessage =
      "${placemarks[0].subLocality},${placemarks[0].street}, ${placemarks[0].postalCode}, ${placemarks[0].locality}, ${placemarks[0].administrativeArea}, ${placemarks[0].country}";
    });*/
    print(placemarks[0].subLocality!);
    print(placemarks[0]);
    print(placemarks);


  }
  getUser()async{
    User? currentUser = getCurrentUser();
    if (currentUser != null) {
   setState(() {
    _bodyView = <Widget>[
       GpListFree(),
       GpListPage (),
         UserInfoScreen()
     ];
   _icons = const [
       Icon(Icons.airplanemode_active),
       Icon(Icons.airplanemode_active),

       Icon(Icons.person)
     ];
     _labels = ['Gp disponible',"Mes annonces", 'Mon compte'];
   });
   _tabController = TabController(vsync: this, length: _icons.length);

      // Perform actions with the user's information
    } else {
      _tabController = TabController(vsync: this, length: _icons.length);

      // No user is signed in
    }

  }


  String _authStatus = 'Unknown';
  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlugin() async {
    final TrackingStatus status = await AppTrackingTransparency.trackingAuthorizationStatus;
    setState(() => _authStatus = '$status');
    //If the system can show an authorization request dialog
    if (status == TrackingStatus.notDetermined) {
      //Show a custom explainer dialog before the system dialog
      //  await showCustomTrackingDialog(context);
      //Wait for dialog popping animation
      //  await Future.delayed(const Duration(milliseconds: 200));
      //Request system's tracking authorization dialog
      final TrackingStatus status =
      await AppTrackingTransparency.requestTrackingAuthorization();
      setState(() => _authStatus = '$status');
    }

    final uuid = await AppTrackingTransparency.getAdvertisingIdentifier();
    print("UUID: $uuid");
  }

  @override
  void initState() {
    // TODO: implement initState


    super.initState();
    initPlugin();

  }


showDialog(){
  if(Permission.location.status!=PermissionStatus.granted){
    showPlatformDialog(
      context: context,
      builder: (_) => BasicDialogAlert(
        title: Text("Location Permission"),
        content: Text("This app requires access to your location to provide XYZ feature."),
        actions: <Widget>[
          BasicDialogAction(
            title: Text("OK"),
            onPressed: () {
              Navigator.pop(context); // Close the disclosure dialog
              requestLocationPermission(); // Request the location permission
            },
          ),
        ],
      ),
    );
  }

}
  Future<void> requestLocationPermission() async {
    PermissionStatus status = await Permission.location.request();
    if (status == PermissionStatus.granted) {
      // La permission a été accordée par l'utilisateur
      // Vous pouvez maintenant utiliser la fonctionnalité nécessitant la permission de localisation
    } else {
      // La permission a été refusée par l'utilisateur
      // Gérez le cas où la fonctionnalité ne peut pas être utilisée en raison du refus de la permission
    }
  }


  Widget _tabItem(Widget child, String label, {bool isSelected = false}) {
    return AnimatedContainer(
        margin: EdgeInsets.all(8),
        alignment: Alignment.center,
        duration: const Duration(milliseconds: 500),
        decoration: !isSelected
            ? null
            : BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.black,
        ),
        padding: const EdgeInsets.all(1),
        child: Column(
          children: [
            child,
            Text(label, style: TextStyle(fontSize: 8)),
          ],
        ));
  }



  @override
  Widget build(BuildContext context) {


    return Scaffold(
     // backgroundColor: CustomColors.firebaseNavy.withOpacity(opacity),
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
      //  backgroundColor: CustomColors.firebaseNavy,
        title: AppBarTitle(),
        actions: [IconButton(onPressed:(){}, icon: Icon(Icons.notifications,color: Colors.orange,))],
      ),
      body: Center(
        child: _bodyView.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: Container(
        height: 100,
        padding: const EdgeInsets.all(12),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(50.0),
          child: Container(
            color: Colors.teal.withOpacity(0.1),
            child: TabBar(
                onTap: (x) {
                  setState(() {
                    _selectedIndex = x;
                  });
                },
                labelColor: Colors.white,
                unselectedLabelColor: Colors.blueGrey,
                padding: EdgeInsets.all(0),
                labelStyle: TextStyle(),
                indicator: const UnderlineTabIndicator(
                  borderSide: BorderSide.none,
                ),
                tabs: [
                  for (int i = 0; i < _icons.length; i++)
                    _tabItem(
                      _icons[i],
                      _labels[i],
                      isSelected: i == _selectedIndex,
                    ),
                ],
                controller: _tabController),
          ),
        ),
      ),
    );
  }
}
*/