import 'dart:io';

import 'package:dio/dio.dart';
import 'package:dio/io.dart';
import 'package:flutter/material.dart';
import 'package:snapvoyance/begin.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'firebase_options.dart';
import 'package:snapvoyance/repository.dart';
import 'package:snapvoyance/utils/const.dart';
import 'package:snapvoyance/auth/PhoneNumberAuthenticationScreen.dart';
import 'package:fcm_config/fcm_config.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:snapvoyance/firebase_options.dart';

const USE_DATABASE_EMULATOR = true;
// The port we've set the Firebase Database emulator to run on via the
// `firebase.json` configuration file.
const emulatorPort = 9000;
// Android device emulators consider localhost of the host machine as 10.0.2.2
// so let's use that if running on Android.
final emulatorHost =
    (!kIsWeb && defaultTargetPlatform == TargetPlatform.android)
        ? '10.0.2.2'
        : 'localhost';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // Obtenir la liste des caméras disponibles sur l'appareil
  // final cameras = await availableCameras();
//FCM
  await FCMConfig.instance
      .init(
    options: DefaultFirebaseOptions.currentPlatform,
    // Note once channel created it can not be changed
    //  defaultAndroidForegroundIcon: "@mipmap/launcher_icon",
    defaultAndroidChannel: const AndroidNotificationChannel(
      'fcm_channel',
      'Fcm config',
      importance: Importance.high,
    ),
    displayInForeground: (notification) {
      return false;
    },
  )
      .then((value) async {
    if (kDebugMode) {
      print(await FCMConfig.instance.messaging.getToken());
    }
    if (!kIsWeb) {
      FCMConfig.instance.messaging.subscribeToTopic('snv_app');
    }
  });
//
  Dio dio = Dio();
  (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
      (HttpClient client) {
    client.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    return client;
  };
  dio.options.baseUrl = APIConstants.API_BASE_URL;
  ResponseRepository rRepository = ResponseRepository(dio: dio);
  ////firebase////
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  if (USE_DATABASE_EMULATOR) {
    //  FirebaseDatabase.instance.useDatabaseEmulator('10.0.2.2', emulatorPort);
  }

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MyApp(rRepository: rRepository));
  });
}

class MyApp extends StatelessWidget {
  final ResponseRepository rRepository;
  // final List<CameraDescription> cameras;
  MyApp({super.key, required this.rRepository});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: '',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home:
          //    PhoneNumberAuthenticationScreen() //BeginPage(rRepository:rRepository ,),
          //CameraPage(cameras)
          BeginPage(
        rRepository: rRepository,
      ),
    );
  }
}
