import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:pinput/pinput.dart';
import 'package:loading_indicator/loading_indicator.dart';

class PhoneNumberAuthenticationScreen extends StatefulWidget {
  @override
  _PhoneNumberAuthenticationScreenState createState() =>
      _PhoneNumberAuthenticationScreenState();
}

class _PhoneNumberAuthenticationScreenState
    extends State<PhoneNumberAuthenticationScreen> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _smsCodeController = TextEditingController();
  String initialCountry = 'SN';
  PhoneNumber number = PhoneNumber(isoCode: 'SN');
  String _verificationId = '';
  bool sendedSms = false;
  bool _sendingSms = false;
  bool _otpSms = false;
  User? getCurrentUser() {
    final User? user = _auth.currentUser;
    return user;
  }

  Future<void> _verifyPhoneNumber() async {
    setState(() {
      _sendingSms = true;
    });
    print(_phoneNumberController.text);
    PhoneVerificationCompleted verificationCompleted =
        (PhoneAuthCredential phoneAuthCredential) async {
      print("sms");
      print(phoneAuthCredential.smsCode!);
      _smsCodeController.setText(phoneAuthCredential.smsCode!);
      await _auth.signInWithCredential(phoneAuthCredential);
      // TODO: Perform actions after successful verification
    };

    PhoneVerificationFailed verificationFailed =
        (FirebaseAuthException authException) {
      setState(() {
        _sendingSms = false;
      });
      // TODO: Handle verification failure
      print('Verification failed: ${authException.message}');
    };

    PhoneCodeSent codeSent = (String verificationId, int? resendToken) async {
      _verificationId = verificationId;
      setState(() {
        sendedSms = true;
        _sendingSms = false;
      });
    };

    PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      _verificationId = verificationId;
      setState(() {
        sendedSms = true;
        _sendingSms = false;
      });
    };

    await _auth.verifyPhoneNumber(
      phoneNumber: _phoneNumberController.text,
      verificationCompleted: verificationCompleted,
      verificationFailed: verificationFailed,
      codeSent: codeSent,
      codeAutoRetrievalTimeout: codeAutoRetrievalTimeout,
      //  autoRetrievedSmsCodeForTesting: (String? sms) {}
    );
  }

  Future<void> _signInWithPhoneNumber() async {
    try {
      setState(() {
        _otpSms = true;
      });
      final PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: _verificationId,
        smsCode: _smsCodeController.text,
      );

      await _auth.signInWithCredential(credential);
      setState(() {
        _otpSms = false;
      });
      // TODO: Perform actions after successful sign-in
    } catch (e) {
      setState(() {
        _otpSms = false;
      });
      // TODO: Handle sign-in failure
      print('Sign-in failed: $e');
    }
  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    print("getCurrentUser().uid");

    print(getCurrentUser()!.uid);
    print(getCurrentUser()!.phoneNumber);
    print(getCurrentUser()!.displayName);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Phone Number Authentication'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            sendedSms != true
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      InternationalPhoneNumberInput(
                        onInputChanged: (PhoneNumber number) {
                          print(number.phoneNumber);
                          setState(() {
                            _phoneNumberController.text = number.phoneNumber!;
                          });
                        },
                        onInputValidated: (bool value) {
                          print(value);
                        },
                        selectorConfig: SelectorConfig(
                          selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
                        ),
                        ignoreBlank: false,
                        autoValidateMode: AutovalidateMode.disabled,
                        selectorTextStyle: TextStyle(color: Colors.black),
                        initialValue: number,
                        //  textFieldController: _phoneNumberController,
                        formatInput: true,
                        keyboardType: TextInputType.numberWithOptions(
                            signed: true, decimal: true),
                        inputBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(
                            color: Colors.grey,
                          ),
                        ),
                        onSaved: (PhoneNumber number) {
                          print('On Saved: $number');
                        },
                      ),
                      SizedBox(height: 16.0),
                      _sendingSms
                          ? Container(
                              width: 50,
                              height: 50,
                              child: LoadingIndicator(
                                  indicatorType: Indicator.ballPulse,

                                  /// Required, The loading type of the widget
                                  colors: const [Colors.blue],

                                  /// Optional, The color collections
                                  strokeWidth: 2,

                                  /// Optional, The stroke of the line, only applicable to widget which contains line
                                  backgroundColor: Colors.transparent,

                                  /// Optional, Background of the widget
                                  pathBackgroundColor: Colors.black

                                  /// Optional, the stroke backgroundColor
                                  ),
                            )
                          : ElevatedButton(
                              onPressed: _verifyPhoneNumber,
                              child: Text('Send Verification Code'),
                            ),
                    ],
                  )
                : Center(
                    child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Pinput(
                        length: 6,
                        androidSmsAutofillMethod:
                            AndroidSmsAutofillMethod.smsRetrieverApi,
                        controller: _smsCodeController,
                      ),
                      SizedBox(height: 16.0),
                      _otpSms
                          ? Container(
                              width: 100,
                              height: 100,
                              child: LoadingIndicator(
                                  indicatorType: Indicator.ballPulse,

                                  /// Required, The loading type of the widget
                                  colors: const [Colors.blue],

                                  /// Optional, The color collections
                                  strokeWidth: 2,

                                  /// Optional, The stroke of the line, only applicable to widget which contains line
                                  backgroundColor: Colors.transparent,

                                  /// Optional, Background of the widget
                                  pathBackgroundColor: Colors.black

                                  /// Optional, the stroke backgroundColor
                                  ),
                            )
                          : ElevatedButton(
                              onPressed: _signInWithPhoneNumber,
                              child: Text('Sign In'),
                            )
                    ],
                  ))
          ],
        ),
      ),
    );
  }
}
